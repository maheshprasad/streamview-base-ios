//
//  WatchFullVideo.m
//  Streaming
//
//  Created by KrishnaDev on 10/10/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "WatchFullVideo.h"
#import "YTPlayerView.h"
#import "AppDelegate.h"
#import <AVKit/AVKit.h>
#import "WebServiceHandler.h"
#import "UtilityClass.h"

@interface WatchFullVideo ()<YTPlayerViewDelegate,UIGestureRecognizerDelegate, AVPlayerViewControllerDelegate>
{
    YTPlayerView *playerView;
    NSTimer *timerHide;
    AppDelegate *appDelegate;
    
    UIView *viewHeader;
    
    AVPlayer *player;
    UIImageView *defaultImage;
    AVPlayerViewController *controller;
    AVPlayerItem *playerItem;
    int playerPausedTime;
    NSUserDefaults *defaluts;
}
//@property (nonatomic) JWPlayerController *player;
@end

@implementation WatchFullVideo
@synthesize strUserType,strVideoURL,strTitle,strImageURL;


- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.strPlayerIdenty=@"yes";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appInactive:) name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoEndNotification:) name: AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    [self UIUpdate];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [appDelegate setShouldRotate:YES];
    
    defaluts = [[NSUserDefaults alloc]init];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)appInactive:(NSNotification *)notification{
    [player pause];
    
    [appDelegate setShouldRotate:YES];
    if([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight){
        
        [[UIDevice currentDevice] setValue:
         [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                    forKey:@"orientation"];
    }
}

- (void)videoEndNotification:(NSNotification *)notification{
    
    NSLog(@"End video");
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
    
    [self serviceRequest:METHOD_VIDEO_COMPLETE params:postDataDict key:@"VideoComplete"];
    
    [self serviceRequest:METHOD_ADD_HISTORY params:postDataDict key:@"AddHistory"];
    
    [self onBack];
}

- (void)UIUpdate{
    if ([strUserType isEqualToString:@"2"])
    {
        playerView=[[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        playerView.backgroundColor=[UIColor blackColor];
        [self.view addSubview:playerView];
        
        playerView.delegate = self;
        
        NSArray *arrLocal=[strVideoURL componentsSeparatedByString:@"/"];
        
        NSString *strVideo=[arrLocal lastObject];
        
        NSDictionary *playerVars = @{
                                     @"controls" : @2,
                                     @"autoplay" :@1,
                                     @"playsinline" : @1,
                                     @"autohide" : @1,
                                     @"theme" : @"light",
                                     @"color" : @"red"
                                     // @"showinfo" : @0,
                                     // @"rel" : @0,
                                     
                                     };
        [playerView loadWithVideoId:strVideo playerVars:playerVars];
        
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayerTapped:)];
        singleFingerTap.numberOfTapsRequired = 1;
        singleFingerTap.delegate = self;
        [playerView addGestureRecognizer:singleFingerTap];
        
        playerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    }
    else
    {
        
        // AVPlayer
        
        NSURL *movieURL = [NSURL URLWithString: strVideoURL];
        AVAsset *asset = [AVURLAsset assetWithURL:movieURL];
        playerItem = [AVPlayerItem playerItemWithAsset: asset];
        
        player = [AVPlayer playerWithPlayerItem:playerItem];
        [player addObserver:self forKeyPath:@"timeControlStatus" options:0 context:nil];
        [player addObserver:self forKeyPath:@"status" options:0 context:nil];
        
        controller = [[AVPlayerViewController alloc] init];
        
        [self addChildViewController:controller];
        [self.view addSubview:controller.view];
        
        controller.view.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height);
        controller.delegate = self;
        controller.player = player;
        controller.updatesNowPlayingInfoCenter = YES;
        //        controller.entersFullScreenWhenPlaybackBegins = YES;
        //        controller.exitsFullScreenWhenPlaybackEnds = YES;
        
        if (self.isLive){
            controller.showsPlaybackControls = NO;
            
        }else{
            controller.showsPlaybackControls = YES;
        }
        
        defaultImage = [[UIImageView alloc] initWithFrame: controller.view.frame];
        defaultImage.image = [UIImage imageNamed:@"sample.png"];
        //        [defaultImage sd_setImageWithURL:[NSURL URLWithString:[dictValue valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
        defaultImage.hidden = NO;
        [controller.contentOverlayView addSubview:defaultImage];
        
        //        [player play];
    }
    
    // [self shouldAutorotate];
    viewHeader=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    viewHeader.backgroundColor=[UIColor clearColor];
    
    UIButton *btnBack=[UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(20, 20, 30, 30);
    [btnBack addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    //    [viewHeader addSubview:btnBack];
    //    [controller.contentOverlayView addSubview:btnBack];
    [self.view addSubview:btnBack];
    [self.view bringSubviewToFront:btnBack];
    
    viewHeader.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

// Time Control Status

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    
    if (object == player && [keyPath isEqualToString:@"timeControlStatus"]) {
        if (player.timeControlStatus == AVPlayerTimeControlStatusPlaying) {
            defaultImage.hidden = YES;
            
            NSLog(@"Playing");
        }
        else if (player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
            playerPausedTime = CMTimeGetSeconds(player.currentTime);
            
            NSString *timeFormat = [self timeFormatted:playerPausedTime];
            NSLog(@"%@",timeFormat);
            NSLog(@"Paused");
            
            
            NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
            [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
            [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
            [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
            [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
            [postDataDict setValue:timeFormat forKey:@"duration"];
            
            [self serviceRequest:METHOD_VIDEO_CURRENT_TIME params:postDataDict key:@"PlayerPausedTime"];
            
            
        }
        
    }else if (object == player && [keyPath isEqualToString:@"status"]){
        if (player.status == AVPlayerStatusReadyToPlay){
            
            Float64 seconds = [self.seekTime floatValue];
            CMTime targetTime = CMTimeMakeWithSeconds(seconds, NSEC_PER_SEC);
            [player seekToTime:targetTime
               toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
            
            [player play];
            
        }else {
            [player pause];
            NSLog(@"%@",player.error);
        }
    }
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)HideNavBar
{
    viewHeader.hidden=YES;
}
-(void)onDisplayClick
{
    //NSLog(@"clicked");
    
    if (viewHeader.hidden == NO)
    {
        viewHeader.hidden=YES;
        // hide the Navigation Bar
        //  [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    // if Navigation Bar is already hidden
    else if (viewHeader.hidden == YES)
    {
        // Show the Navigation Bar
        
        viewHeader.hidden=NO;
        
        //    [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        [timerHide invalidate];
        timerHide=nil;
        
        timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    }
}

#pragma mark Webservice

- (void)serviceRequest:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                
                
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
        
    }];
}

//Delegate Method

-(void) onPlayerTapped:(UIGestureRecognizer *)gestureRecognizer {
    // isInPlayingMode = NO;
    //NSLog(@"clicked");
    [self onDisplayClick];
}
#pragma mark youtube delegate methods

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification {
    if([notification.name isEqual:@"Playback started"] && notification.object != self) {
        [playerView pauseVideo];
    }
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
            case kYTPlayerStatePlaying:
            //NSLog(@"Started playback");
            break;
            case kYTPlayerStatePaused:
            //NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}

- (void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality{
    
}
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error{
    
}
#pragma mark -- Jw Player Delegate
#pragma mark - callback delegate methods

-(void)onTime:(double)position ofDuration:(double)duration
{
    NSString *playbackPosition = [NSString stringWithFormat:@"%.01f/.01%f", position, duration];
    // self.playbackTime.text = playbackPosition;
}
-(void)onPlay
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onPause
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onBuffer
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onIdle
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onReady
{
    //  [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onFirstFrame:(NSInteger)loadTime
{
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}
-(void)onComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdSkipped:(NSString *)tag
{
    //  [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdComplete:(NSString *)tag
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdImpression:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforePlay
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforeComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPlay:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPause:(NSString *)tag
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onAdError:(NSError *)error
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}



#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
    
    [playerView removeFromSuperview];
    //    [_player.view removeFromSuperview];
    
    //    [controller.view removeFromSuperview];
    //    [controller removeFromParentViewController];
    
    [timerHide invalidate];
    timerHide=nil;
    
    //    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
    //                                                  forBarMetrics:UIBarMetricsDefault];
    //
    //    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    // [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    //self.navigationController.navigationBar.translucent =YES;
    
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (IBAction)onBack {
    [player pause];
    [controller removeFromParentViewController];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end

