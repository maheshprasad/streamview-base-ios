//
//  SeasonVideoCell.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 10/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "SeasonVideoCell.h"

@implementation SeasonVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
    
}

- (void)initialize
{
    // This code is only called once
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.center = self.videoImageView.center;
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.color = [UIColor whiteColor];
    [self.videoImageView addSubview:self.activityIndicator];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
