//
//  InvoiceView.m
//  Streaming
//
//  Created by Aravinth Ramesh on 09/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "InvoiceView.h"

@implementation InvoiceView

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self custominit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self custominit];
    }
    
    return self;
}

- (void)custominit{
    
    self.backgroundColor = [UIColor clearColor];
//    self.bgView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.7];
    self.backgroundColor = [UIColor clearColor];

    self.closeButton.layer.cornerRadius = 11.0F;
//    self.paymentBackView.layer.cornerRadius = 5.0f;
    self.contentView.layer.cornerRadius = 5.0f;
    self.okButton.layer.cornerRadius = 3.0f;
//    self.couponCodeTF.autocapitalizationType = UITextAutocapitalizationTypeWords;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
