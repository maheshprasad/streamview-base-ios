//
//  RegisterView.m
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "RegisterView.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import "UtilityClass.h"
#import "PrivacyandTermsView.h"

#define thumbSize CGSizeMake(130, 150)

@interface RegisterView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    NSMutableArray *arrImage;
    AppDelegate *appDelegate;
    
    NSString *strProfileImg,*strID,*strName,*strEmailID,*strLoginType;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    
    UITextField *currentTextView;
    NSUserDefaults *defaults;
    NSDictionary *tAndcDict;
    UIButton *checkBox;
    BOOL isCheckBoxSelected;
}

@end

@implementation RegisterView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    defaults = [NSUserDefaults new];
    
    self.title=@"Signup";
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    arrImage=[[NSMutableArray alloc]init];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [self UIUpdate];
}

- (void)viewWillAppear:(BOOL)animated{
}

- (void)UIUpdate{
    
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    // [signIn setClientID: kClientId];
    [signIn setScopes:[NSArray arrayWithObject:@"https://www.googleapis.com/auth/plus.login"]];
    signIn.delegate = self;
    signIn.uiDelegate = self;
//    [signIn setDelegate: self];
//    [signIn setUiDelegate:self];
    //NSLog(@"Initialized auth2...");
    
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    tapGesture.delegate = self;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    int xPos=0;
    int yPos=100;
    
//    UIButton *btnGoogle=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-160, yPos, 150, 45)];
//    UIButton *btnGoogle=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-165, yPos, 150, 45)];
//    [btnGoogle setTintColor:[UIColor whiteColor]];
//    [btnGoogle setImage:[UIImage imageNamed:@"google_final.png"] forState:UIControlStateNormal];
//    btnGoogle.backgroundColor = [UIColor whiteColor];
//    btnGoogle.contentMode = UIViewContentModeScaleAspectFit;
//    //    btnGoogle.clipsToBounds = YES;
//    btnGoogle.layer.cornerRadius = 3.0f;
//    [btnGoogle addTarget:self action:@selector(onGoogle:) forControlEvents:UIControlEventTouchDown];
//    [scrollView addSubview:btnGoogle];
//
//    UIButton *btnFacebook=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+10, yPos, 150, 45)];
//    btnFacebook.backgroundColor = [UIColor blueColor];
//    btnFacebook.contentMode = UIViewContentModeScaleAspectFit;
//    //    btnFacebook.clipsToBounds = YES;
//    btnFacebook.layer.cornerRadius = 3.0f;
//    [btnFacebook setImage:[UIImage imageNamed:@"fb_final.png"] forState:UIControlStateNormal];
//    [btnFacebook addTarget:self action:@selector(onFacebook:) forControlEvents:UIControlEventTouchDown];
//    [scrollView addSubview:btnFacebook];
//
//    yPos+=75;
//
    UILabel *lblOR=[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50)];
    lblOR.text=@"Register";
    lblOR.textColor = [UIColor whiteColor];
    lblOR.textAlignment=NSTextAlignmentCenter;
    //    [lblOR setFont:[UIFont boldSystemFontOfSize:20]];
    [lblOR setFont:[UIFont systemFontOfSize:35.0f]];
    [scrollView addSubview:lblOR];
//
//    yPos+=50;
//
//    btnImage=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, yPos, 100, 100)];
//    [btnImage setBackgroundImage:[UIImage imageNamed:@"Default.png"] forState:UIControlStateNormal];
//    [btnImage addTarget:self action:@selector(onChooseImage:) forControlEvents:UIControlEventTouchDown];
//    [scrollView addSubview:btnImage];
//    btnImage.layer.cornerRadius = btnImage.frame.size.height / 2;
//    btnImage.clipsToBounds = YES;
//
//    [btnImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
//    [btnImage.layer setBorderWidth: 2.0];
//
    yPos+=120;
    
    txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtEmail setBorderStyle:UITextBorderStyleNone];
    txtEmail.placeholder=@"Email";
    txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    txtEmail.keyboardType = UIKeyboardTypeASCIICapable;
    txtEmail.delegate=self;
    [txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtEmail.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtEmail];
    
    
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=20;
    }
    
    txtName=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtName setBorderStyle:UITextBorderStyleNone];
    txtName.placeholder=@"Name";
    txtName.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtName.autocorrectionType = UITextAutocorrectionTypeNo;
    txtName.keyboardType = UIKeyboardTypeASCIICapable;
    txtName.delegate=self;
    [txtName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtName.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtName];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=20;
    }
    
    txtPassword=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtPassword setBorderStyle:UITextBorderStyleNone];
    txtPassword.placeholder=@"Password";
    txtPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;
    txtPassword.delegate=self;
    txtPassword.secureTextEntry=YES;
    [txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtPassword.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtPassword];
    
    
//    txtPhone=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
//    [txtPhone setBorderStyle:UITextBorderStyleNone];
//    txtPhone.placeholder=@"Phone (Optional)";
//    txtPhone.autocapitalizationType = UITextAutocapitalizationTypeNone;
//    txtPhone.autocorrectionType = UITextAutocorrectionTypeNo;
//    txtPhone.delegate=self;
//    txtPhone.keyboardType=UIKeyboardTypeNumberPad;
//    [txtPhone setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
//    txtPhone.textColor=[UIColor whiteColor];
//    [scrollView addSubview:txtPhone];
    
//    yPos+=40;
//    {
//        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
//        viewLine.backgroundColor=[UIColor whiteColor];
//        [scrollView addSubview:viewLine];
//
//        yPos+=20;
//    }
    
   
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=10;
    }
    
    yPos += 30;

    checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
    checkBox.frame = CGRectMake(20, yPos, 25, 25);
    [checkBox setImage:[UIImage imageNamed:@"checkbox_empty.png"] forState:UIControlStateNormal];
    checkBox.selected = NO;
    [checkBox addTarget:self action:@selector(checkBoxTapped:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:checkBox];

    NSMutableAttributedString *tAndcString = [[NSMutableAttributedString alloc] initWithString:@"I agree, Terms and Conditions."];
    [tAndcString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,8)];
    [tAndcString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:56.0f/255.0f green:85.0f/255.0f blue:143.0f/255.0f alpha:1.0f] range:NSMakeRange(9,tAndcString.length-9)];

    UIButton *tandcButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tandcButton.frame = CGRectMake(checkBox.frame.size.width+25, yPos, self.view.frame.size.width-75, 25);
    [tandcButton setAttributedTitle:tAndcString forState:UIControlStateNormal];
    tandcButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [tandcButton addTarget:self action:@selector(tAndcTapped:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:tandcButton];
    
    yPos+=40;
    
    UIButton *btnSignUp=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSignUp.frame=CGRectMake(20, yPos, self.view.frame.size.width-40, 50);
    [btnSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    //    [btnSignUp.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnSignUp.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnSignUp.layer.cornerRadius = 5.0f;
    [btnSignUp setBackgroundColor:[UIColor redColor]];
    [btnSignUp addTarget:self action:@selector(onSignup:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnSignUp];
    
    yPos += 70;
    UIButton *btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLogin.frame = CGRectMake(20, yPos, self.view.frame.size.width-40, 40);
//    [btnLogin setTitle:@"Have account? Login" forState:UIControlStateNormal];
    [btnLogin.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(onLoginpage:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnLogin];
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Have account? Login"]];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(14, 5)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 14)];
    [btnLogin setAttributedTitle:attrStr forState:UIControlStateNormal];
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+50);
    
   
}

-(IBAction)onLoginpage:(id)sender
{
  
    [self performSegueWithIdentifier:@"login" sender:self];
    printf("login btn pressed");
}

-(IBAction)onBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)checkBoxTapped:(UIButton *)sender{
    
    if (checkBox.selected) {
        
        [checkBox setImage:[UIImage imageNamed:@"checkbox_empty.png"] forState:UIControlStateNormal];
        isCheckBoxSelected = NO;
        checkBox.selected = NO;

    }else{
        
        [checkBox setImage:[UIImage imageNamed:@"checkbox_tick.png"] forState:UIControlStateNormal];
        isCheckBoxSelected = YES;
        checkBox.selected = YES;
        
    }
}

- (void)tAndcTapped:(UIButton *)sender{
    
    NSString *strID=[defaults valueForKey:@"id"];
    NSString *strToken=[defaults valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=2;
    NSString *strSend=[NSString stringWithFormat:@"?%@&=%@",strID,strToken];
    
    [service executeWebserviceWithMethod1:METHOD_TERMS withValues:strSend];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)dismissKeyBoard
{
    [txtName resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtPhone resignFirstResponder];
}
- (IBAction)onSignup:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    // [self presentViewController:alertController animated:YES completion:nil];
    
    NSString *emailRegEx =@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([txtEmail.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Email id";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if([emailTest evaluateWithObject:txtEmail.text] == NO)
    {
        alertController.message=@"Please enter valid email address.";
        [self presentViewController:alertController animated:YES completion:nil];
        //_txtEmail.text=@"";
        return;
    } else if ([txtName.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Name";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([txtPassword.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
//    else if ([txtPhone.text isEqualToString:@""])
//    {
//        alertController.message=@"Please Enter Phone Number";
//        [self presentViewController:alertController animated:YES completion:nil];
//        return;
//    }
    
    else if (!isCheckBoxSelected)
    {
        alertController.message=@"Please agree our Terms and Condition";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    
    Webservice *service=[[Webservice alloc]init];
    service.tag=1;
    service.delegate=self;
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:txtEmail.text forKey:@"email"];
    [_params setObject:txtPassword.text forKey:@"password"];
    [_params setObject:txtName.text forKey:@"name"];
    [_params setObject:@"manual" forKey:@"login_by"];
    [_params setObject:@"ios" forKey:@"device_type"];
    [_params setObject:appDelegate.strDeviceToken forKey:@"device_token"];
    
    //[_params setObject:strID forKey:@"height"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
//    NSString* FileParamConstant = @"picture";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    
    
    // create request
    NSString *strURL = [SERVICE_URL stringByAppendingString:METHOD_REGISTER];
    NSURL *requestURL = [NSURL URLWithString:strURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
   
//    for (int i=0; i<arrImage.count; i++)
//    {
//        NSDictionary *dictLocal=[arrImage objectAtIndex:i];
//        UIImage *image1 = [dictLocal objectForKey:@"UIImagePickerControllerOriginalImage"];
//        //      NSData * imageData1 = UIImageJPEGRepresentation(image1,100);
//        // add image data
//        NSData *imageData = UIImageJPEGRepresentation(image1, 1.0);
//        if (imageData) {
//            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:imageData];
//            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//        }
//
//        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    [service executeWebserviceWithMethodinImage:METHOD_REGISTER withValues:request];
    
    
    [defaults setObject:@"1"  forKey:@"logintype"];
    [defaults synchronize];
//    [self performSegueWithIdentifier:@"login_menu" sender:self];
    
}
- (IBAction)onChooseImage:(id)sender
{
//    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //Use camera if device has one otherwise use photo library
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//    {
//        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
//        // imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
//        // picker.showsCameraControls = NO;
//    }
//    else
//    {
//        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
   // }
    
//    [imagePicker setDelegate:self];
    
    //Show image picker
//    [self presentModalViewController:imagePicker animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [arrImage removeAllObjects];
    [arrImage addObject:info];
    
    UIImage *image1 = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [btnImage setBackgroundImage:image1 forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [defaults setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"id"]]  forKey:@"id"];
            [defaults setObject:[dictResponse valueForKey:@"name"]  forKey:@"name"];
            [defaults setObject:[dictResponse valueForKey:@"picture"]  forKey:@"picture"];
            [defaults setObject:[dictResponse valueForKey:@"token"]  forKey:@"token"];
            [defaults setObject:[dictResponse valueForKey:@"sub_profile_id"]  forKey:@"sub_profile_id"];
             [defaults setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"push_status"]]  forKey:@"push"];
            [defaults setObject:[dictResponse objectForKey:@"login_by"] forKey:@"login_by"];
            //[user setObject:[dictResponse valueForKey:@"status"]  forKey:@"status"];
            [defaults setObject:[dictResponse objectForKey:@"payment_subscription"] forKey:@"payment_subscription"];
//            [defaults setObject:@"1" forKey:@"payment_subscription"];
            [defaults synchronize];
            [self performSegueWithIdentifier:@"login_menu" sender:self];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }else if (webservice.tag == 2){
        
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            tAndcDict = [dictResponse valueForKey:@"page"];
            
            PrivacyandTermsView *tAndcVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyandTermsView"];
            tAndcVC.dictValue = tAndcDict;
            [self.navigationController pushViewController:tAndcVC animated:YES];
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
}

- (IBAction)onGoogle:(id)sender {
    
    [[GIDSignIn sharedInstance] signOut];
    
    [[GIDSignIn sharedInstance] signIn];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    if (error)
    {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
    }
    else
        
    {
        
        //NSLog(@"User Id: %@",user.userID);
        //NSLog(@"Name: %@",user.profile.name);
        //NSLog(@"Email: %@",user.profile.email);
        
        strName=user.profile.name;
        strEmailID=user.profile.email;
        strID=user.userID;
        strLoginType=@"google";
        
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            strProfileImg=imageURL.absoluteString;
            //NSLog(@"Image URL :%@",imageURL);
        }
        
        
        [self onSocialLogin];
        
    }
    
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    if (error) {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
        return;
    }
}



-(IBAction)onFacebook:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
        if (error) {
            //NSLog(@"Process error");
        }
        else if (result.isCancelled)
        {
            //NSLog(@"Cancelled");
        }
        else
        {
            //NSLog(@"Logged in");
            
           // [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                          initWithGraphPath:@"/me"
                                          parameters:@{ @"fields": @"id,email,name,gender,birthday"}
                                          HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             
             
             {
                 if (!error) {
                     [defaults setValue:result[@"email"] forKey:@"Email"];
                                          
                     strName=result[@"name"];
                     strEmailID=result[@"email"];
                     strID=result[@"id"];
                     strProfileImg=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", strID];
                     strLoginType=@"facebook";
                     
                     
                     //NSLog(@"fetched user:%@", result);
                     [self onSocialLogin];
                     
                     
                 }
                 
             }];
            
        }
    }];
    
    
    
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}
-(void)onSocialLogin
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strSend=[NSString stringWithFormat:@"{\"social_unique_id\":\"%@\",\"email\":\"%@\",\"picture\":\"%@\",\"mobile\":\"%@\",\"login_by\":\"%@\",\"device_token\":\"%@\",\"device_type\":\"%@\",\"name\":\"%@\"}",strID,strEmailID,strProfileImg,@"",strLoginType,appDelegate.strDeviceToken,@"ios",strName];
    
    [service executeWebserviceWithMethod:METHOD_REGISTER withValues:strSend];
    
    [defaults setObject:@"2"  forKey:@"logintype"];
    [defaults synchronize];
    
    
}
#pragma mark -- Keyboard hide  and moving

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtPhone) {
        txtPhone.userInteractionEnabled = YES;
    }
    //    if (!textField.inputAccessoryView)
    //    {
    //        textField.inputAccessoryView = toolKeyboard;
    //    }
    currentTextView=textField;
    
    //    [self moveViewUp:YES toRect:textField.frame];
    [self moveScrollView:textField.frame];    //edited
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if (textField == txtPhone) {
        if ([txtPhone.text length]<13)
        {
            txtPhone.userInteractionEnabled = YES;

        }else{

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please Enter a Valid Mobile number" message:@"Mobile number must be between 6 - 13 digits." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
            txtPhone.text = @"";
        }
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

//    NSLog(@"Phone Number : %@",phoneNumber);
//    if (textField == txtPhone) {
//        if ([txtPhone.text length]==13 || [txtPhone.text length]==10)
//        {
//            txtPhone.userInteractionEnabled = YES;
//
//        }else{
//
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:@"Please Enter a Valid Mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//
//        }
//    }
    return YES;
}

- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
    //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

@end
