//
//  Webservice.m
//  //

//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import "Webservice.h"

@implementation Webservice
@synthesize tag;

-(void)executeWebserviceWithMethod:(NSString *)method withValues:(NSString *)values
{
    //NSLog(@"REQ: %@",values);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    
    NSString *strURL = [SERVICE_URL stringByAppendingString:method];
    
    // NSURL *requestURL = [NSURL URLWithString:strURL];
    
    
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    // NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:[values dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
    [dataTask resume];
    
}
-(void) executeWebserviceWithMethodinImage:(NSString *)method withValues:(NSURLRequest *)values
{

    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:values];
    [dataTask resume];
}

- (void) executeWebserviceWithMethod1:(NSString *) method withValues:(NSString *) values
{
    
    NSString *strURL =[NSString stringWithFormat:@"%@%@%@",SERVICE_URL,method,values];
    //  NSURL *requestURL = [NSURL URLWithString:strURL];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    // NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
    // [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //  [urlRequest setHTTPBody:[values dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
    [dataTask resume];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    
    receivedData=nil; receivedData=[[NSMutableData alloc] init];
    [receivedData setLength:0];
    
    completionHandler(NSURLSessionResponseAllow);
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
   didReceiveData:(NSData *)data {
    
    [receivedData appendData:data];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error {
    
    if (error) {
        // Handle error
        //   [self.delegate receivedErrorWithMessage:@"No response / Invalid response from server."];
        if([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."]){
            [self.delegate receivedErrorWithMessage:@"Please check your internet connection"];
        }else{
            [self.delegate receivedErrorWithMessage:@"Server not Connected."];
        }
        
    }
    else {
        NSDictionary* response=(NSDictionary*)[NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];
        // perform operations for the  NSDictionary response
        
        if(response)
        {
            if([self.delegate respondsToSelector:@selector(receivedResponse:fromWebservice:)])
            {
                [self.delegate receivedResponse:response fromWebservice:self];
            }
            else
            {
                [self.delegate receivedErrorWithMessage:[response valueForKey:@"Error"]];
            }
        }
        else
        {
            [self.delegate receivedErrorWithMessage:@"No response / Invalid response from server."];
        }
        
        //NSLog(@"%@",response );
    }
}


@end

