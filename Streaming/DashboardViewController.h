//
//  DashboardViewController.h
//  StreamFlix
//
//  Created by Mahesh Prasad on 28/05/19.
//  Copyright © 2019 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardViewController : UIViewController
- (IBAction)letsDiveInBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
