//
//  UtilityClass.h
//  Streaming
//
//  Created by Aravinth Ramesh on 17/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UtilityClass : UIViewController

@property(strong, nonatomic)UIActivityIndicatorView *activityIndicator;

+ (UtilityClass *)sharedInstance;

- (void)errorHandling:(NSDictionary *)dictResponse VC:(UIViewController *)currentVC;

- (void)networkError:(UIViewController *)currentVC;

- (BOOL)isNetworkConnected;

- (UIImageView *)setImageWithActivityIndicator:(UIImageView *)imgView urlString:(NSString *)urlString;
@end
