//
//  PaymentCompleteVC.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 25/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCompleteVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIView *contentVIew;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *paymentIDLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

@property (strong, nonatomic) NSString *price, *status, *paymentID;
@property (strong, nonatomic) UIImage *bgImage;

@end
