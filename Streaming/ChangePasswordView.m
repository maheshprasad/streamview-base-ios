//
//  ChangePasswordView.m
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "ChangePasswordView.h"
#import "ProgressIndicator.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "UtilityClass.h"
#import "HomeView.h"
#import "WebServiceHandler.h"

@interface ChangePasswordView ()<WebServiceDelegate,UIGestureRecognizerDelegate>
{
    AppDelegate *appDeleagte;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    NSUserDefaults *defaluts;
    UITextField *currentTextView;
}

@end

@implementation ChangePasswordView

- (void)viewDidLoad {
    [super viewDidLoad];
    appDeleagte=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    defaluts = [[NSUserDefaults alloc]init];
    // Do any additional setup after loading the view.
    self.title=@"Change Password";
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
        UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
        tapRecognizer1.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:tapRecognizer1];

    
    [self.txtCurrentPassword setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtCurrentPassword.textColor=[UIColor blackColor];
    
    [self.txtNewPwd setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtNewPwd.textColor=[UIColor blackColor];
    
    [self.txtOldPwd setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    _txtOldPwd.textColor=[UIColor blackColor];

    
}
-(void)dismissKeyBoard
{
    [_txtCurrentPassword resignFirstResponder];
    [_txtNewPwd resignFirstResponder];
    [_txtOldPwd resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onChangePassword:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    // [self presentViewController:alertController animated:YES completion:nil];
    
    
     if ([_txtCurrentPassword.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Current Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([_txtNewPwd.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter New Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if ([_txtOldPwd.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Old Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if (![_txtNewPwd.text isEqualToString:_txtCurrentPassword.text])
    {
        alertController.message=@"The passwords entered does not match";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"password\":\"%@\",\"password_confirmation\":\"%@\",\"old_password\":\"%@\"}",strID,strToken,_txtNewPwd.text,_txtOldPwd.text,_txtCurrentPassword.text];
    
    [service executeWebserviceWithMethod:METHOD_CHANGE_PASSWORD withValues:strSend];

}

- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)callLogoutAPI{
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
    [self callService:METHOD_LOGOUT params:postDataDict key:@"logout"];
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue] == true)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[dictResponse valueForKey:@"message"] message:@"Are you want to login with new password ?" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self callLogoutAPI];
                });
                
            }];
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                HomeView *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeView"];
                [self.navigationController pushViewController:home animated:YES];
            }];
            [alertController addAction:noAction];
            [alertController addAction:yesAction];
            [self presentViewController:alertController animated:YES completion:nil];
            
            _txtCurrentPassword.text=@"";
            _txtNewPwd.text=@"";
            _txtOldPwd.text=@"";
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }

}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([dict objectForKey:@"success"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [defaluts setObject:@""  forKey:@"id"];
                    [defaluts setObject:@""  forKey:@"token"];
                    [defaluts synchronize];
                    [appDeleagte onExpriedPage];
                    
                });
                
            }else{
                
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
    }];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}



@end
