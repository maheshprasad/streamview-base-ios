//
//  SingleVideoVC.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 09/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "SingleVideoVC.h"
#import "MKDropdownMenu.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import <AVKit/AVKit.h>
#import "HCSStarRatingView.h"
#import "UIImageView+WebCache.h"
#import "YTPlayerView.h"
#import "WatchFullVideo.h"
#import "WebServiceHandler.h"
#import "SearchBarVC.h"
#import "UtilityClass.h"
#import "APIService.h"
#import "ViewPlansVC.h"
#import "DetailedCell.h"
#import "UtilityCell.h"
#import "SeasonVideoCell.h"
#import "InvoiceView.h"
#import "PaymentInvoice.h"
#import "viewAllViewController.h"
#import "PaymentInvoiceVC.h"
#import "PaymentCompleteVC.h"

@interface SingleVideoVC () <MKDropdownMenuDelegate, AVPlayerViewControllerDelegate, YTPlayerViewDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, WebServiceDelegate>{
    
    AppDelegate *appDelegate;
    NSUserDefaults *defaults;
    NSDictionary *responseDict;
    NSDictionary *videoDetails;
    AVPlayer *player;
    YTPlayerView *youTubePlayer;
    AVPlayerViewController *controller;
    NSArray *genreArray;
    NSArray *genreList;
    int like_dislike;
    UIView *gradientViewBottom;
    UIView *gradientViewTop;
    int pay_per_view_status;
    int is_ppv_subscribe_page;
    InvoiceView *invoice;
    PaymentInvoice *selectPaymentView;
    UIVisualEffectView *blurEffectView;
    NSString *stripeAmount;
    NSString *currency;
    NSArray *castArray;
    NSString *castLabelStr, *castingID, *castName;
    NSArray *spamCatList;
    UILabel *bannerLabel;
    NSString *spaceString;
    NSString *genreTitle;
    PaymentCompleteVC *completeInvoice;
}

@end

@implementation SingleVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentCompleted:) name:@"PaymentCompleted" object:nil];
    defaults = [NSUserDefaults new];
    self.videoTableView.delegate = self;
    self.videoTableView.dataSource = self;
    self.videoTableView.estimatedRowHeight = 350.0;
    self.videoTableView.rowHeight = UITableViewAutomaticDimension;
    [self.videoTableView registerNib:[UINib nibWithNibName:@"SeasonVideoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SeasonVideoCell"];
    bannerLabel = [UILabel new];
    bannerLabel.frame = CGRectMake(0 , self.playerView.frame.size.height-40, self.view.frame.size.width, 30);
    bannerLabel.backgroundColor = [UIColor redColor];
    bannerLabel.textColor = [UIColor whiteColor];
    bannerLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    bannerLabel.textAlignment = NSTextAlignmentCenter;
    [self.playerView addSubview:bannerLabel];
    bannerLabel.hidden = YES;
    spaceString = @"      ";
    [self reportSpamCatagoryList];
    [self shadowView];
}

- (void)shadowView{
    [self.view bringSubviewToFront:self.titleView];
    [self.view bringSubviewToFront:self.customNavView];
    UIView *topShadow = [UIView new];
    topShadow.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
    [self.videoImageView addSubview:topShadow];
    UIView *bottomShadow = [UIView new];
    bottomShadow.frame = CGRectMake(0, self.videoImageView.frame.size.height-56, self.view.frame.size.width, 100);
    [self.videoImageView addSubview:bottomShadow];
    
    CAGradientLayer *gradientBottom = [CAGradientLayer layer];
    gradientBottom.frame = bottomShadow.bounds;
    gradientBottom.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f].CGColor];
    [bottomShadow.layer insertSublayer:gradientBottom atIndex:0];
    
    CAGradientLayer *gradientTop = [CAGradientLayer layer];
    gradientTop.frame = topShadow.bounds;
    gradientTop.colors = @[(id)[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha: 1.0f].CGColor, (id)[UIColor clearColor].CGColor];
    [topShadow.layer insertSublayer:gradientTop atIndex:0];
}

- (void)callSingleVideoAPI{

    NSMutableDictionary *postParams = [NSMutableDictionary new];
    [postParams setObject:[defaults objectForKey:@"id"] forKey:@"id"];
    [postParams setObject:[defaults objectForKey:@"token"] forKey:@"token"];
    [postParams setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postParams setObject:self.adminVideoID forKey:@"admin_video_id"];
    
    [self callService:METHOD_SINGLE_VIDEO params:postParams key:@"SingleVideo"];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    [self callSingleVideoAPI];
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}
- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)shareButtonTapped:(id)sender {
    NSURL *URL = [NSURL URLWithString:[responseDict valueForKey:@"share_link"]];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[URL] applicationActivities:nil];
    
    [activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError * error){
        if ( completed )
        {
        } else {
        }
    }];
    
    if ( [activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
    activityViewController.popoverPresentationController.sourceView = self.view;
    }
    
    [self presentViewController:activityViewController animated:YES completion:nil];
}
- (IBAction)searchButtonTapped:(id)sender {
    SearchBarVC *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchBarVC"];
    [self.navigationController pushViewController:search animated:YES];
}

#pragma mark Webservice

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                if ([key isEqualToString:@"SingleVideo"]) {
                    responseDict = dict;
                    currency = [responseDict objectForKey:@"currency"];
                    castArray = [responseDict objectForKey:@"cast_crews"];
                    pay_per_view_status = [[responseDict objectForKey:@"pay_per_view_status"] intValue];
                    is_ppv_subscribe_page = [[responseDict objectForKey:@"is_ppv_subscribe_page"] intValue];
                
                    if ([[responseDict objectForKey:@"is_genre"] intValue] == 1) {
                        genreArray = [responseDict objectForKey:@"genre_videos"];
                        genreList = [responseDict objectForKey:@"genres"];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        [self loadMainUI];
                        [self.videoTableView reloadData];
                    });
                }else if ([key isEqualToString:@"GenreVideos"]){
                    genreArray = [dict objectForKey:@"data"];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.videoTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }else if ([key isEqualToString:@"likes"]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        DetailedCell *cell = [self.videoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

                        if (like_dislike) {
                            cell.likesLabel.text = [NSString stringWithFormat:@"%@ Likes", [dict valueForKey:@"like_count"]];
                        }
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }else if ([key isEqualToString:@"CastAndCrew"]){
                    viewAllViewController *viewAll = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAllViewController"];
                    viewAll.allVideoDict = dict;
                    viewAll.strIdenty = @"CastAndCrew";
                    viewAll.castID = castingID;
                    viewAll.strTitleName = castName;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController pushViewController:viewAll animated:YES];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            }
        }
    }];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

- (void)loadMainUI{
    videoDetails = [responseDict valueForKey:@"video"];
    genreTitle = [videoDetails objectForKey:@"genre_name"];
    int videoType = [[videoDetails objectForKey:@"video_type"] intValue];
//    [self addingCast];
    if (videoType == 1) {
        [self playInAVPlayer];
    }else{
        [self playInYoutubePlayer];
    }
    self.titleLabel.text = [videoDetails objectForKey:@"title"];
    self.videoImageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:self.videoImageView urlString:[videoDetails valueForKey:@"default_image"]];
    
    bannerLabel.text = [NSString stringWithFormat:@"You need to pay %@%@ to watch the Full Video",currency,[[responseDict objectForKey:@"video"] objectForKey:@"amount"]];
    
    if (pay_per_view_status == 0) {
        bannerLabel.hidden = NO;
    }else{
        bannerLabel.hidden = YES;
    }
}

//- (void)addingCast{
//    NSMutableArray *castingArray = [NSMutableArray new];
//    if (castArray.count > 0) {
//        for (NSDictionary *castDict in castArray) {
//            NSString *hash = @"#";
//            NSString *castName = [castDict objectForKey:@"name"];
//            castName = [castName stringByReplacingOccurrencesOfString:@" " withString:@""];
//            [castingArray addObject: [hash stringByAppendingString:castName]];
//            castLabelStr = [NSString stringWithFormat:@"Cast: %@", [castingArray componentsJoinedByString:@" "]];
//        }
//    }else{
//        castLabelStr = @"";
//    }
//    DetailedCell *cell = [self.videoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    // Attach a block to be called when the user taps a hashtag
//    cell.castLabel.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
//
//        for (int i=0; i<castingArray.count; i++) {
//            NSString *name = [castingArray objectAtIndex:i];
//            if ([string isEqualToString:name]) {
//                NSLog(@"%d",i);
//                castingID = [[castArray objectAtIndex:i] objectForKey:@"cast_crew_id"];
//                castName = [[castArray objectAtIndex:i] objectForKey:@"name"];
//                NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
//                [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
//                [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
//                [postDataDict setValue: [defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
//                [postDataDict setValue:[[castArray objectAtIndex:i] objectForKey:@"cast_crew_id"] forKey:@"cast_crew_id"];
//                [postDataDict setValue:@"0" forKey:@"skip"];
//                [postDataDict setValue:@"ios" forKey:@"device_type"];
//                [self callService:METHOD_CAST_VIDEOS params:postDataDict key:@"CastAndCrew"];
//
//            }
//        }
//    };
//}

- (void)playInAVPlayer{

}

- (void)playInYoutubePlayer{

}
- (IBAction)playButtonTapped:(id)sender {
    
    WatchFullVideo *fullVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"WatchFullVideo"];
    fullVideo.strVideoURL=[responseDict valueForKey:@"ios_video"];
    fullVideo.strVideoURL= [responseDict objectForKey:@"ios_trailer_video"];
    fullVideo.strUserType=[NSString stringWithFormat:@"%@",  [[responseDict valueForKey:@"video"] valueForKey:@"video_type"]];
    fullVideo.strImageURL=[[responseDict valueForKey:@"video"] valueForKey:@"default_image"];
    fullVideo.strTitle=[[responseDict valueForKey:@"video"] valueForKey:@"title"];
    fullVideo.adminVideoID = [[responseDict valueForKey:@"video"] objectForKey:@"admin_video_id"];
    fullVideo.seekTime = [responseDict objectForKey:@"seek"];
    [self.navigationController pushViewController:fullVideo animated:YES];
}

#pragma Report Spam
- (IBAction)addSpamButtonTapped:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Report as Spam" message:@"" preferredStyle:UIAlertControllerStyleAlert]; // 7
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        __block NSString *spamReason = [[NSString alloc] init];

        UIAlertController *spamListAlert = [UIAlertController alertControllerWithTitle:@"Reason to Report" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSDictionary *spamValue in spamCatList) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:[spamValue objectForKey:@"value"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                spamReason = [spamValue objectForKey:@"value"];
                [self addSpamReport:spamReason];
            }];
            
            [spamListAlert addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        
        [spamListAlert addAction:cancelAction];
        [self presentViewController:spamListAlert animated:YES completion:nil];
        
//        [self reportSpamCatagoryList];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)reportSpamCatagoryList{
    
    [[WebServiceHandler sharedInstance] sendRequest:METHOD_REPORTSPAMLIST isApiRequired:NO urlParameters:nil method:GET isAuthRequired:NO postData:nil withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if ([[dict objectForKey:@"success"] intValue] == 1) {
            spamCatList = [dict objectForKey:@"data"];
        }else{
            [[UtilityClass sharedInstance] errorHandling:dict VC:self];
        }
    }];
}

- (void)addSpamReport:(NSString *)reason{
    
    __block NSError *error;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
    [postDataDict setValue:reason forKey:@"reason"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDataDict options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:METHOD_ADDSPAM isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if ([[dict objectForKey:@"success"] intValue] == 1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
                [self popup:[dict objectForKey:@"message"]];
            });
        }
    }];
}

- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

#pragma mark Tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([[responseDict objectForKey:@"is_genre"] intValue] == 1) {
        return 3;
    }else{
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 2) {
        return genreArray.count;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        DetailedCell *detailedCell = [tableView dequeueReusableCellWithIdentifier:@"DetailedCell"];
//        if ([responseDict objectForKey:@"likes"] == nil) {
//            detailedCell.likesLabel.text = @"0 Likes";
//        }else{
//            detailedCell.likesLabel.text = [NSString stringWithFormat:@"%@ Likes",[responseDict objectForKey:@"likes"]];
//        }
        detailedCell.likesLabel.text = [NSString stringWithFormat:@"%@ Likes", [self checkingNilValue:[responseDict objectForKey:@"likes"]]];
        detailedCell.dateLabel.text = [self checkingNilValue:[videoDetails objectForKey:@"publish_time"]];
        detailedCell.descriptionLabel.text = [videoDetails objectForKey:@"description"];
        detailedCell.ratingView.value = [[videoDetails objectForKey:@"ratings"] floatValue];
        detailedCell.castLabel.text = castLabelStr;
        detailedCell.castLabel.textColor = [UIColor whiteColor];
        detailedCell.castLabel.tintColor = [UIColor darkGrayColor];
        detailedCell.ageLabel.text = [NSString stringWithFormat:@"  %@  ", [self checkingNilValue:[videoDetails objectForKey:@"age"]]];

        if ([[responseDict valueForKey:@"wishlist_status"]integerValue] == 0)
        {
//            [detailedCell.wishListButton setTitle:[NSString stringWithFormat:@"%@ ADD TO WISHLIST", spaceString] forState:UIControlStateNormal];
//            detailedCell.wishListImageView.image = [UIImage imageNamed:@"plus@48.png"];
        }
        else
        {
//            [detailedCell.wishListButton setTitle:[NSString stringWithFormat:@"%@ ADDED", spaceString] forState:UIControlStateNormal];
//            detailedCell.wishListImageView.image = [UIImage imageNamed:@"tick@48.png"];
        }
        
        if ([[responseDict objectForKey:@"is_liked"] intValue] == 0) {
            [detailedCell.likeButton setImage:[UIImage imageNamed:@"thumb_up_white.png"] forState:UIControlStateNormal];
            detailedCell.likeButton.tag = 0;
        }else{
            [detailedCell.likeButton setImage:[UIImage imageNamed:@"thumb_down_white.png"] forState:UIControlStateNormal];
            detailedCell.likeButton.tag = 1;
        }
        
        [detailedCell.likeButton addTarget:self action:@selector(likesButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [detailedCell.wishListButton addTarget:self action:@selector(addRemoveWishlist:) forControlEvents:UIControlEventTouchUpInside];
        [detailedCell.fullVideoButton addTarget:self action:@selector(fullVideoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        return detailedCell;
    }else if (indexPath.section == 1){
        UtilityCell *utilityCell = [tableView dequeueReusableCellWithIdentifier:@"UtilityCell"];
        utilityCell.genreLabel.text = genreTitle;
        return utilityCell;
    }else{

        SeasonVideoCell *seasonCell = (SeasonVideoCell *)[tableView dequeueReusableCellWithIdentifier:@"SeasonVideoCell" forIndexPath:indexPath];
            NSDictionary *videoDict = [genreArray objectAtIndex:indexPath.row];
        
        seasonCell.videoImageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:seasonCell.videoImageView urlString:[videoDict objectForKey:@"default_image"]];
            seasonCell.videoTitleLabel.text = [videoDict objectForKey:@"title"];
            seasonCell.videoDurationLabel.text = [videoDict objectForKey:@"duration"];
            seasonCell.descriptionLabel.text = [videoDict objectForKey:@"description"];

        return seasonCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        self.adminVideoID = [[genreArray objectAtIndex:indexPath.row] objectForKey:@"admin_video_id"];
        [self callSingleVideoAPI];
    }
}

- (NSString *)checkingNilValue:(NSString *)nilString{
    if (nilString == nil) {
        nilString = @"";
    }
    return nilString;
}

#pragma mark Dropdown delegates

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu{
    return 1;
}
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    return genreList.count;
}

- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return  [[genreList objectAtIndex:row] objectForKey:@"genre_name"];
}
- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    UtilityCell *cell = [self.videoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    genreTitle = [[genreList objectAtIndex:row] objectForKey:@"genre_name"];
    cell.genreLabel.text =genreTitle;
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:[videoDetails objectForKey:@"sub_category_id"] forKey:@"sub_category_id"];
    [postDataDict setValue:[[genreList objectAtIndex:row] objectForKey:@"genre_id"] forKey:@"genre_id"];
    [self callService:METHOD_GENRE_VIDEOS params:postDataDict key:@"GenreVideos"];
    [dropdownMenu closeAllComponentsAnimated:YES];
    [dropdownMenu reloadAllComponents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)fullVideoButtonTapped:(UIButton *)sender{
    if (!pay_per_view_status) {
        
        if (is_ppv_subscribe_page == 1) {
            [self displaySelectPaymentView];
        }else{
            [self displayInvoice:nil];
        }
        
    }else{
        
        if ([[responseDict objectForKey:@"user_type"] intValue] == 0) {
            
            UIAlertController *premiumAlert = [UIAlertController alertControllerWithTitle:@"Pay now !" message:@"Subscribe as Premium user to Watch the full Video." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *viewplans = [UIAlertAction actionWithTitle:@"View Plans" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                ViewPlansVC *plans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
                [self.navigationController pushViewController:plans animated:YES];
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [premiumAlert addAction:viewplans];
            [premiumAlert addAction:cancelAction];
            [self presentViewController:premiumAlert animated:YES completion:nil];
        }else{
            [self playButtonTapped:sender];
        }
    }
}

#pragma mark youtube delegate methods

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification {
    if([notification.name isEqual:@"Playback started"] && notification.object != self) {
        [youTubePlayer pauseVideo];
    }
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            //NSLog(@"Started playback");
            break;
        case kYTPlayerStatePaused:
            //NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    
}

- (void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality{
    
}
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error{
    
}

#pragma mark Like/Dislike methods

//- (void)likesButtonTapped:(UIButton *)sender{
//    DetailedCell *cell = [self.videoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//    
//    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
//    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
//    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
//    [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
//    [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
//    
//    if (sender.tag == 0) {
//        sender.tag = 1;
//        like_dislike = 1;
//        [cell.likeButton setImage:[UIImage imageNamed:@"thumb_down_white.png"] forState:UIControlStateNormal];
//        
//        // Service for Like Video.
//        [self callService:METHOD_LIKE_VIDEO params:postDataDict key:@"likes"];
//        
//    }else{
//        sender.tag = 0;
//        like_dislike = 1;
//        [cell.likeButton setImage:[UIImage imageNamed:@"thumb_up_white.png"] forState:UIControlStateNormal];
//        [self callService:METHOD_LIKE_VIDEO params:postDataDict key:@"likes"];
//    }
//    
//}

#pragma mark Add/Remove wishlist

- (void)addRemoveWishlist:(UIButton *)sender{
    NSString *message;
    
    if ([sender.currentTitle isEqualToString:[NSString stringWithFormat:@"%@ ADDED", spaceString]]) {
        message = @"You want to remove this video from your wishlist ?";
    }else{
        message = @"You want to add this video from your wishlist ?";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=2;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,self.adminVideoID];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:defaultAction1];
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            DetailedCell *cell = [self.videoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            if ([cell.wishListButton.currentTitle isEqualToString:[NSString stringWithFormat:@"%@ ADDED", spaceString]]) {
                [cell.wishListButton setTitle:[NSString stringWithFormat:@"%@ ADD TO WISHLIST", spaceString] forState:UIControlStateNormal];
                cell.wishListImageView.image = [UIImage imageNamed:@"plus@48.png"];
            }else{
                [cell.wishListButton setTitle:[NSString stringWithFormat:@"%@ ADDED", spaceString] forState:UIControlStateNormal];
                cell.wishListImageView.image = [UIImage imageNamed:@"tick@48.png"];
            }
            [self popup:[dictResponse objectForKey:@"message"]];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
}

#pragma mark Payment Request

#pragma mark Invoive for Stripe payment
- (void)displayInvoice:(UIButton *)sender{
    NSDictionary *sendDict = [NSDictionary dictionaryWithObjectsAndKeys:[responseDict objectForKey:@"currency"],@"currency",
                              [[responseDict objectForKey:@"video"] objectForKey:@"title"], @"title",
                              [[responseDict objectForKey:@"video"] objectForKey:@"amount"], @"amount",
                              [[responseDict objectForKey:@"video"] objectForKey:@"admin_video_id"], @"admin_video_id", nil];
    PaymentInvoiceVC *invoice = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentInvoiceVC"];
    invoice.isPPV = YES;
    invoice.bgImage = [self takeScreenshot];
    invoice.detailedDict = sendDict;
    [self.navigationController pushViewController:invoice animated:NO];
//    invoice.couponCodeTF.text = @"";
//    // Invoice View Config :
//    invoice.couponCodeView.hidden = NO;
//    invoice.couponSuccessView.hidden = YES;
//    invoice = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] lastObject];
//    invoice = [invoice initWithFrame:self.view.frame];
//    invoice.contentView.layer.cornerRadius = 5.0f;
//
//    invoice.durationLabel.hidden = YES;
//    invoice.monthLabel.hidden = YES;
//    invoice.okButton.hidden = YES;
//
//    invoice.titleLabel.text = [[responseDict objectForKey:@"video"] valueForKey:@"title"];
//    invoice.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency,[[responseDict objectForKey:@"video"] objectForKey:@"amount"]];
//
//    //Note:
//
//    if ([[[responseDict objectForKey:@"video"] objectForKey:@"amount"] intValue] == 0){
//
//        invoice.stripePayButton.hidden = YES;
//        invoice.okButton.hidden = NO;
//
//    }else{
//        invoice.stripePayButton.hidden = NO;
//        invoice.okButton.hidden = YES;
//
//    }
////    [self.view.window addSubview:blurEffectView];
////    [self.view.window addSubview:invoice];
//    [self.view addSubview:blurEffectView];
//    [self.view addSubview:invoice];
//    [self fadeInAnimation:invoice];
//
//    [invoice.closeButton addTarget:self action:@selector(closeInvoice:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.stripePayButton addTarget:self action:@selector(stripePay:) forControlEvents:UIControlEventTouchUpInside];
//
//    [invoice.checkCouponButton addTarget:self action:@selector(applyCouponTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.removeButton addTarget:self action:@selector(removeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.okButton addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    invoice.checkCouponButton.tag = sender.tag;
}

- (UIImage *)takeScreenshot {
    CGSize size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGRect rec = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view drawViewHierarchyInRect:rec afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)displaySelectPaymentView{
    selectPaymentView = [[[NSBundle mainBundle] loadNibNamed:@"PaymentInvoice" owner:self options:nil] lastObject];
    selectPaymentView = [selectPaymentView initWithFrame:self.view.frame];
    selectPaymentView.layer.cornerRadius = 5.0f;
    selectPaymentView.contentView.layer.cornerRadius = 5.0f;
    [selectPaymentView.amountPayButton setTitle:[NSString stringWithFormat:@"Pay %@%@",currency,[[responseDict objectForKey:@"video"] objectForKey:@"amount"]] forState:UIControlStateNormal];
//    [self.view.window addSubview:blurEffectView];
//    [self.view.window addSubview:selectPaymentView];
    [self.view addSubview:blurEffectView];
    [self.view addSubview:selectPaymentView];
    
    [selectPaymentView.viewPlansButton addTarget:self action:@selector(goToViewPlans:) forControlEvents:UIControlEventTouchUpInside];
    [selectPaymentView.amountPayButton addTarget:self action:@selector(displayInvoice:) forControlEvents:UIControlEventTouchUpInside];
    [selectPaymentView.closeButton addTarget:self action:@selector(closeInvoice:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)goToViewPlans:(UIButton *)sender{
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    ViewPlansVC *viewPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
    [self.navigationController pushViewController:viewPlans animated:YES];
}

- (void)stripePay:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
    defaults = [[NSUserDefaults alloc] init];
    if (!(stripeAmount.length > 0)) {
        stripeAmount = [[responseDict objectForKey:@"video"] objectForKey:@"amount"];
    }
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
    
    UIAlertController *payAlert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Are you sure to pay %@%@ ?",currency, stripeAmount] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self stripePayRequest:METHOD_STRIPE_PPV params:postDataDict tag:sender.tag key:@"StripeAmount"];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    [payAlert addAction:okAction];
    [payAlert addAction:cancelAction];
    
    [self presentViewController:payAlert animated:YES completion:nil];
}

- (void)stripePayfor0Amount:(UIButton *)sender{
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:self.adminVideoID forKey:@"admin_video_id"];
    [self stripePayRequest:METHOD_STRIPE_PPV params:postDataDict tag:sender.tag key:@"0Amount"];
}

- (void)closeInvoice:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
}
-(void)paymentCompleteInvoice:(NSDictionary *)response{
    
    invoice.couponCodeView.hidden = YES;
    invoice.couponSuccessView.hidden = YES;
    
    invoice.payThroughLabel.hidden = YES;
    invoice.stripePayButton.hidden = YES;
    invoice.okButton.hidden = NO;
    invoice.closeButton.hidden = YES;
    invoice.durationLabel.hidden = NO;
    invoice.monthLabel.hidden = NO;
    
    invoice.title.text = @"Paid Amount";
    invoice.price.text = @"Status";
    invoice.monthLabel.text = @"Payment ID";
    
    invoice.titleLabel.text = [NSString stringWithFormat:@"%@%@", currency,[[responseDict objectForKey:@"video"] objectForKey:@"amount"]];
    invoice.priceLabel.text = @"Approved";
    invoice.durationLabel.text = [[response objectForKey:@"data"] objectForKey:@"payment_id"];
    
    [invoice.okButton addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view.window addSubview:blurEffectView];
//    [self.view.window addSubview:invoice];
    [self.view addSubview:blurEffectView];
    [self.view addSubview:invoice];
}

- (void)okButtonTapped:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    //    [self.player stop];
    //    [self.player.view removeFromSuperview];
//    [player pause];
//    [controller removeFromParentViewController];
    [self callSingleVideoAPI];
}

#pragma mark Coupon request

- (void)applyCouponTapped:(UIButton *)sender{
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:invoice.couponCodeTF.text forKey:@"coupon_code"];
    [postDataDict setValue:[[responseDict objectForKey:@"video"] objectForKey:@"admin_video_id"] forKey:@"admin_video_id"];
    
    if ([invoice.couponCodeTF.text isEqualToString:@""]) {
        [self popup:@"Please enter valid code"];
    }else{
        [self serviceRequest:METHOD_APPLY_COUPON_PPV params:postDataDict key:@"ApplyCoupon"];
    }
}

- (void)removeButtonTapped:(UIButton *)sender{
    invoice.couponCodeTF.text = @"";
    invoice.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency,[[responseDict objectForKey:@"video"] objectForKey:@"amount"]];
    stripeAmount = [[responseDict objectForKey:@"video"] objectForKey:@"amount"];
        invoice.couponSuccessView.hidden = YES;
    invoice.couponCodeView.hidden = NO;
    invoice.okButton.hidden = YES;
    invoice.stripePayButton.hidden = NO;
}

- (void)serviceRequest:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            
            [[UtilityClass sharedInstance] networkError:self];
            
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                if ([key isEqualToString:@"ApplyCoupon"]){
                    NSString *amount = [NSString stringWithFormat:@"%@", [[dict objectForKey:@"data"] objectForKey:@"remaining_amount"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        invoice.couponCodeView.hidden = YES;
                        invoice.couponSuccessView.hidden = NO;
                        invoice.appliedCouponLabel.text = [[dict objectForKey:@"data"] objectForKey:@"coupon_code"];
                        stripeAmount = amount;
                        invoice.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency, stripeAmount];
                        if ([stripeAmount isEqualToString:@"0"]) {
                            completeInvoice.price = [NSString stringWithFormat:@"%@%@",currency, stripeAmount];
                            completeInvoice.status = @"Approved";
                            completeInvoice.paymentID = @"Coupon added";
                            [self presentViewController:completeInvoice animated:YES completion:nil];
                        }else{
                            invoice.stripePayButton.hidden = NO;
                            invoice.okButton.hidden = YES;                        }
                    });
                }
                
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
        
    }];
}

- (void)stripePayRequest:(NSString *)service params:(NSMutableDictionary *)params tag:(NSInteger)tag key:(NSString *)key{
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([key isEqualToString:@"StripeAmount"]) {
                        [self paymentCompleteInvoice:dict];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    }else{
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        [self callSingleVideoAPI];
                        [self popup:@"Payment Complete !"];
                    }
                });
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            }
        }
    }];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

#pragma mark Blur view

- (void)addBlurView{
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        //        [self.view.window addSubview: blurEffectView];
        
    } else {
        self.view.backgroundColor = [UIColor blackColor];
    }
}

- (void)fadeInAnimation:(UIView *)view{
    
//    CATransition *transition = [CATransition animation];
//    transition.type =kCATransitionFade;
//    transition.duration = 0.5f;
//    transition.delegate = self;
//    [view.layer addAnimation:transition forKey:nil];
}

- (void)paymentCompleted:(NSNotification *)notification{
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self viewWillAppear:YES];
    });
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
