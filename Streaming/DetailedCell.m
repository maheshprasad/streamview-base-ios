//
//  DetailedCell.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 10/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "DetailedCell.h"

@implementation DetailedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.fullVideoButton.layer.cornerRadius = 5.0;
    self.fullVideoButton.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
