//
//  viewAllViewController.m
//  Streaming
//
//  Created by KrishnaDev on 05/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "viewAllViewController.h"
#import "SingleVideoView.h"
#import "UIImageView+WebCache.h"
#import "Webservice.h"
#import "ProgressIndicator.h"
#import "AppDelegate.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import "UtilityClass.h"
#import "MKDropdownMenu.h"
#import "WebServiceHandler.h"
#import "SingleVideoVC.h"

@interface viewAllViewController ()<WebServiceDelegate,UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, MKDropdownMenuDelegate>
{
    UIScrollView *scrollView;
    NSString *strAd_VideoID;
    AppDelegate *appDelegate;
    
    NSMutableArray *arrAllList;
    NSArray *genreArray;
    UIRefreshControl *refreshControl;
    NSUserDefaults *defaults;
    int nSendSkipVal;
    NSString *genreID;
    UICollectionView *_collectionView;
    UIImageView *noDataImg;
    UILabel *noDataLbl;
}

@end

@implementation viewAllViewController
@synthesize arrList,strTitleName;

@synthesize strKey,strTotalCount,strIdenty;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=strTitleName;
    // Do any additional setup after loading the view.
    defaults = [NSUserDefaults new];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setShouldRotate:NO];
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    
    noDataImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
    noDataImg.image=[UIImage imageNamed:@"oops.png"];
    [self.view addSubview:noDataImg];
    
    noDataLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
    noDataLbl.text=@"Oops! List is empty ";
    noDataLbl.textAlignment=NSTextAlignmentCenter;
    noDataLbl.textColor=[UIColor whiteColor];
    [noDataLbl setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:noDataLbl];
    noDataImg.hidden = YES;
    noDataLbl.hidden = YES;
    nSendSkipVal=12;
    
    arrAllList=[[NSMutableArray alloc]init];
//    arrAllList =[NSMutableArray arrayWithArray:arrList];
    arrAllList = [[self.allVideoDict objectForKey:@"data"] mutableCopy];
    genreArray = [self.allVideoDict objectForKey:@"genres"];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];

    if (genreArray.count) {
        self.seasonListView.hidden = NO;
        self.seasonTitleLabel.text = [[genreArray objectAtIndex:0] objectForKey:@"genre_name"];
        genreID = [[genreArray objectAtIndex:0] objectForKey:@"genre_id"];
        _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, self.seasonListView.frame.size.height+12, self.view.frame.size.width, self.view.frame.size.height-self.seasonListView.frame.size.height-72) collectionViewLayout:layout];
    }else{
        self.seasonListView.hidden = YES;
        _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 8, self.view.frame.size.width, self.view.frame.size.height-60) collectionViewLayout:layout];
    }
//    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, self.seasonListView.frame.size.height+12, self.view.frame.size.width, self.view.frame.size.height-self.seasonListView.frame.size.height-72) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
    
    //    refreshControl = [[UIRefreshControl alloc] init];
    //    refreshControl.tintColor = [UIColor whiteColor];
    //    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    //    [_collectionView addSubview:refreshControl];
    
    _collectionView.alwaysBounceVertical=YES;
    _collectionView.showsVerticalScrollIndicator=NO;
    _collectionView.showsHorizontalScrollIndicator=NO;
    
    refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(Refresh:) forControlEvents:UIControlEventValueChanged];
    _collectionView.bottomRefreshControl = refreshControl;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrAllList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    //cell.backgroundColor=[UIColor greenColor];
    
    NSDictionary *dictLocal=[arrAllList objectAtIndex:indexPath.row];
    
    [[cell viewWithTag:123] removeFromSuperview];   // REMOVING PREVIOUS CONTENT VIEW
    
    UIView *viewContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
    viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewContent.layer.shadowOpacity = 0.7f;
    viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    viewContent.layer.shadowRadius = 1.0f;
    viewContent.layer.cornerRadius = 2.0f;
    viewContent.tag=123;
    [cell addSubview:viewContent];
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,140)];
    imageView.layer.cornerRadius = 2.0f;
    imageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:imageView urlString:[dictLocal valueForKey:@"default_image"]];
//    [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [viewContent addSubview:imageView];
    
    UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
    imgPlay.image=[UIImage imageNamed:@"play.png"];
    //imgPlay.alpha=0.6;
    //    [viewContent addSubview:imgPlay];
    
    UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
    btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    //[btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
    [viewContent addSubview:btnClick];
    
    //Single tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
    tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [btnClick addGestureRecognizer:tapGesture];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 113+30, viewContent.frame.size.width-10, 20)];
    
    lblTitle.text=[dictLocal valueForKey:@"title"];
    [lblTitle setFont:[UIFont systemFontOfSize:15.0f]];
    lblTitle.textColor=[UIColor lightGrayColor];
    lblTitle.lineBreakMode = NSLineBreakByClipping;
    [viewContent addSubview:lblTitle];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3-10, 140+25);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)Refresh:(UIRefreshControl *)refreshControl1
{
    //     [refreshControl endRefreshing];
    
    int nVal=[strTotalCount intValue];
    
    if (nVal>nSendSkipVal)
    {
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=4;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        
        if ([strIdenty isEqualToString:@"home"] || [strIdenty isEqualToString:@"category"])
        {
            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"key\":\"%@\",\"skip\":\"%@\"}",strID,strToken,strKey,[NSString stringWithFormat:@"%d",nSendSkipVal]];
            
            [service executeWebserviceWithMethod:METHOD_VIEW_ALL withValues:strSend];
        }else if ([strIdenty isEqualToString:@"CastAndCrew"]){
            NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
            [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
            [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
            [postDataDict setValue: [defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
            [postDataDict setValue:self.castID forKey:@"cast_crew_id"];
            [postDataDict setValue:[NSString stringWithFormat:@"%d", nSendSkipVal] forKey:@"skip"];
            [postDataDict setValue:@"ios" forKey:@"device_type"];
            [self callService:METHOD_CAST_VIDEOS params:postDataDict key:@"CastAndCrew"];
        }
        else
        {

            NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
            [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
            [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
            [postDataDict setObject: self.subCategoryID forKey:@"sub_category_id"];
            [postDataDict setValue:genreID forKey:@"genre_id"];
            [postDataDict setValue: [NSString stringWithFormat:@"%d", nSendSkipVal] forKey:@"skip"];
            
            [self callService:METHOD_GENRE_VIDEOS params:postDataDict key:@"GenreRefresh"];
        }
        nSendSkipVal+=12;
    }
    else
    
    [refreshControl1 endRefreshing];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}
-(IBAction)onBack:(id)sender
{
        [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onBtnClick:(id)sender
{
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
    singleVideo.adminVideoID = strVideoID;
    [self.navigationController pushViewController:singleVideo animated:YES];
    
}
-(IBAction)onSingleClear:(id)sender
{
    // UIButton *btn=(UIButton *) sender;
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=1;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Dropdown delegates

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu{
    return 1;
}
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    return genreArray.count;
}

- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return  [[genreArray objectAtIndex:row] objectForKey:@"genre_name"];
}
- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.seasonTitleLabel.text = [[genreArray objectAtIndex:row] objectForKey:@"genre_name"];
    genreID = [[genreArray objectAtIndex:row] objectForKey:@"genre_id"];
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject: self.subCategoryID forKey:@"sub_category_id"];
    [postDataDict setValue:[[genreArray objectAtIndex:row] objectForKey:@"genre_id"] forKey:@"genre_id"];
    [postDataDict setValue:@"0" forKey:@"skip"];
    
    [self callService:METHOD_GENRE_VIDEOS params:postDataDict key:@"GenreVideos"];
    [dropdownMenu closeAllComponentsAnimated:YES];
    [dropdownMenu reloadAllComponents];
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [refreshControl endRefreshing];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            //dictSendVal=dictResponse;
            //  [self performSegueWithIdentifier:@"singleVideo" sender:self];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [arrAllList addObjectsFromArray:[dictResponse valueForKey:@"data"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self noDataFound];
            });
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
        singleVideo.strVideoID=strAd_VideoID;
    }
    
}

#pragma mark Webservice

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                if ([key isEqualToString:@"GenreVideos"]){
                    arrAllList = [[dict objectForKey:@"data"] mutableCopy];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self noDataFound];
                    });
                }else if ([key isEqualToString:@"GenreRefresh"]){
                    [arrAllList addObjectsFromArray:[[dict objectForKey:@"data"] mutableCopy]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_collectionView reloadData];
                    });
                }
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
    }];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

- (void)noDataFound{
        if (arrAllList.count > 0) {
            noDataImg.hidden = YES;
            noDataLbl.hidden = YES;
            [_collectionView reloadData];
        }else{
            noDataImg.hidden = NO;
            noDataLbl.hidden = NO;
            [_collectionView reloadData];
        }
}

@end

