//
//  APIService.h
//  Streaming       
//
//  Created by Aravinth Ramesh on 23/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#ifndef APIService_h
#define APIService_h

#define HOST @"http://admin.arfeenkhan.tv/"
#define SERVICE_URL @"http://admin.arfeenkhan.tv/"
//#define HOST @"http://staging.botfingers.com/"
//#define SERVICE_URL @"http://staging.botfingers.com/"
#define API @""
#define APP_NAME @"ArfeenKhan TV"

//Methods

#define METHOD_REGISTER @"userApi/register"
#define METHOD_LOGIN @"userApi/login"
#define METHOD_FORGOT_PASSWORD @"userApi/forgotpassword"
#define METHOD_CHANGE_PASSWORD @"userApi/changePassword"
#define METHOD_GET_USER_PROFILE @"userApi/userDetails"
#define METHOD_PROFILE_UPDATE @"userApi/updateProfile"
#define METHOD_TOKEN_RENEW @"userApi/tokenRenew"
#define METHOD_PAYPAL @"userApi/paybypaypal"
#define METHOD_GET_CATEGORIES @"userApi/categories"
#define METHOD_GET_SUB_CATEGORIES @"userApi/subCategories"
#define METHOD_HOME @"userApi/home"
#define METHOD_GET_VIDEO @"userApi/categoryVideos"
#define METHOD_GET_SUB_CATEGORY @"userApi/subCategoryVideos"
#define METHOD_SEARCH_VIDEO @"userApi/searchVideo"
#define METHOD_SINGLE_VIDEO @"userApi/singleVideo"
#define METHOD_USER_RATING @"userApi/userRating"
#define METHOD_ADD_WISHLIST @"userApi/addWishlist"
#define METHOD_GET_WISHLIST @"userApi/getWishlist"
#define METHOD_DELETE_LIST @"userApi/deleteWishlist"

#define METHOD_ADD_HISTORY @"userApi/addHistory"
#define METHOD_GET_HISTROY @"userApi/getHistory"
#define METHOD_DELETE_HISTORY @"userApi/deleteHistory"
#define METHD_CLEAR_HISTORY @"userApi/deleteHistory"
//#define METHOD_VIEW_ALL @"userApi/common"
#define METHOD_VIEW_ALL @"userApi/keyBasedDetails"

#define METHOD_LIKE_VIDEO @"userApi/like_video"
#define METHOD_DISLIKE_VIDEO @"userApi/dis_like_video"

#define METHOD_REPORTSPAMLIST @"userApi/spam-reasons"
#define METHOD_ADDSPAM @"userApi/add_spam"
#define METHOD_SPAMVIDEOSLIST @"userApi/spam_videos"
#define METHOD_REMOVE_SPAM @"userApi/remove_spam"

#define METHOD_MANAGE_PROFILES @"userApi/active-profiles"
#define METHOD_ADD_PROFILE @"userApi/add-profile"
#define METHOD_EDIT_PROFILE @"userApi/edit-sub-profile"

#define METHOD_DELETE_SUBPROFILE @"userApi/delete-sub-profile"

#define METHOD_CARD_LIST @"userApi/card_details"
#define METHOD_DELETE_CARD @"userApi/delete_card"

#define METHOD_TERMS @"terms_condition"
#define METHOD_PRIVACY @"privacy"
#define METHOD_NOTIFICATION @"userApi/settings"
#define METHOD_DELETE @"userApi/deleteAccount"
#define METHOD_PLAN_LISTS @"userApi/subscription_plans"
#define METHOD_PAID_VIDEOS @"userApi/ppv_list"

#define METHOD_VIEW_PLANS  @"userApi/subscription_plans"
#define METHOD_SUBSCRIBED_PLANS @"userApi/subscribedPlans"
#define METHOD_ADD_CREDIT_CARD @"userApi/payment_card_add"
#define METHOD_ADD_DEFAULT_CARD @"userApi/default_card"
#define METHOD_STRIPE_PAY_PLAN @"userApi/stripe_payment"
#define METHOD_STRIPE_PPV @"userApi/stripe_ppv"

#define METHOD_LOGOUT @"userApi/logout"
#define METHOD_LIVE_VIDEOS_LIST @"userApi/live/videos"
#define METHOD_VIDEO_CURRENT_TIME @"userApi/save/watching/video"
#define METHOD_VIDEO_COMPLETE @"userApi/oncomplete/video"
#define METHOD_CONTINUE_WATCHING @"userApi/continue/videos"

#define METHOD_ENABLE_AUTO_RENEWAL @"userApi/autorenewal/enable"
#define METHOD_DISABLE_AUTO_RENEWAL @"userApi/cancel/subscription"
#define METHOD_FREE_PLAN_PAY @"userApi/pay_now"

#define METHOD_APPLY_COUPON_SUBS @"userApi/apply/coupon/subscription"
#define METHOD_APPLY_COUPON_PPV @"userApi/apply/coupon/ppv"

#define METHOD_GENRE_VIDEOS @"userApi/genres/videos"
#define METHOD_CAST_VIDEOS @"userApi/cast_crews/videos"

#endif /* APIService_h */

