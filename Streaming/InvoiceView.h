//
//  InvoiceView.h
//  Streaming
//
//  Created by Aravinth Ramesh on 09/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface InvoiceView : UIView
@property (strong, nonatomic) IBOutlet UILabel *payThroughLabel;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
//@property (weak, nonatomic) IBOutlet ACFloatingTextField *couponCodeTF;
@property (weak, nonatomic) IBOutlet UITextField *couponCodeTF;

@property (strong, nonatomic) IBOutlet UIButton *stripePayButton;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;

@property (strong, nonatomic) IBOutlet UIView *titleBackView;
@property (strong, nonatomic) IBOutlet UIView *paymentBackView;
@property (strong, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *checkCouponButton;
@property (weak, nonatomic) IBOutlet UIView *couponCodeView;
@property (weak, nonatomic) IBOutlet UIView *couponSuccessView;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UILabel *appliedCouponLabel;

@end
