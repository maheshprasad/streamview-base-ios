//
//  AFNetworkService.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "AFNetworkService.h"
#import <AFNetworking.h>

@implementation AFNetworkService

+ (AFNetworkService *)sharedInstance {
    static AFNetworkService *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (void)sendRequestForImageUpload:(NSString *)service urlParameters:(NSDictionary *)params method:(NSString *)method imageParam:(NSString *)imgParam imageData:(NSData *)imgData withCompletionHandler:(CompletionHandler)myCompletionHandler{
    
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%@",HOST,service];
    
//    NSString *webMethod = [NSString stringWithFormat:@"%lu",(unsigned long)method];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:method URLString:urlStr parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imgData name:imgParam fileName:@"filename.jpg" mimeType:@"image/jpeg"];
        
//        [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath] name:imgParam fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
//                          [progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"%@ %@", response, responseObject);
                          NSDictionary *responseDict = (NSDictionary *)responseObject;
                          myCompletionHandler(responseDict, response, error);
                      }
                  }];
    
    [uploadTask resume];

}

- (void)sendRequestGETandPOST:(NSString *)service postParams:(NSDictionary *)params method:(WebMethod)method withCompletionHandler:(responseHandler)myResponseHandler{
    
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%@",HOST,service];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    // GET
    
    if (method == 0) {
        
        [manager GET:urlStr parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            NSDictionary *responseDict = (NSDictionary *)responseObject;
            
            myResponseHandler(responseDict);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error: %@", error);

        }];
        
    //Post
        
    }else if (method == 1){
        
        [manager POST:urlStr parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            NSDictionary *responseDict = (NSDictionary *)responseObject;
            
            myResponseHandler(responseDict);
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
        }];
    }
    
}

@end
