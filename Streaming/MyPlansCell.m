//
//  MyPlansCell.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 28/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "MyPlansCell.h"

@implementation MyPlansCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor colorWithRed:26/255 green:26/255 blue:26/255 alpha:1.0f];
    self.cardBackView.layer.shadowOpacity = 0.6f;
    self.cardBackView.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    self.cardBackView.layer.shadowRadius = 2.0f;
    self.cardBackView.layer.masksToBounds = YES;
    self.cardBackView.layer.cornerRadius = 10;
    self.renewalButton.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
