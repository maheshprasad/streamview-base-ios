//
//  CardsVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "CardsVC.h"
#import "CardCustomCell.h"
#import "SWRevealViewController.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import <Stripe/Stripe.h>
#import "AFNetworkService.h"
#import <AFNetworking.h>
#import "UtilityClass.h"

@interface CardsVC () <UITableViewDelegate, UITableViewDataSource, STPAddCardViewControllerDelegate, STPPaymentCardTextFieldDelegate>
{
    NSMutableArray *cardsArray;
    NSIndexPath *lastSelected;
    NSUserDefaults *defaults;
    UIImageView *emptyListImg;
    UILabel *emptyListLbl;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end

@implementation CardsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Cards";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    STPPaymentCardTextField *STPtextField = [[STPPaymentCardTextField alloc] init];
    STPtextField.delegate = self;

    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    emptyListImg = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
    emptyListImg.image=[UIImage imageNamed:@"oops.png"];
    [self.view addSubview:emptyListImg];
    emptyListLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
    emptyListLbl.text=@"Oops! List is empty ";
    emptyListLbl.textAlignment=NSTextAlignmentCenter;
    emptyListLbl.textColor=[UIColor whiteColor];
    [emptyListLbl setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:emptyListLbl];
    emptyListImg.hidden = YES;
    emptyListLbl.hidden = YES;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    cardsArray = [NSMutableArray new];
    defaults = [[NSUserDefaults alloc] init];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [self callService:METHOD_CARD_LIST params:postDataDict key:@"CardList"];
}
#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                if ([dict objectForKey:@"success"]) {
                    if ([key isEqualToString:@"DeleteCard"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self popup:[dict objectForKey:@"message"]];
                            [self viewWillAppear:YES];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                        
                    }else if ([key isEqualToString:@"DefaultCard"]){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self popup:@"Default card added !"];
                            [self viewWillAppear:YES];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }
                    else{
                        cardsArray = [dict objectForKey:@"data"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (cardsArray.count > 0) {
                                [self.tableView reloadData];
                            }else{
                                [self emptyListUpdateUI];
                            }
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }
                }else{
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                }
            }
        }];
}

- (void)emptyListUpdateUI{
        emptyListLbl.hidden = NO;
        emptyListImg.hidden = NO;
}

- (IBAction)addButtonTapped:(id)sender {
    
    // Present payment methods view controller
    STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
    addCardViewController.delegate = self;
    // Present add card view controller
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
    navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark STPAddCardViewControllerDelegate

- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController {
    
    // Dismiss add card view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController didCreateToken:(STPToken *)token completion:(STPErrorBlock)completion {
    
    defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    //New card number saved on STPaddCardPaymentVC
    [postDataDict setValue:[defaults valueForKey:@"NewCardNumber"] forKey:@"number"];
    [postDataDict setValue:token forKey:@"card_token"];
    
    NSMutableString *urlStr = [NSMutableString stringWithFormat:@"%@%@",HOST,METHOD_ADD_CREDIT_CARD];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager POST:urlStr parameters:postDataDict progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSDictionary *responseDict = (NSDictionary *)responseDict;
        completion(nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self popup:[responseObject objectForKey:@"message"]];
        });
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completion(error);
    }];
}

- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

- (IBAction)sideButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cardsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 165.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *cardDetails = [cardsArray objectAtIndex:indexPath.row];
    
    CardCustomCell *cardCell = (CardCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CardCustomCell"];
    
    if (!cardCell) {
        
        cardCell = [[CardCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardCustomCell"];
    }
    
    cardCell.selectionStyle = UITableViewCellSelectionStyleNone;
    cardCell.backgroundColor = [UIColor blackColor];
    cardCell.cardNumberLabel.text = [NSString stringWithFormat:@"xxxx-xxxx-xxxx-%@",[cardDetails objectForKey:@"last_four"]];
    cardCell.tintColor = [UIColor whiteColor];
    
    if ([[cardDetails objectForKey: @"is_default"] intValue] == 1) {
        cardCell.defaultCardImage.hidden = NO;
        cardCell.deleteButton.hidden = YES;
    }else{
        cardCell.defaultCardImage.hidden = YES;
        cardCell.deleteButton.hidden = NO;
    }
    
    cardCell.deleteButton.tag = indexPath.row;
    [cardCell.deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cardCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [defaults setObject:[[cardsArray objectAtIndex:indexPath.row] objectForKey:@"card_id"] forKey:@"defaultCardID"];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaults objectForKey:@"defaultCardID"] forKey:@"card_id"];
    [self callService:METHOD_ADD_DEFAULT_CARD params:postDataDict key:@"DefaultCard"];
}

- (void)deleteButtonTapped:(UIButton *)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Card" message:@"Are you sure to delete the card ?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesaAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSDictionary *currentCardDetail = [cardsArray objectAtIndex:sender.tag];
        
        NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
        [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
        [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
        [postDataDict setValue:[currentCardDetail objectForKey:@"card_id"] forKey:@"card_id"];
        
        [cardsArray removeObjectAtIndex:sender.tag];
        [self callService:METHOD_DELETE_CARD params:postDataDict key:@"DeleteCard"];
    }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:yesaAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
