//
//  AddSubProfileVC.h
//  Streaming
//
//  Created by Aravinth Ramesh on 02/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddSubProfileVC : UIViewController

@property (strong, nonatomic) NSString *deleteORadd;
@property (strong, nonatomic) NSDictionary *subProfileDict;

@end
