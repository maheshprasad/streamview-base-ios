//
//  Webservice.h
//  Royal Water
//
//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIService.h"

@class Webservice;

@protocol WebServiceDelegate <NSObject,NSURLSessionDelegate,NSURLSessionDataDelegate>

-(void) receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice;
-(void) receivedErrorWithMessage:(NSString *)message;

@end

@interface Webservice : NSObject
{
    NSMutableData *receivedData;
}

@property(nonatomic,retain) id<WebServiceDelegate> delegate;

@property int tag;

- (void) executeWebserviceWithMethod:(NSString *) method withValues:(NSString *) values;
-(void) executeWebserviceWithMethodinImage:(NSString *)method withValues:(NSURLRequest *)values ;
//- (void) executeWebserviceWithGetMethod:(NSString *) method;
- (void) executeWebserviceWithMethod1:(NSString *) method withValues:(NSString *) values;

@end
