//
//  MyPlans.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 28/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "MyPlans.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "UtilityClass.h"
#import "MyPlansCell.h"

@interface MyPlans ()<UITableViewDelegate, UITableViewDataSource, CAAnimationDelegate, UITextFieldDelegate>
{
    NSMutableArray *plansArray;
    NSUserDefaults *defaults;
    NSString *currency;
    NSString *stripeAmount;
}

@end

@implementation MyPlans

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"My Plans";
    self.plansTableView.delegate = self;
    self.plansTableView.dataSource = self;
    self.plansTableView.rowHeight = UITableViewAutomaticDimension;
    self.plansTableView.estimatedRowHeight = 350;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    plansArray = [NSMutableArray new];
    defaults = [[NSUserDefaults alloc] init];
    [self loadApi];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    backButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)loadApi{
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:@"0" forKey:@"skip"];
    [self callService:METHOD_SUBSCRIBED_PLANS params:postDataDict key:@"CardList"];
}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return plansArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *planDetails = [plansArray objectAtIndex:indexPath.row];
    
     MyPlansCell *cardCell = (MyPlansCell *)[tableView dequeueReusableCellWithIdentifier:@"MyPlansCell"];
    cardCell.renewalButtonView.hidden = YES;
        if (indexPath.row == 0) {
            if ([[planDetails objectForKey:@"cancelled_status"] intValue] == 1) {
                cardCell.renewalButtonView.hidden = NO;
                [cardCell.renewalButton setTitle:@"Enable Subscription" forState:UIControlStateNormal];
                cardCell.renewalButton.selected = NO;
            }else{
                cardCell.renewalButtonView.hidden = NO;
                [cardCell.renewalButton setTitle:@"Pause Subscription" forState:UIControlStateNormal];
                cardCell.renewalButton.selected = YES;
            }
        }
    
        if ([[planDetails objectForKey:@"popular_status"] integerValue] == 1) {
            cardCell.priceBackView.backgroundColor = [UIColor redColor];
        }else{
            cardCell.priceBackView.backgroundColor = [UIColor blackColor];
        }
    
    cardCell.headingLabel.text = [planDetails objectForKey:@"title"];
    cardCell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[planDetails objectForKey:@"currency"],[planDetails objectForKey:@"amount"]];
    cardCell.expireLabel.text =  [NSString stringWithFormat:@"Expires on %@",[planDetails objectForKey:@"expiry_date"]];
    cardCell.couponAmtLabel.text = [NSString stringWithFormat:@"Coupon amount: %@", [planDetails objectForKey:@"coupon_amount"]];
    cardCell.totalAmtLabel.text = [NSString stringWithFormat:@"Total amount: %@", [planDetails objectForKey:@"total_amount"]];
    cardCell.contentLabel.text = [planDetails objectForKey:@"description"];
    cardCell.durationLabel.text = [NSString stringWithFormat:@"%@ months",[planDetails objectForKey:@"plan"]];
    cardCell.accountCountLabel.text = [NSString stringWithFormat:@"%@ Accounts",[planDetails objectForKey:@"no_of_account"]];
    
    cardCell.renewalButton.tag = indexPath.row;
    [cardCell.renewalButton addTarget:self action:@selector(renewalButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cardCell;
}

- (void)renewalButtonTapped:(UIButton *)sender{
    NSString *alertMessage;
    MyPlansCell *cardCell = [self.plansTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    NSMutableDictionary *postDataDict = [NSMutableDictionary new];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    if (cardCell.renewalButton.selected) {
        alertMessage = @"Want to take a break ? Pause your subscription to take a break on the payment";
    }else{
        alertMessage = @"Do you want to enable auto renewal subscription ?";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirmation" message: alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (cardCell.renewalButton.selected) {
            [cardCell.renewalButton setTitle:@"Enable Subscription" forState:UIControlStateNormal];
            [self callService:METHOD_DISABLE_AUTO_RENEWAL params:postDataDict key:@"AutoRenewal"];
        }else{
            [cardCell.renewalButton setTitle:@"Pause Subscription" forState:UIControlStateNormal];
            [self callService:METHOD_ENABLE_AUTO_RENEWAL params:postDataDict key:@"AutoRenewal"];
        }
    }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:noAction];
    [alert addAction:yesAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                if ([key isEqualToString:@"CardList"]) {
                    plansArray = [dict objectForKey:@"data"];
                    currency = [[plansArray objectAtIndex:0] objectForKey:@"currency"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.plansTableView reloadData];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }else if ([key isEqualToString:@"AutoRenewal"]){
                    [self popup:[dict objectForKey:@"message"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self loadApi];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }
            }
            else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            }
        }
    }];
}

- (void)labelAndClearUpdate{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (plansArray.count == 0) {
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [self.view addSubview:imgeView];
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops! List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [self.view addSubview:lblName];
    }
}

// Pop up
- (void) popup:(NSString *)message{
    if (message == nil) {
        message = @"Done !";
    }
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:spamAlert animated:YES completion:nil];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
