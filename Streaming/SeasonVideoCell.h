//
//  SeasonVideoCell.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 10/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeasonVideoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end
