//
//  WatchFullVideo.h
//  Streaming
//
//  Created by KrishnaDev on 10/10/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WatchFullVideo : UIViewController

@property(strong,nonatomic) NSString *strVideoURL, *strUserType, *strImageURL, *strTitle, *adminVideoID, *seekTime;
@property (nonatomic) BOOL isLive;

- (IBAction)onBack:(id)sender;

@end

