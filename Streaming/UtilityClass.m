//
//  UtilityClass.m
//  Streaming
//
//  Created by Aravinth Ramesh on 17/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "UtilityClass.h"
#import "AppDelegate.h"
#import "LoginView.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"

@interface UtilityClass ()

@end

@implementation UtilityClass
{
    AppDelegate *delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (UtilityClass *)sharedInstance {
    
    static UtilityClass *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (BOOL)isNetworkConnected{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
    
}

- (UIImageView *)setImageWithActivityIndicator:(UIImageView *)imgView urlString:(NSString *)urlString{
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.center = imgView.center;
    self.activityIndicator.hidesWhenStopped = YES;
    
    [self.activityIndicator startAnimating];
    [imgView sd_setImageWithURL:[NSURL URLWithString:urlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.activityIndicator stopAnimating];
    }];
    return imgView;
}

- (void)networkError:(UIViewController *)currentVC{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:APP_NAME message:@"Please check your internet connection." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [currentVC.navigationController popViewControllerAnimated:NO];
    }];
    
    [alert addAction:action];
    [currentVC presentViewController:alert animated:YES completion:nil];
}

- (void)errorHandling:(NSDictionary *)dictResponse VC:(UIViewController *)currentVC{
    NSInteger errorCode = [[dictResponse valueForKey:@"error_code"] integerValue];
    if (errorCode == 133 || errorCode == 3000 || errorCode == 103 || errorCode == 104 || errorCode == 905 || errorCode == 111 || errorCode == 3001)
    {
        [self errorAlert:[dictResponse objectForKey:@"error_messages"] VC:currentVC];
    }else{
        
        [self alertToBack:[dictResponse objectForKey:@"error_messages"] VC:currentVC];
    }
}

- (void)errorAlert:(NSString *)errorMessage VC:(UIViewController *)currentVC {
    
    if (errorMessage == nil){
        errorMessage = @"Server not Connected.";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:APP_NAME message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSUserDefaults *user=[[NSUserDefaults alloc]init];
        [user setObject:@""  forKey:@"id"];
        [user setObject:@""  forKey:@"token"];
        [user synchronize];
        
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Main_Page"];
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:viewController];
        currentVC.view.window.rootViewController = navi;
        [currentVC.view.window makeKeyAndVisible];
        
    }];
    
    [alert addAction:action];
    [currentVC presentViewController:alert animated:YES completion:nil];
}

- (void)alertToBack:(NSString *)message VC:(UIViewController *)currentVC{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //        [currentVC.navigationController popViewControllerAnimated:NO];
        }];
        
        [alert addAction:action];
        [currentVC presentViewController:alert animated:YES completion:nil];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

