//
//  AFNetworkService.h
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceHandler.h"
#import "APIService.h"

typedef void(^CompletionHandler)(NSDictionary *dict, NSURLResponse *response, NSError *error);

typedef void(^responseHandler)(NSDictionary *dict);
typedef void(^errorHandler)(NSError *error);

@interface AFNetworkService : NSObject

+ (AFNetworkService *)sharedInstance;

- (void)sendRequestForImageUpload:(NSString *)service urlParameters:(NSDictionary *)params method:(NSString *)method imageParam:(NSString *)imgParam imageData:(NSData *)imgData withCompletionHandler:(CompletionHandler)myCompletionHandler;

- (void)sendRequestGETandPOST:(NSString *)service postParams:(NSDictionary *)params method:(WebMethod)method withCompletionHandler:(responseHandler)myResponseHandler;

@end
