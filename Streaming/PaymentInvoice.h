//
//  PaymentInvoice.h
//  Streaming
//
//  Created by Aravinth Ramesh on 12/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentInvoice : UIView
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *noteVIew;
@property (strong, nonatomic) IBOutlet UIButton *viewPlansButton;
@property (strong, nonatomic) IBOutlet UIButton *amountPayButton;

@end
