//
//  PaymentInvoiceVCViewController.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 30/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "PaymentInvoiceVC.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "CardsVC.h"
#import "UtilityClass.h"
#import "PaymentCompleteVC.h"
#import "MyPlans.h"
#import "ViewPlansVC.h"
#import "SingleVideoVC.h"

@interface PaymentInvoiceVC ()
{
    NSString *currency;
    NSString *stripeAmount;
    NSUserDefaults *defaults;
    PaymentCompleteVC *completeInvoice;
}
@end
@implementation PaymentInvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentCompleted:) name:@"PaymentCompleted" object:nil];
    self.contentView.layer.cornerRadius = 10.0;
    self.contentView.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = 5.0;
    completeInvoice = [self.storyboard instantiateViewControllerWithIdentifier: @"PaymentCompleteVC"];
    self.bgImageView.image = self.bgImage;
    currency = [self.detailedDict objectForKey:@"currency"];
    self.titleLabel.text = [self.detailedDict objectForKey:@"title"];
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency, [self.detailedDict objectForKey:@"amount"]];
    self.successCodeView.hidden = YES;
    self.couponCodeView.hidden = NO;
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateUI{
    if ([[self.detailedDict objectForKey:@"amount"] isEqualToString:@"0.00"] || [stripeAmount isEqualToString:@"0.00"] || [stripeAmount isEqualToString:@"0"]) {
        self.okButtonView.hidden = NO;
        self.stripePayView.hidden = YES;
    }else{
        self.okButtonView.hidden = YES;
        self.stripePayView.hidden = NO;
    }
}

- (IBAction)closeButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)applyButtonTapped:(id)sender {
    if ([self.couponCodeTF.text isEqualToString:@""]) {
        [self popup:@"Please enter valid code"];
    }else{
        NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
        [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
        [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
        [postDataDict setValue:self.couponCodeTF.text forKey:@"coupon_code"];
        if (self.isPPV) {
        [postDataDict setValue:[self.detailedDict objectForKey:@"admin_video_id"] forKey:@"admin_video_id"];
            [self callService:METHOD_APPLY_COUPON_PPV params:postDataDict key:@"ApplyCoupon"];
        }else{
            [postDataDict setValue:[self.detailedDict objectForKey:@"subscription_id"] forKey:@"subscription_id"];
            [self callService:METHOD_APPLY_COUPON_SUBS params:postDataDict key:@"ApplyCoupon"];
        }
    }
}
- (IBAction)removeButtonTapped:(id)sender {
    self.couponCodeTF.text = @"";
    self.okButtonView.hidden = YES;
    self.stripePayView.hidden = NO;
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@", currency,[self.detailedDict objectForKey:@"amount"]];
    stripeAmount = [self.detailedDict objectForKey:@"amount"];
    self.successCodeView.hidden = YES;
    self.couponCodeView.hidden = NO;
}
- (IBAction)okButtonTapped:(id)sender {
            NSMutableDictionary *postDataDict = [NSMutableDictionary new];
            [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
            [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
            [postDataDict setValue:[self.detailedDict objectForKey:@"subscription_id"] forKey:@"subscription_id"];
            [postDataDict setValue:@"free_plan" forKey:@"payment_id"];
            [postDataDict setValue:@"" forKey:@"coupon_code"];
            [self callService:METHOD_FREE_PLAN_PAY params:postDataDict key:@"FreePlanPay"];
}
- (IBAction)stripePayButtonTapped:(id)sender {
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    if (self.isPPV) {
        [postDataDict setValue:[self.detailedDict objectForKey:@"admin_video_id"] forKey:@"admin_video_id"];
    }else{
        [postDataDict setValue:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
        [postDataDict setValue:[self.detailedDict objectForKey:@"subscription_id"] forKey:@"subscription_id"];
    }
    UIAlertController *payAlert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Are you sure to pay %@%@ for %@",currency, stripeAmount,[self.detailedDict objectForKey:@"title"]] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self.isPPV) {
        [self stripePayRequest:METHOD_STRIPE_PPV params:postDataDict];
        }else{
            [self stripePayRequest:METHOD_STRIPE_PAY_PLAN params:postDataDict];
        }
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    [payAlert addAction:okAction];
    [payAlert addAction:cancelAction];
    [self presentViewController:payAlert animated:YES completion:nil];
}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Please wait..."];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                 if ([key isEqualToString:@"ApplyCoupon"]){
                    stripeAmount = [NSString stringWithFormat:@"%@", [[dict objectForKey:@"data"] objectForKey:@"remaining_amount"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.couponCodeView.hidden = YES;
                        self.successCodeView.hidden = NO;
                        self.couponCodeLabel.text = [[dict objectForKey:@"data"] objectForKey:@"coupon_code"];
                        self.priceLabel.text = [NSString stringWithFormat:@"%@%@",currency, stripeAmount];
                            [self updateUI];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }else if ([key isEqualToString:@"FreePlanPay"]){
                    stripeAmount = @"0.00";
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completeInvoice.price = [NSString stringWithFormat:@"%@%@", currency, stripeAmount];
                        completeInvoice.status = @"Approved";
                        completeInvoice.paymentID = @"Free Plan";
                        completeInvoice.bgImage = self.bgImage;
                        [self presentViewController:completeInvoice animated:false completion:nil];
                        [self viewWillAppear:YES];
                        [self popup: [dict objectForKey:@" message"]];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }
            }
            else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            }
        }
    }];
}

#pragma mark Stripe payment
- (void)stripePayRequest:(NSString *)service params:(NSMutableDictionary *)params {
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                [self dismissViewControllerAnimated:YES completion:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    completeInvoice.price = [NSString stringWithFormat:@"%@%@", currency, stripeAmount];
                    completeInvoice.status = @"Approved";
                    completeInvoice.paymentID = [[dict objectForKey:@"data"] objectForKey:@"payment_id"];
                    [self presentViewController:completeInvoice animated:false completion:nil];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                });
                
            }else{
                CardsVC *addCard = [self.storyboard instantiateViewControllerWithIdentifier:@"CardsVC"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController pushViewController:addCard animated:YES];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    [self popup:[dict objectForKey:@"error_messages"]];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                });
            }
        }
    }];
}

// Pop up
- (void) popup:(NSString *)message{
    if (message == nil) {
        message = @"Done !";
    }
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:spamAlert animated:YES completion:nil];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)paymentCompleted:(NSNotification *)notification{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
