//
//  PaidVideos.m
//  Streaming
//
//  Created by Aravinth Ramesh on 04/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "PaidVideosVC.h"
#import "PaidVideosCell.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import "UtilityClass.h"
#import "SingleVideoVC.h"

@interface PaidVideosVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *cardsArray;
}
@property (strong, nonatomic) IBOutlet UITableView *paidVideosTable;

@end

@implementation PaidVideosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    cardsArray = [NSMutableArray new];
    
    self.paidVideosTable.delegate = self;
    self.paidVideosTable.dataSource = self;
    
    self.paidVideosTable.rowHeight = UITableViewAutomaticDimension;
    self.paidVideosTable.estimatedRowHeight = 250;
    
    self.title = @"Paid Videos";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    
         [self callService:METHOD_PAID_VIDEOS params:postDataDict key:@"PaidVideos"];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    backButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Webservice

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                if ([dict objectForKey:@"success"]) {
                    cardsArray = [dict objectForKey:@"data"];
                    if (cardsArray.count == 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self noData];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.paidVideosTable reloadData];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }
                    
                }else{
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                }
            }

        }];
}

- (void)noData{
    UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
    imgeView.image=[UIImage imageNamed:@"oops.png"];
    [self.view addSubview:imgeView];
    
    UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
    lblName.text=@"Oops! List is empty ";
    lblName.textAlignment=NSTextAlignmentCenter;
    lblName.textColor=[UIColor whiteColor];
    [lblName setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:lblName];
}
#pragma mark TableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cardsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *paidVideoDetail = [cardsArray objectAtIndex:indexPath.row];
    PaidVideosCell *paidVideoCell = (PaidVideosCell *)[tableView dequeueReusableCellWithIdentifier:@"PaidVideosCell"];
    
    if (!paidVideoCell) {
        paidVideoCell = [[PaidVideosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PaidVideosCell"];
    }
    NSString *urlString;
    if ([paidVideoDetail objectForKey:@"picture"] == (id)[NSNull null]) {
        urlString = @"";
    }else{
        urlString = [paidVideoDetail objectForKey:@"picture"];
    }
    if ([paidVideoDetail objectForKey:@"title"] == (id)[NSNull null]) {
        paidVideoCell.titleLabel.text = @"";
    }else{
        paidVideoCell.titleLabel.text = [paidVideoDetail objectForKey:@"title"];
    }
    
    paidVideoCell.videoImageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:paidVideoCell.videoImageView urlString:urlString];
//    [paidVideoCell.videoImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    paidVideoCell.dateLabel.text = [paidVideoDetail objectForKey:@"paid_date"];
    paidVideoCell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[paidVideoDetail objectForKey:@"currency"],[paidVideoDetail objectForKey:@"amount"]];
    paidVideoCell.userTypeLabel.text = [paidVideoDetail objectForKey:@"type_of_user"];
    paidVideoCell.subscriptionTypeLabel.text = [paidVideoDetail objectForKey:@"type_of_subscription"];
    
    return paidVideoCell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *paidVideoDetail = [cardsArray objectAtIndex:indexPath.row];
    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
    singleVideo.adminVideoID = [paidVideoDetail objectForKey:@"admin_video_id"];
    [self.navigationController pushViewController:singleVideo animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
