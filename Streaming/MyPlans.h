//
//  MyPlans.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 28/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlans : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *plansTableView;

@end
