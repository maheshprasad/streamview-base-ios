//
//  SearchBarVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 04/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "SearchBarVC.h"
#import "WebServiceHandler.h"
#import "UIImageView+WebCache.h"
#import "ProgressIndicator.h"
#import "SearchVideosCell.h"
#import "SingleVideoView.h"
#import "UtilityClass.h"
#import "SingleVideoVC.h"

@interface SearchBarVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>
{
    NSString *searchKey;
    NSMutableArray *videosArray;
    NSArray *dataArray;
    NSUserDefaults *defaults;
    NSMutableDictionary *postDataDict;
    NSDictionary *videoDetails;
    
}

@property (strong, nonatomic) IBOutlet UISearchBar *videoSearchBar;

@property (strong, nonatomic) IBOutlet UICollectionView *searchCollectionView;

@end

@implementation SearchBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchKey = @"";
    videosArray = [NSMutableArray new];
    
    self.title = @"Search Videos";
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    self.navigationController.toolbar.tintColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.searchCollectionView.delegate = self;
    self.searchCollectionView.dataSource = self;
    self.videoSearchBar.delegate = self;
    self.videoSearchBar.tintColor = [UIColor whiteColor];
    self.videoSearchBar.barTintColor = [UIColor blackColor];
    
    defaults = [[NSUserDefaults alloc] init];
    postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setObject:@"" forKey:@"key"];
    [self callService:METHOD_SEARCH_VIDEO params:postDataDict];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    backButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
//    if (searchText.length > 0) {
    
        searchKey = searchText;
        [postDataDict setObject:searchKey forKey:@"key"];

        [self callService:METHOD_SEARCH_VIDEO params:postDataDict];
//    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
//    self.navigationController.navigationBar.hidden = NO;
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (void)emptyList{
    
    if (videosArray.count == 0) {
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [self.view addSubview:imgeView];
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops! List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [self.view addSubview:lblName];
    }
    
}


#pragma mark CollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return videosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    videoDetails = [videosArray objectAtIndex:indexPath.row];
    SearchVideosCell *cell = (SearchVideosCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SearchVideosCell" forIndexPath:indexPath];
    
    cell.videoImageView.layer.cornerRadius = 3.0f;
    cell.cellBackView.layer.cornerRadius = 3.0f;
    
    NSURL *imgURL = [NSURL URLWithString:[videoDetails objectForKey:@"default_image"]];
//    [cell.videoImageView sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    
    cell.videoImageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:cell.videoImageView urlString:[videoDetails objectForKey:@"default_image"]];
    
    cell.videoTitleLabel.text = [videoDetails objectForKey:@"title"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    videoDetails = [videosArray objectAtIndex:indexPath.row];
    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
    singleVideo.adminVideoID = [videoDetails objectForKey:@"admin_video_id"];
    [self.navigationController pushViewController:singleVideo animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.searchCollectionView.frame.size.width/3-15, 170);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

#pragma mark Webservice

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    
                    videosArray = [dict objectForKey:@"data"];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (videosArray) {
                            [self.searchCollectionView reloadData];
                        }else{
                            [self popup:@"No videos found"];
                        }
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                    
                }else{
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                }
            }
        }];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:spamAlert animated:YES completion:nil];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
