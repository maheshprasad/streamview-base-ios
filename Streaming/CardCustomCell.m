//
//  CardCustomCell.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "CardCustomCell.h"

@implementation CardCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.backgroundColor = [UIColor colorWithRed:26/255 green:26/255 blue:26/255 alpha:1.0f];
    
    self.cardBackView.layer.shadowOpacity = 0.6f;
    self.cardBackView.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    self.cardBackView.layer.shadowRadius = 2.0f;
    self.cardBackView.layer.cornerRadius = 5.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)defaultCardTapped:(id)sender {
}

- (IBAction)deleteButtonTapped:(id)sender {
}
@end
