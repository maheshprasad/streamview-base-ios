//
//  MyList.m
//  Streaming
//
//  Created by Ramesh on 24/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "MyList.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import "WebServiceHandler.h"
#import "SlideMenuView.h"
#import "UtilityClass.h"
#import "SingleVideoVC.h"

@interface MyList ()<WebServiceDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    AppDelegate *appDelegate;
    NSMutableArray *arrList;
    UIScrollView *scrollView;
    NSDictionary *dictSendVal;
    NSString *strAD_VideoID;
    NSString *strClearID;
    NSDictionary *dictLocal;
    UILabel *lblTitle;
    UIButton *btnClearAll;
    
    NSMutableArray *arrAllList;
    
    UIRefreshControl *refreshControl;
    
    int nSendSkipVal;
    
    UICollectionView *_collectionView;
    int nTotalCount;
}

@end

@implementation MyList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    self.title=appDelegate.strListORHistroyORSpam;
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    nSendSkipVal=12;
    
// Clearall button
    btnClearAll=[UIButton buttonWithType:UIButtonTypeCustom];
    btnClearAll.frame=CGRectMake(self.view.frame.size.width-110, 20,100, 30);
    [btnClearAll setTitle:@"Clear all" forState:UIControlStateNormal];
    [btnClearAll.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [btnClearAll setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    btnClearAll.backgroundColor=[UIColor colorWithRed:94.0f/255.0f green:94.0f/255.0f blue:100.0f/255.0f alpha:1];
    btnClearAll.backgroundColor = [UIColor blackColor];
    [btnClearAll addTarget:self action:@selector(onClearAll:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:btnClearAll];
    btnClearAll.hidden = YES;
    
// Label
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:15]];
    lblTitle.textColor=[UIColor whiteColor];
    [self.view addSubview:lblTitle];
    lblTitle.hidden = YES;
    
    
//    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    scrollView.showsHorizontalScrollIndicator=NO;
//    scrollView.showsVerticalScrollIndicator=NO;
//    [self.view addSubview:scrollView];

    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-110) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
    
    //    refreshControl = [[UIRefreshControl alloc] init];
    //    refreshControl.tintColor = [UIColor whiteColor];
    //    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    //    [_collectionView addSubview:refreshControl];
    
    _collectionView.alwaysBounceVertical=YES;
    _collectionView.showsVerticalScrollIndicator=NO;
    _collectionView.showsHorizontalScrollIndicator=NO;
    
    refreshControl = [UIRefreshControl new];
    
    [refreshControl addTarget:self action:@selector(Refresh:) forControlEvents:UIControlEventValueChanged];
    _collectionView.bottomRefreshControl = refreshControl;
        
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
    arrList = [[NSMutableArray alloc] init];
    [self onCallListWebService];

//    [[UIDevice currentDevice] setValue:
//     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
//                                forKey:@"orientation"];
//     [self.navigationController.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width , 65)];
}
-(void)onCallListWebService

{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:@"0" forKey:@"skip"];

    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Live Videos"]) {
        [postDataDict setValue:@"ios" forKey:@"device_type"];
    }else{
        [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    }
    
    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"]){
        
        [self myWebservice:METHOD_GET_WISHLIST params:postDataDict];
        
    }else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
        
        [self myWebservice:METHOD_SPAMVIDEOSLIST params:postDataDict];
        
    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"]){
        
        [self myWebservice:METHOD_GET_HISTROY params:postDataDict];

    }
//    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Live Videos"]){
//
//        [self myWebservice:METHOD_LIVE_VIDEOS_LIST params:postDataDict];
//
//    }
}

#pragma mark Webservice

- (void)myWebservice:(NSString *)service params:(NSDictionary *)params{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
                
            }else{
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    
                    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"]){
                        
                        [arrList addObjectsFromArray:[dict objectForKey:@"wishlist"]];
                        
                    }else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
                        
                        [arrList addObjectsFromArray:[dict objectForKey:@"data"]];
                        
                    }
                    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"]){
                        
                        [arrList addObjectsFromArray:[dict objectForKey:@"history"]];
                    }
//                    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Live Videos"]){
//
//                        [arrList addObjectsFromArray:[dict objectForKey:@"livevideos"]];
//                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_collectionView reloadData];
                        [self labelAndClearUpdate];
                        
                    });
                }else{
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                }
            }
 
        }];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

}

- (void)clearAllService:(NSMutableDictionary *)params andService:(NSString *)service{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    
                    [arrList removeAllObjects];
                    [self popup:[dict objectForKey:@"message"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_collectionView reloadData];
                        [self labelAndClearUpdate];
                    });
                }else{
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                }
            }
            
        }];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
}

- (void) labelAndClearUpdate{
    
    if (arrList.count!=0)
    {
        btnClearAll.hidden = NO;
        lblTitle.hidden = NO;
//        lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
        
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            lblTitle.text=@"My List";
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
            lblTitle.text = @"History";
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"])
            lblTitle.text=@"Spam";
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Live Videos"])
            lblTitle.text = @"Live videos";
        
//        [lblTitle setFont:[UIFont boldSystemFontOfSize:15]];
//        lblTitle.textColor=[UIColor whiteColor];
//        [self.view addSubview:lblTitle];
        
//        [_collectionView reloadData];
    }
    else
    {
        btnClearAll.hidden = YES;
        lblTitle.hidden = YES;
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [self.view addSubview:imgeView];
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops! List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [self.view addSubview:lblName];
    }
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

#pragma mark -- collectionview data source and Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrList.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    //cell.backgroundColor=[UIColor greenColor];
    
    dictLocal=[arrList objectAtIndex:indexPath.row];
    
    [[cell viewWithTag:123] removeFromSuperview];   // REMOVING PREVIOUS CONTENT VIEW
    
    UIView *viewContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
    viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewContent.layer.shadowOpacity = 0.7f;
    viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    viewContent.layer.shadowRadius = 1.0f;
    viewContent.layer.cornerRadius = 2.0f;
    viewContent.tag=123;
    [cell addSubview:viewContent];
    
//    if ([appDelegate.strListORHistroy isEqualToString:@"My List"])
//        cell.tag=[[dictLocal valueForKey:@"wishlist_id"]intValue];
//    else
//        cell.tag=[[dictLocal valueForKey:@"history_id"]intValue];
//    
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,140)];
    imageView.layer.cornerRadius = 2.0f;
    imageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:imageView urlString:[dictLocal valueForKey:@"default_image"]];
//    [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLocal valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [viewContent addSubview:imageView];
    
    UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
    imgPlay.image=[UIImage imageNamed:@"play.png"];
    //imgPlay.alpha=0.6;
//    [viewContent addSubview:imgPlay];
    
    UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
    btnClick.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
    [viewContent addSubview:btnClick];
    
    //Single tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
    tapGesture.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [btnClick addGestureRecognizer:tapGesture];
    
    //Long press
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
                if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
                    cell.tag=[[dictLocal valueForKey:@"admin_video_id"] intValue];
                else
                    cell.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
  //  longPress.view.tag=[[dictLocal valueForKey:@"admin_video_id"]intValue];
    longPress.minimumPressDuration = 1;
    [cell addGestureRecognizer:longPress];
    
    UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 140, viewContent.frame.size.width-10, 20)];
    lblTitle.text=[dictLocal valueForKey:@"title"];
    [lblTitle setFont:[UIFont systemFontOfSize:15.0f]];
    lblTitle.textColor=[UIColor lightGrayColor];
    lblTitle.lineBreakMode = NSLineBreakByClipping;
    [viewContent addSubview:lblTitle];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3-15, 140+30);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    
    return 0;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)Refresh:(UIRefreshControl *)refreshControl1
{
    
    if (nTotalCount>nSendSkipVal)
    {
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=4;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"skip\":\"%d\"}",strID,strToken,nSendSkipVal];
        
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            [service executeWebserviceWithMethod:METHOD_GET_WISHLIST withValues:strSend];
        else
            [service executeWebserviceWithMethod:METHOD_GET_HISTROY withValues:strSend];
        
        nSendSkipVal+=12;
    }
    else
    {
        [refreshControl1 endRefreshing];
        
    }
}

-(IBAction)onBtnClick:(id)sender
{
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
    singleVideo.adminVideoID = strVideoID;
    [self.navigationController pushViewController:singleVideo animated:YES];
    }

-(IBAction)onClearAll:(id)sender
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
    
    [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
    [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
    [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [paramsDict setObject: @"" forKey:@"admin_video_id"];
    
    [paramsDict setObject:@"1" forKey:@"status"];  // Clear all


    if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
    {

        [self clearAllService:paramsDict andService:METHOD_DELETE_LIST];

    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
    {

        [self clearAllService:paramsDict andService:METHD_CLEAR_HISTORY];

    }
    else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"])
    {

        [self clearAllService:paramsDict andService:METHOD_REMOVE_SPAM];

    }

}

- (void) popup:(NSString *)message{
    
    if (message == nil) {
        message = @"Done !";
    }
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}
- (void)handleLongPress:(UILongPressGestureRecognizer*)sender

{
    //NSLog(@"%ld", sender.view.tag);
    
   // UIButton *btn=(UIButton *) sender;
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
    
    [paramsDict setObject:[defaults objectForKey:@"id"] forKey:@"id"];
    [paramsDict setObject:[defaults objectForKey:@"token"] forKey:@"token"];
    [paramsDict setObject:[defaults objectForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
//    [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"admin_video_id"];
    
   UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    strClearID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
      
        if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
        {
            [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"wishlist_id"];

            [self removeSingleVideo:METHOD_DELETE_LIST params:paramsDict];
            
        }
        
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
            
            [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"admin_video_id"];

            [self removeSingleVideo:METHOD_REMOVE_SPAM params:paramsDict];

        }
        else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"History"])
        {
            [paramsDict setObject:[dictLocal valueForKey:@"admin_video_id"] forKey:@"admin_video_id"];

            [self removeSingleVideo:METHOD_DELETE_HISTORY params:paramsDict];
            
        }
    }];
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }];
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
}

- (void)removeSingleVideo:(NSString *)service params:(NSMutableDictionary *)params {
    
    __block NSError *error;

    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if ([[dict objectForKey:@"success"] intValue] == 1) {
            
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"admin_video_id"]]]) {
                        
                        [arrList removeObjectAtIndex:i];
                    }
                }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_collectionView reloadData];
                [self labelAndClearUpdate];
                [self popup:[dict objectForKey:@"message"]];
            });
        }
        
    }];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
     [refreshControl endRefreshing];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
     [refreshControl endRefreshing];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            nTotalCount=[[dictResponse valueForKey:@"total"] intValue];
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"]){
                  [arrList addObjectsFromArray:[dictResponse valueForKey:@"wishlist"]];
            }else if ([appDelegate.strListORHistroyORSpam isEqualToString:@"Spam"]){
                
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"spam"]];
                
            }
            else{
                
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"history"]];

            }
            
            [self labelAndClearUpdate];
            
        }
        else
        {
            
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            //strClearID
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
            {
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];
                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"wishlist_id"]]]) {
                        
                         [arrList removeObjectAtIndex:i];
                        
                    }
                }
               
            }
            else
            {
                for (int i=0;i<arrList.count;i++)// NSDictionary *dictLocal in arrList)
                {
                    NSDictionary *dictLocal=[arrList objectAtIndex:i];
                    if ([strClearID isEqualToString:[NSString stringWithFormat:@"%@",[dictLocal valueForKey:@"history_id"]]]) {
                        
                        [arrList removeObjectAtIndex:i];
                    }
                }
            }
            [_collectionView reloadData];
           // [self onCallListWebService];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            if ([appDelegate.strListORHistroyORSpam isEqualToString:@"My List"])
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"wishlist"]];
            else
                [arrList addObjectsFromArray:[dictResponse valueForKey:@"history"]];
            [_collectionView reloadData];

        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     if ([[segue identifier] isEqualToString:@"singleVideo"])
     {
         SingleVideoView *singleVideo=[segue destinationViewController];
         singleVideo.strVideoID=strAD_VideoID;
     }
 }


@end
