    //
//  AppDelegate.m
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <OpenGLES/EAGLIOSurface.h>
#import "Reachability.h"
#import "APIService.h"

#import <Stripe/Stripe.h>

#define thumbSize CGSizeMake(130, 150)

@interface AppDelegate (){
}

@end

@implementation AppDelegate
@synthesize strDeviceToken,strCategroryID,strCategoryName,strListORHistroyORSpam,strExpiredMsg,arrMenuCategoryList,strPlayerIdenty,strAlertTitle;

@synthesize shouldRotate;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // StatusBar Color
    UIView *statusBar = (UIView *)[[UIApplication sharedApplication] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(backgroundColor)]) {
        statusBar.backgroundColor = [UIColor blackColor];
    }
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

    
    // Stripe Payment
    
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_uDYrTXzzAuGRwDYtu7dkhaF3"];

    [[STPTheme defaultTheme] setAccentColor:[UIColor whiteColor]];
    [[STPTheme defaultTheme] setPrimaryBackgroundColor:[UIColor colorWithRed:10/255 green:10/255  blue:10/255  alpha:1]];
    [[STPTheme defaultTheme] setSecondaryBackgroundColor:[UIColor blackColor]];
    [[STPTheme defaultTheme] setPrimaryForegroundColor:[UIColor whiteColor]];
    [[STPTheme defaultTheme] setPrimaryForegroundColor:[UIColor whiteColor]];


    [[STPTheme defaultTheme] errorColor];
    strDeviceToken=@"e4824fc8e39e24be79efa8ca84afa73b3d58739f8c715d7bf599a76da91263ae";
    strCategoryName=@"";
    strCategroryID=@"";
    strExpiredMsg=@"Oops! Your session is expired,Please login again.";
    strPlayerIdenty=@"";
    strAlertTitle= APP_NAME;
    
//     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //[[UILabel appearance] setFont:[UIFont fontWithName:@"Zapfino" size:15.0]];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    NSString *strUserID=[defaults valueForKey:@"id"];
  
    if([strUserID length] != 0)//if (![strUserName isEqualToString:@""]||!(strUserName.length) )
    {
        
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController * navController=[[UINavigationController alloc]initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"Home"]];
            self.window.rootViewController = navController;
            [self.window makeKeyAndVisible];

    }
    
    
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
#ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
                                                                                             | UIUserNotificationTypeBadge
                                                                                             | UIUserNotificationTypeSound) categories:nil];
        [application registerUserNotificationSettings:settings];
#endif
    }
    else
    {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }

    [FBSDKLoginButton class];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];

    
    return YES;
}
- (void)applicationDidChangeStatusBarOrientation:(NSNotification *)notification
{
    [UIViewController attemptRotationToDeviceOrientation];
}


-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if (self.shouldRotate)
        return UIInterfaceOrientationMaskAll;
    else
        return UIInterfaceOrientationMaskPortrait;
}
-(void)onExpriedPage
{
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Main_Page"];
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = navi;
    [self.window makeKeyAndVisible];
    
  
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    //NSLog(@"%@",sourceApplication);
    
    if ([[GIDSignIn sharedInstance] handleURL:url
                            sourceApplication:sourceApplication
                                   annotation:annotation])
    {
        return YES;
    }
    else if([[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:sourceApplication
                                                        annotation:annotation])
    {
        return  YES;
    }
    
    return  NO;
}
//-(void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
//{
//
//}


- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options
{
    
    if( [[GIDSignIn sharedInstance] handleURL:url
                            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                   annotation:options[UIApplicationOpenURLOptionsAnnotationKey]])
    {
        return YES;
    }
    else if ([[FBSDKApplicationDelegate sharedInstance] application:app
                                                            openURL:url
                                                  sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                         annotation:options[UIApplicationOpenURLOptionsAnnotationKey]])
    {
        return YES;
    }
    
    return NO;
}


-(BOOL)application:(UIApplication *)application processOpenURLAction:(NSURL*)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation iosVersion:(int)version
{
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}

#pragma mark -- Notifoction

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // NS_AVAILABLE_IOS(8_0);
{
    [application registerForRemoteNotifications];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //NSLog(@"deviceToken: %@", deviceToken);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    strDeviceToken=token;
    //NSLog(@"%@",strDeviceToken);
    
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //NSLog(@"Did Fail to Register for Remote Notifications");
    //NSLog(@"%@, %@", error, error.localizedDescription);
    
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey:@"badgecount"] integerValue];
    // [arrnotiList addObject:userInfo];
    
    //   dictNotificationVal= [[userInfo objectForKey:@"aps"] objectForKey:@"message"];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        //[PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
        
        //        self.is_from_push = @"yes";
        //        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
        //        NSString *strUserID=[defaults valueForKey:@"id"];
        //        NSString *strLtype=[defaults valueForKey:@"LoginType"];
        
        
        //        NSString * storyboardName = @"Main";
        //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        //        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Main_Page"];
        //        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:viewController];
        //        self.window.rootViewController = navi;
        //        [self.window makeKeyAndVisible];
          
        
    }
    else if (state == UIApplicationStateActive) {
        NSString *cancelTitle = @"OK";
        // NSString *showTitle = @"Show";
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:strAlertTitle
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:cancelTitle
                                                  otherButtonTitles:nil];
        
        //[alertView setTag:101];
        [alertView show];
        
        
        //[alertView release];
    } else {
        //Do stuff that you would do if the application was not active
    }
    // //NSLog(@"%@",arrnotiList);
    //NSLog(@"%ld",(long)[UIApplication sharedApplication].applicationIconBadgeNumber);
}


@end
