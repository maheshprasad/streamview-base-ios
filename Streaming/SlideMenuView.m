//
//  SlideMenuView.m
//  Royal Water
//
//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import "SlideMenuView.h"
#import "AppDelegate.h"
#import "ExpandableTableViewCell.h"
#import "ProgressIndicator.h"
#import "SWRevealViewController.h"
#import "ManageProfileVC.h"
#import "CardsVC.h"
#import "WebServiceHandler.h"
#import "UtilityClass.h"
#import "UIImageView+WebCache.h"

@interface SlideMenuView ()
{
    NSMutableArray *arrUserMenuList;
    AppDelegate *appDelegate;
    NSArray *arrGetCategory;
    NSUserDefaults *defaults;
    
}
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIImageView *sideMenuLogo;
@property (strong, nonatomic) IBOutlet UIButton *addProfileButton;
@property (strong, nonatomic) IBOutlet UIButton *profilePic;

@end

@implementation SlideMenuView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    defaults = [[NSUserDefaults alloc]init];
    
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];

    self.backButton.frame = CGRectMake(10, 20, 25, 25);
    self.sideMenuLogo.frame = CGRectMake(self.backButton.frame.origin.x+30, self.backButton.frame.origin.y-5, 70, 35);
    
    // Hide side menu
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.backButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    NSString *strName=[defaults valueForKey:@"name"];
    NSString *strPic=[defaults valueForKey:@"picture"];
    
    if ([strName length]!=0)
        lblProfileName.text=strName;
    
    if ([strPic length]!=0)
    {
        [imgProfile sd_setImageWithURL:[NSURL URLWithString:strPic] placeholderImage:[UIImage imageNamed:@"Default.png"]];
    }
    self.addProfileButton.frame = CGRectMake(imgProfile.frame.origin.x+120, imgProfile.frame.origin.y, 30, 30);
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

    arrUserMenuList=[[NSMutableArray alloc]init];
    [self onCreateMenuItems];
}

- (void)viewWillLayoutSubviews{
    //  UserPart
    
    imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2;
    imgProfile.clipsToBounds = YES;
    
    [imgProfile.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [imgProfile.layer setBorderWidth: 2.0];
}

- (IBAction)backButtonTapped:(id)sender {

}

- (IBAction)addProfileTapped:(id)sender {
    
    ManageProfileVC *manageProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageProfileVC"];
    [self.navigationController pushViewController:manageProfile animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEditProfile:(id)sender
{
    [self performSegueWithIdentifier:@"profileEdit" sender:self];
}
-(void)onCreateMenuItems
{
    NSMutableArray *arrLocal=[[NSMutableArray alloc]init];
    arrLocal=[NSMutableArray arrayWithArray:appDelegate.arrMenuCategoryList];
    for (int i=0; i<arrLocal.count; i++)
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[arrLocal objectAtIndex:i];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"1" forKey:@"level"];
        [arrLocal replaceObjectAtIndex:i withObject:newDict];
    }

    NSArray *arrMenu;
    
    if ([[defaults objectForKey:@"payment_subscription"] intValue]) {
        arrMenu = @[@{@"name":@"Home",@"ImageURL":@"home_new.png",@"level":@"0"},@{@"name":@"My List",@"ImageURL":@"heart_outline_new.png",@"level":@"0"},@{@"name":@"Categories",@"ImageURL":@"star_outline_new.png",@"SubMenu":arrLocal},@{@"name":@"History",@"ImageURL":@"history_new.png",@"level":@"0"},@{@"name":@"Spam",@"ImageURL":@"spam_new.png",@"level":@"0"},@{@"name":@"Cards",@"ImageURL":@"credit_card_new.png",@"level":@"0"},@{@"name":@"Settings",@"ImageURL":@"settings_new.png",@"level":@"0"},@{@"name":@"Logout",@"ImageURL":@"logout_new.png",@"level":@"0"}];
    }else{
        arrMenu = @[@{@"name":@"Home",@"ImageURL":@"home_new.png",@"level":@"0"},@{@"name":@"My List",@"ImageURL":@"heart_outline_new.png",@"level":@"0"},@{@"name":@"Categories",@"ImageURL":@"star_outline_new.png",@"SubMenu":arrLocal},@{@"name":@"History",@"ImageURL":@"history_new.png",@"level":@"0"},@{@"name":@"Spam",@"ImageURL":@"spam_new.png",@"level":@"0"},@{@"name":@"Settings",@"ImageURL":@"settings_new.png",@"level":@"0"},@{@"name":@"Logout",@"ImageURL":@"logout_new.png",@"level":@"0"}];
    }

    arrUserMenuList=[NSMutableArray arrayWithArray:arrMenu];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.menuTableView reloadData];
    });
}



#pragma mark
#pragma mark - Table view data sources


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return arrUserMenuList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *Title = [[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    
     return [self createCellWithTitle:Title image:[[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"ImageURL"] indexPath:indexPath];
    

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSString *CellIdentifier = [arrUserMenuList objectAtIndex:indexPath.row];
    
    NSDictionary *dic=[arrUserMenuList objectAtIndex:indexPath.row];
    if([dic valueForKey:@"SubMenu"])
    {
        NSArray *arr=[dic valueForKey:@"SubMenu"];
        BOOL isTableExpanded=NO;
        
        for(NSDictionary *subitems in arr )
        {
            NSInteger index=[arrUserMenuList indexOfObjectIdenticalTo:subitems];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [arrUserMenuList insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }

    if ([[dic valueForKey:@"name"] isEqualToString:@"Logout"])
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:APP_NAME message:@"Are you sure to logout ?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
            [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
            [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
            
            [self callService:METHOD_LOGOUT params:postDataDict key:@"logout"];
        }];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:noAction];
        [alert addAction:yesAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Home"])
    {
        
        [self performSegueWithIdentifier:@"Home" sender:self];
    }
//    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Live Videos"])
//    {
//
//        appDelegate.strListORHistroyORSpam=@"Live Videos";
//        [self performSegueWithIdentifier:@"myList" sender:self];
//    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"My List"])
    {
        appDelegate.strListORHistroyORSpam=@"My List";
        [self performSegueWithIdentifier:@"myList" sender:self];
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"History"])
    {
        appDelegate.strListORHistroyORSpam=@"History";
        [self performSegueWithIdentifier:@"myList" sender:self];
       
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Settings"])
    {
        
        [self performSegueWithIdentifier:@"Settings" sender:self];
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Share"])
    {
        NSString *string = [NSString stringWithFormat:@"%@ Invited you to install StreamFlix", @"Ramesh"];
        
        // NSURL *URL = [NSURL URLWithString:@"www.apppoets.com/"];
        
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com"]];
        
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc]
                                                            initWithActivityItems:@[string,URL]
                                                            applicationActivities:nil];
        
        [activityViewController setCompletionWithItemsHandler:^(NSString *activityType,
                                                                
                                                                BOOL completed,
                                                                
                                                                NSArray *returnedItems,
                                                                
                                                                NSError * error){
            
            if ( completed )
            {
                //  TODO error handling
                //NSLog(@"share complete");
                //            NSExtensionItem* extensionItem = [returnedItems firstObject];
                //            NSItemProvider* itemProvider = [[extensionItem attachments] firstObject];
                
            } else {
                
                //NSLog(@"canceld");
                
            }
            
        }];
        
        if ( [activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
            
            // iOS8
            activityViewController.popoverPresentationController.sourceView = self.view;
            
            //        activityViewController.popoverPresentationController.sourceRect = self.view.bounds;
        }
        
        [self presentViewController:activityViewController animated:YES completion:nil];
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Categories"])
    {
        
    }
    else  if ([[dic valueForKey:@"name"] isEqualToString:@"Spam"])
    {
        appDelegate.strListORHistroyORSpam=@"Spam";
        [self performSegueWithIdentifier:@"myList" sender:self];
        
    }
    else if ([[dic valueForKey:@"name"] isEqualToString:@"Cards"]){
        CardsVC *cards = [self.storyboard instantiateViewControllerWithIdentifier:@"CardsVC"];
        cards.title = @"Cards";
        [self.navigationController pushViewController:cards animated:YES];
    }
    else
    {
        appDelegate.strCategroryID=[dic valueForKey:@"id"];
        appDelegate.strCategoryName=[dic valueForKey:@"name"];
        [self performSegueWithIdentifier:@"Sub_Menu" sender:self];
    }

    
}
- (UITableViewCell*)createCellWithTitle:(NSString *)title image:(NSString *)image  indexPath:(NSIndexPath*)indexPath
{
    NSString *CellIdentifier = @"Cell";
    ExpandableTableViewCell* cell = [self.menuTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = bgView;
    cell.backgroundColor=[UIColor clearColor];
    cell.lblTitle.text = title;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.imgIcon.image=[UIImage imageNamed:image];
    
    [cell setIndentationLevel:[[[arrUserMenuList objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    cell.indentationWidth = 25;
    
    float indentPoints = cell.indentationLevel * cell.indentationWidth;
    
    cell.contentView.frame = CGRectMake(indentPoints,cell.contentView.frame.origin.y,cell.contentView.frame.size.width - indentPoints,cell.contentView.frame.size.height);
    
    NSDictionary *d1=[arrUserMenuList objectAtIndex:indexPath.row] ;
    
    if([d1 valueForKey:@"SubMenu"])
    {
        cell.btnExpand.alpha = 1.0;
        [cell.btnExpand addTarget:self action:@selector(showSubMenu:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.btnExpand.alpha = 0.0;
    }
    return cell;
}

-(void)showSubMenu :(id) sender
{
    UIButton *btn = (UIButton*)sender;
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.menuTableView];
    NSIndexPath *indexPath = [self.menuTableView indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    NSDictionary *d=[arrUserMenuList objectAtIndex:indexPath.row] ;
    NSArray *arr=[d valueForKey:@"SubMenu"];
    if([d valueForKey:@"SubMenu"])
    {
        BOOL isTableExpanded=NO;
        for(NSDictionary *SubMenu in arr )
        {
            NSInteger index=[arrUserMenuList indexOfObjectIdenticalTo:SubMenu];
            isTableExpanded=(index>0 && index!=NSIntegerMax);
            if(isTableExpanded) break;
        }
        
        if(isTableExpanded)
        {
            [self CollapseRows:arr];
        }
        else
        {
            NSUInteger count=indexPath.row+1;
            NSMutableArray *arrCells=[NSMutableArray array];
            for(NSDictionary *dInner in arr )
            {
                [arrCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
                [arrUserMenuList insertObject:dInner atIndex:count++];
            }
            [self.menuTableView insertRowsAtIndexPaths:arrCells withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
    
    
}
-(void)CollapseRows:(NSArray*)ar
{
    for(NSDictionary *dInner in ar )
    {
        NSUInteger indexToRemove=[arrUserMenuList indexOfObjectIdenticalTo:dInner];
        NSArray *arInner=[dInner valueForKey:@"SubMenu"];
        if(arInner && [arInner count]>0)
        {
            [self CollapseRows:arInner];
        }
        
        if([arrUserMenuList indexOfObjectIdenticalTo:dInner]!=NSNotFound)
        {
            [arrUserMenuList removeObjectIdenticalTo:dInner];
            [self.menuTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                        [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                        ]
                                      withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([dict objectForKey:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [defaults setObject:@""  forKey:@"id"];
                    [defaults setObject:@""  forKey:@"token"];
                    [defaults synchronize];
                    [appDelegate onExpriedPage];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                });
            }else{
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
    }];
}



@end
