//
//  SingleVideoView.m
//  Streaming
//
//  Created by Ramesh on 23/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "SingleVideoView.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import <MediaPlayer/MediaPlayer.h>
#import "HCSStarRatingView.h"
#import "UIImageView+WebCache.h"
#import "YTPlayerView.h"
#import "WatchFullVideo.h"
#import "WebServiceHandler.h"
#import "SlideMenuView.h"
#import "SWRevealViewController.h"
#import "HomeView.h"
#import "ViewPlansVC.h"
#import "InvoiceView.h"
#import "WatchFullVideo.h"
#import "WebServiceHandler.h"
#import "PaymentInvoice.h"
#import "SearchBarVC.h"
#import "UtilityClass.h"

#import <AVKit/AVKit.h>

@interface SingleVideoView ()<WebServiceDelegate,UIGestureRecognizerDelegate,YTPlayerViewDelegate,UITextViewDelegate,UINavigationBarDelegate, CAAnimationDelegate, AVPlayerViewControllerDelegate>
{
    AppDelegate *appDelegate;
    
    MPMoviePlayerController *moviePlayer;
    UIWebView *videoView;
    UIScrollView *scrollView;
    UIView *viewcontent1;
    UILabel *lblRatings;
    NSString *strRating;
    UITextView *txtComment;
    NSString *strAdmin_VideoId;
    NSDictionary *dictGetValue;
    NSTimer *timerHide;
    YTPlayerView *playerView;
    
//    UIButton *likeButton;
//    UILabel *likesLabel;
    
    int nWatchHistory;
    
    int pay_per_view_status;
    int is_ppv_subscribe_page;
    int like_dislike;
    
    InvoiceView *invoice;
    PaymentInvoice *selectPaymentView;
    UIVisualEffectView *blurEffectView;
    NSUserDefaults *defaults;
    NSString *trailerORfullVideo;
    UIView *gradientViewBottom;
    UIView *gradientViewTop;
    
    UILabel *lblTitle;
    AVPlayer *player;
    UIImageView *defaultImage;
    AVPlayerViewController *controller;
    NSString *stripeAmount;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;
//@property (nonatomic) JWPlayerController *player;
//@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;
@end

@implementation SingleVideoView
@synthesize strVideoID;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    like_dislike = 0;
    
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setShouldRotate:NO];
    
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    
    strRating=@"";
    nWatchHistory=0;
    
    
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

-(void)onCallWebService
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=3;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strVideoID];
    
    [service executeWebserviceWithMethod:METHOD_SINGLE_VIDEO withValues:strSend];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [self onCallWebService];
    
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                forKey:@"orientation"];
    
}

- (void) orientationChanged:(NSNotification *)note
{
    
    UIDevice * device = note.object;
    switch(device.orientation)
    {
            case UIDeviceOrientationPortrait:
            /* start special animation */
            //            [self.navigationController.navigationBar setFrame:CGRectMake(0, 10, self.view.frame.size.width , 50)];
            
            break;
            
            case UIDeviceOrientationPortraitUpsideDown:
            /* start special animation */
            //            [self.navigationController.navigationBar setFrame:CGRectMake(0, 10, self.view.frame.size.width , 50)];
            
            break;
            
        default:
            break;
    };
}

-(void)viewWillDisappear:(BOOL)animated
{
    [timerHide invalidate];
    timerHide=nil;
    [playerView pauseVideo];
    //    [_player pause ];
    [player pause];
    
    //    self.navigationController.navigationBar.translucent = NO;
    //
    ///[[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [appDelegate setShouldRotate:NO];
    
}

-(void)HideNavBar
{
    ////     Hide
    //     self.navigationController.navigationBar.hidden=YES;
    //    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

//Delegate Method

-(void)onDisplayClick:(UIButton *)sender
{
    //NSLog(@"clicked");
    sender.tag = 0; // Trailer video
    
    if(nWatchHistory==0)
    
    // Hide
    
    if (self.navigationController.navigationBar.hidden == NO)
    {
        self.navigationController.navigationBar.hidden=YES;
        // hide the Navigation Bar
        //  [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    // if Navigation Bar is already hidden
    else if (self.navigationController.navigationBar.hidden == YES)
    {
        // Show the Navigation Bar
        
        self.navigationController.navigationBar.hidden=NO;
        
        //    [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        [timerHide invalidate];
        timerHide=nil;
        
        timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    }
    
}
- (void)dismissKeyBoard
{
    [viewcontent1 removeFromSuperview];
    [txtComment resignFirstResponder];
}
- (void)onPageLoad
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO]; // Edited
    
    for (UIView *v in scrollView.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
    int xPos=0;
    int yPos=0;
    
    NSDictionary *dictValue = [dictGetValue valueForKey:@"video"];
    //
    //    [timerHide invalidate];
    //    timerHide=nil;
    //
    //    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    //
    int nVideoType=[[dictValue valueForKey:@"video_type"] intValue];
    if(nVideoType==2)
    {
        [self playInYouTubePlayer];
        
    }
    else
    {
        
        [self playInAVPlayer];
    }
    
    // yPos+=self.view.frame.size.height/3+10;
    
    int nHeight=self.view.frame.size.height-(self.view.frame.size.height/3+50);
    
    int startPostion=self.view.frame.size.height/3+50;
    
    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, startPostion, self.view.frame.size.width, nHeight)];
    scrollView.showsHorizontalScrollIndicator=NO;
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    
    //Hide
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width-35, 30)];
    lblTitle.text=[dictValue valueForKey:@"title"];
    [lblTitle setFont:[UIFont boldSystemFontOfSize:20]];
    lblTitle.lineBreakMode = NSLineBreakByClipping;
    // [lblTitle setFont:[UIFont fontWithName:@"Helvetica Neue-Bold" size:20]];
    //    lblTitle.textColor=[UIColor colorWithRed:255.0f/255.0f green:154.0f/255.0f blue:3.0f/255.0f alpha:1];
    lblTitle.textColor = [UIColor whiteColor];
    [scrollView addSubview:lblTitle];
    [scrollView bringSubviewToFront:lblTitle];
    
    // Report Spam
    
    UIButton *reportSpam=[UIButton buttonWithType:UIButtonTypeCustom];
    reportSpam.frame=CGRectMake(self.view.frame.size.width-35, yPos,30, 30);
    [reportSpam setImage:[UIImage imageNamed:@"dots_vertical.png"] forState:UIControlStateNormal];
    reportSpam.clipsToBounds=YES;
    [reportSpam addTarget:self action:@selector(reportSpamTapped:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:reportSpam];
    
    yPos+=35;
    
    // Likes count Label
//    likesLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, yPos, 55, 20)];
//    likesLabel.text = [NSString stringWithFormat:@"%@ Likes", [dictGetValue valueForKey:@"likes"]];
//    likesLabel.lineBreakMode = NSLineBreakByClipping;
//    [likesLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:12]];
//    //    lblTitle.textColor=[UIColor colorWithRed:255.0f/255.0f green:154.0f/255.0f blue:3.0f/255.0f alpha:1];
//    likesLabel.textColor = [UIColor greenColor];
//    [scrollView addSubview:likesLabel];
    
    // Time Label
    
//    UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(likesLabel.frame.origin.x+likesLabel.frame.size.width, yPos, 100, 20)];
//    timeLabel.text = [NSString stringWithFormat:@"%@", [dictValue valueForKey:@"publish_time"]];
//    timeLabel.lineBreakMode = NSLineBreakByClipping;
//    [timeLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:12]];
//    //    lblTitle.textColor=[UIColor colorWithRed:255.0f/255.0f green:154.0f/255.0f blue:3.0f/255.0f alpha:1];
//    timeLabel.textColor = [UIColor whiteColor];
//    [scrollView addSubview:timeLabel];
    
    yPos+=25;
    
    UITextView *txtDescri=[[UITextView alloc]initWithFrame:CGRectMake(10, yPos, self.view.frame.size.width-20, 30)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineHeightMultiple = 20.0f;
    paragraphStyle.maximumLineHeight = 20.0f;
    paragraphStyle.minimumLineHeight = 20.0f;
    
    NSString *string = [dictValue valueForKey:@"description"];
    NSDictionary *ats = @{
                          NSFontAttributeName : [UIFont fontWithName:@"Helvetica Neue" size:14.0f],
                          NSParagraphStyleAttributeName : paragraphStyle,
                          };
    
    txtDescri.attributedText = [[NSAttributedString alloc] initWithString:string attributes:ats];
    
    
    //    txtDescri.text=[dictValue valueForKey:@"description"];
    txtDescri.userInteractionEnabled=NO;
    // [txtDescri setFont:[UIFont systemFontOfSize:13]];
    txtDescri.backgroundColor=[UIColor clearColor];
    txtDescri.textColor=[UIColor whiteColor];
    //    [txtDescri setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
    [scrollView addSubview:txtDescri];
    
    [txtDescri sizeToFit];
    
    yPos+=txtDescri.frame.size.height+10;
    
    
    HCSStarRatingView *VIEW=[[HCSStarRatingView alloc]initWithFrame:CGRectMake(20,yPos,120,20)];
    //VIEW.text=[dictValue valueForKey:@"Ratings"];
    VIEW.allowsHalfStars=YES;
    VIEW.backgroundColor=[UIColor clearColor];
    VIEW.tag=111;
    VIEW.userInteractionEnabled=NO;
    VIEW.emptyStarImage=[UIImage imageNamed:@"star-with-empty.png"];
    VIEW.filledStarImage=[UIImage imageNamed:@"star-with-white.png"];
    VIEW.halfStarImage=[UIImage imageNamed:@"half-star-with-white.png"];
    
    //    VIEW.emptyStarImage =[UIImage imageNamed:@"star.png"];
    //    VIEW.filledStarImage =[UIImage imageNamed:@"star1.png"];
    VIEW.value=[[dictValue valueForKey:@"ratings"]floatValue];
    // VIEW.tag=MUSIC;
    // [VIEW addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [scrollView addSubview:VIEW];
    
    // Like Button
    
//    likeButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    likeButton.frame=CGRectMake(VIEW.frame.size.width+30, yPos-5,30, 30);
//    likeButton.clipsToBounds=YES;
//    [likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//
//    if ([[dictGetValue objectForKey:@"is_liked"] intValue] == 0) {
//        [likeButton setImage:[UIImage imageNamed:@"thumb_up_white.png"] forState:UIControlStateNormal];
//        likeButton.tag = 0;
//    }else{
//        [likeButton setImage:[UIImage imageNamed:@"thumb_down_white.png"] forState:UIControlStateNormal];
//        likeButton.tag = 1;
//    }
    
//    [scrollView addSubview:likeButton]; //Customised
    
    UIButton *btnWatch=[UIButton buttonWithType:UIButtonTypeCustom];
    btnWatch.tag = 1; // watch full video
    btnWatch.frame=CGRectMake(self.view.frame.size.width-140, yPos,130, 30);
    [btnWatch setTitle:@"WATCH FULL VIDEO" forState:UIControlStateNormal];
    [btnWatch.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
    btnWatch.layer.cornerRadius=3;
    btnWatch.clipsToBounds=YES;
    [btnWatch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    btnWatch.backgroundColor=[UIColor colorWithRed:228.0f/255.0f green:6.0f/255.0f blue:13.0f/255.0f alpha:1];
    btnWatch.backgroundColor = [UIColor redColor];
    [btnWatch addTarget:self action:@selector(onWatchFullVideo:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnWatch];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 1)];
        viewLine.backgroundColor=[UIColor lightGrayColor];
        [scrollView addSubview:viewLine];
        
        yPos+=10;
    }
    strAdmin_VideoId = [dictValue valueForKey:@"admin_video_id"];
    
    UIButton *btnAddPlayList=[UIButton buttonWithType:UIButtonTypeCustom];
    btnAddPlayList.frame=CGRectMake(10, yPos,130, 30);
    btnAddPlayList.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    btnAddPlayList.tag=[[dictValue valueForKey:@"admin_video_id"] intValue];
    [btnAddPlayList.titleLabel setFont:[UIFont systemFontOfSize:13]];
    
    if ([[dictGetValue valueForKey:@"wishlist_status"]integerValue]==0)
    {
        [btnAddPlayList setTitle:@"ADD TO MYLIST" forState:UIControlStateNormal];
        [btnAddPlayList addTarget:self action:@selector(onAddToPlayList:) forControlEvents:UIControlEventTouchDown];
    }
    else
    {
        [btnAddPlayList setTitle:@"Added" forState:UIControlStateNormal];
        [btnAddPlayList addTarget:self action:@selector(onRemoveWishlist:) forControlEvents:UIControlEventTouchDown];
    }
    btnAddPlayList.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btnAddPlayList setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddPlayList.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    [scrollView addSubview:btnAddPlayList];
    
//    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(10, yPos+3, 20, 20)];
//    img.contentMode = UIViewContentModeScaleAspectFit;
//    img.clipsToBounds = YES;
//    img.image=[UIImage imageNamed:@"add-icon-with-white.png"];
//    [scrollView addSubview:img];
    
    UIImageView *img1=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-120, yPos+3, 20, 20)];
    img1.contentMode = UIViewContentModeScaleAspectFit;
    img1.clipsToBounds = YES;
    img1.image=[UIImage imageNamed:@"Share-25x25.png"];
    [scrollView addSubview:img1];
    
    UIButton *btnShare=[UIButton buttonWithType:UIButtonTypeCustom];
    btnShare.frame=CGRectMake(self.view.frame.size.width-120, yPos-3,100, 30);
    btnShare.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [btnShare setTitle:@"Share" forState:UIControlStateNormal];
    [btnShare.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [btnShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnShare addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnShare];
    
    yPos+=30;
    
    //    {
    //        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width, 2)];
    //        viewLine.backgroundColor=[UIColor lightGrayColor];
    //        [scrollView addSubview:viewLine];
    //
    //        yPos+=10;
    //    }
    
    
    NSArray *arrCommentsList=[dictGetValue valueForKey:@"comments"];
    
    if (arrCommentsList.count!=0)
    {
        UILabel *lblRating=[[UILabel alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 30)];
        lblRating.text=@"Comments";
        [lblRating setFont:[UIFont boldSystemFontOfSize:16]];
        lblRating.textColor=[UIColor whiteColor];
        [scrollView addSubview:lblRating];
        
        yPos+=35;
        
    }
    
    for (int i=0; i<arrCommentsList.count; i++)
    {
        
        NSDictionary *dictLoc=[arrCommentsList objectAtIndex:i];
        
        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(10,yPos,50,50)];
        imageView.layer.cornerRadius =imageView.frame.size.width/2;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"user_picture"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [imageView.layer setBorderWidth: 2.0];
        [scrollView addSubview:imageView];
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(70, yPos, self.view.frame.size.width-40, 30)];
        lblName.text=[dictLoc valueForKey:@"username"];
        [lblName setFont:[UIFont boldSystemFontOfSize:15]];
        lblName.textColor=[UIColor colorWithRed:255.0f/255.0f green:154.0f/255.0f blue:3.0f/255.0f alpha:1];
        [scrollView addSubview:lblName];
        
        yPos+=25;
        
        
        
        UITextView *txtDescri1=[[UITextView alloc]initWithFrame:CGRectMake(70, yPos, self.view.frame.size.width-70, 30)];
        txtDescri1.text=[dictLoc valueForKey:@"comment"];
        txtDescri1.userInteractionEnabled=NO;
        [txtDescri1 setFont:[UIFont systemFontOfSize:13]];
        txtDescri1.backgroundColor=[UIColor clearColor];
        txtDescri1.textColor=[UIColor whiteColor];
        [scrollView addSubview:txtDescri1];
        
        [txtDescri1 sizeToFit];
        
        yPos+=txtDescri1.frame.size.height;
        
    }
    
    yPos+=60;
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, yPos+50);
    
}

#pragma mark YouTube Player and JW Player:

- (void)playInYouTubePlayer{
    
    playerView=[[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/3+50)];
    playerView.backgroundColor=[UIColor blackColor];
    [self.view addSubview:playerView];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(10, 25, 30, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(onBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    [playerView bringSubviewToFront:backButton];
    
    playerView.delegate = self;
    
    NSArray *arrLocal=[[dictGetValue valueForKey:@"ios_trailer_video"] componentsSeparatedByString:@"/"];
    
    NSString *strVideo=[arrLocal lastObject];
    
    NSDictionary *playerVars = @{
                                 @"controls" : @2,
                                 // @"autoplay" :@1,
                                 @"playsinline" : @1,
                                 @"autohide" : @1,
                                 // @"showinfo" : @0,
                                 @"theme" : @"light",
                                 @"color" : @"red"
                                 };
    
    [playerView loadWithVideoId:strVideo playerVars:playerVars];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedPlaybackStartedNotification:)
                                                 name:@"Playback started"
                                               object:nil];
    
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPlayerTapped:)];
    singleFingerTap.numberOfTapsRequired = 1;
    singleFingerTap.delegate = self;
    //    [playerView addGestureRecognizer:singleFingerTap];
    
    playerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    
    //    [playerView loadWithVideoId:strVideo];
}

- (void)playInAVPlayer{
    
    NSDictionary *dictValue = [dictGetValue valueForKey:@"video"];
    
    // AVPlayer
    
    NSURL *movieURL = [NSURL URLWithString: [dictGetValue valueForKey:@"ios_trailer_video"]];
    AVAsset *asset = [AVURLAsset assetWithURL:movieURL];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset: asset];
    
    player = [AVPlayer playerWithPlayerItem:playerItem];
    //    [player addObserver:self forKeyPath:@"timeControlStatus" options:0 context:nil];
    
    controller = [[AVPlayerViewController alloc] init];
    
    [self addChildViewController:controller];
    [self.view addSubview:controller.view];
    
    controller.view.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height/3+50);
    controller.delegate = self;
    controller.player = player;
    controller.showsPlaybackControls = NO;
    
    defaultImage = [[UIImageView alloc] initWithFrame: controller.view.frame];
    defaultImage.image = [UIImage imageNamed:@"sample.png"];
    [defaultImage sd_setImageWithURL:[NSURL URLWithString:[dictValue valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    defaultImage.contentMode = UIViewContentModeScaleAspectFill;
    defaultImage.clipsToBounds = YES;
    defaultImage.hidden = NO;
    [controller.contentOverlayView addSubview:defaultImage];
    
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(defaultImage.frame.size.width/2-25, defaultImage.frame.size.height/2-25, 50, 50);
//    [playButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    [playButton setBackgroundColor:[UIColor redColor]];
    [playButton setTitle:@"Play" forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(playButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [controller.contentOverlayView addSubview:playButton];
    
    [player pause];
    //    [player play];
    
    // Gradient View Top & Bottom
    
    gradientViewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, controller.view.frame.size.height-100, self.view.frame.size.width, 100)];
    CAGradientLayer *gradientBottom = [CAGradientLayer layer];
    gradientBottom.frame = gradientViewBottom.bounds;
    gradientBottom.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor colorWithRed:10.0f/255.0f green:10.0f/255.0f blue:10.0f/255.0f alpha:1.0f].CGColor];
    [gradientViewBottom.layer insertSublayer:gradientBottom atIndex:0];
    [controller.contentOverlayView addSubview:gradientViewBottom];
    
    gradientViewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 100)];
    CAGradientLayer *gradientTop = [CAGradientLayer layer];
    gradientTop.frame = gradientViewTop.bounds;
    gradientTop.colors = @[(id)[UIColor blackColor].CGColor, (id)[UIColor clearColor].CGColor];
    [gradientViewTop.layer insertSublayer:gradientTop atIndex:0];
    [controller.contentOverlayView addSubview:gradientViewTop];
    
    // BackButton
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(10, 15, 30, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(onBack:) forControlEvents:UIControlEventTouchUpInside];
    [gradientViewTop addSubview:backButton];
    
    // SearchButton
    
    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    searchButton.frame = CGRectMake(self.view.frame.size.width-40, 15, 30, 30);
    [searchButton setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [gradientViewTop addSubview:searchButton];
    
    UILabel *bannerLabel = [UILabel new];
    bannerLabel.frame = CGRectMake(0 , controller.view.frame.size.height-30, self.view.frame.size.width, 30);
    bannerLabel.backgroundColor = [UIColor redColor];
    bannerLabel.textColor = [UIColor whiteColor];
    bannerLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
    bannerLabel.textAlignment = NSTextAlignmentCenter;
    bannerLabel.text = [NSString stringWithFormat:@"You need to pay %@ %@ to watch the Full Video",[dictGetValue objectForKey:@"currency"],[[dictGetValue objectForKey:@"video"] objectForKey:@"amount"]];
    
    if (!pay_per_view_status) {
        [controller.contentOverlayView addSubview:bannerLabel];
    }
}

- (void)playButtonTapped:(UIButton *)sender{
    
    defaultImage.hidden = YES;
    trailerORfullVideo = @"trailer";
    [player pause];
    //    [self.player.view removeFromSuperview];
    [controller removeFromParentViewController];
    [blurEffectView removeFromSuperview];
    [self performSegueWithIdentifier:@"fullVideo" sender:self];
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
//                        change:(NSDictionary *)change context:(void *)context {
//    if (object == player && [keyPath isEqualToString:@"timeControlStatus"]) {
//        if (player.timeControlStatus == AVPlayerTimeControlStatusPlaying) {
//            defaultImage.hidden = YES;
//
//            trailerORfullVideo = @"trailer";
//            [player pause];
//            [self.player.view removeFromSuperview];
//            [blurEffectView removeFromSuperview];
//            [[UIApplication sharedApplication] setStatusBarHidden:YES];
//            [self performSegueWithIdentifier:@"fullVideo" sender:self];
//        }
//        else if (player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
//            // something went wrong. player.error should contain some information
////        }
//    }
//}

- (IBAction)searchButtonTapped:(id)sender {
    SearchBarVC *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchBarVC"];
    [self.navigationController pushViewController:search animated:YES];
}

#pragma Like/Dislike methods

//- (void)likeButtonTapped:(UIButton *)sender{
//
//    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//
//    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
//    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
//    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
//    [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
//    [postDataDict setValue:strVideoID forKey:@"admin_video_id"];
//
//    if (sender.tag == 0) {
//        sender.tag = 1;
//        like_dislike = 1;
//        [likeButton setImage:[UIImage imageNamed:@"thumb_down_white.png"] forState:UIControlStateNormal];
//
//        // Service for Like Video.
//        [self serviceRequest:METHOD_LIKE_VIDEO params:postDataDict key:@"likes"];
//
//    }else{
//        sender.tag = 0;
//        like_dislike = 1;
//        [likeButton setImage:[UIImage imageNamed:@"thumb_up_white.png"] forState:UIControlStateNormal];
//
//        [self serviceRequest:METHOD_DISLIKE_VIDEO params:postDataDict key:@"likes"];
//    }
//
//}

#pragma Report Spam

- (void)reportSpamTapped:(UIButton *)sender{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Report as Spam" message:@"" preferredStyle:UIAlertControllerStyleAlert]; // 7
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self reportSpamCatagoryList];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)reportSpamCatagoryList{
    
    
    __block NSString *spamReason = [[NSString alloc] init];
    
    [[WebServiceHandler sharedInstance] sendRequest:METHOD_REPORTSPAMLIST isApiRequired:NO urlParameters:nil method:GET isAuthRequired:NO postData:nil withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        
        NSArray *spamCatList = [dict objectForKey:@"data"];
        
        UIAlertController *spamListAlert = [UIAlertController alertControllerWithTitle:@"Reason to Report" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSDictionary *spamValue in spamCatList) {
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:[spamValue objectForKey:@"value"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                spamReason = [spamValue objectForKey:@"value"];
                
                [self addSpamReport:spamReason];
                
            }];
            
            [spamListAlert addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        
        [spamListAlert addAction:cancelAction];
        [self presentViewController:spamListAlert animated:YES completion:nil];
        
    }];
    
}

- (void)addSpamReport:(NSString *)reason{
    
    __block NSError *error;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaluts valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaluts valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaluts valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:strVideoID forKey:@"admin_video_id"];
    [postDataDict setValue:reason forKey:@"reason"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDataDict options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:METHOD_ADDSPAM isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if ([[dict objectForKey:@"success"] intValue] == 1) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
                [self popup:[dict objectForKey:@"message"]];
                
            });
        }
    }];
}

- (void) popup:(NSString *)message{
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}
#pragma mark youtube delegate methods

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification {
    if([notification.name isEqual:@"Playback started"] && notification.object != self) {
        [playerView pauseVideo];
    }
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
            case kYTPlayerStatePlaying:
            //NSLog(@"Started playback");
            break;
            case kYTPlayerStatePaused:
            //NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}

- (void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality{
    
}
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error{
    
}

//Delegate Method

-(void) onPlayerTapped:(UIGestureRecognizer *)gestureRecognizer {
    // isInPlayingMode = NO;
    //NSLog(@"clicked");
    //    [self onDisplayClick];
}

#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - Custom methods

-(IBAction)onAddToPlayList:(id)sender
{
    UIButton *btn=(UIButton *) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"You want to add your playlist ?" preferredStyle:UIAlertControllerStyleAlert]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [btn setTitle:@"Added" forState:UIControlStateNormal];
        [btn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [btn addTarget:self action:@selector(onRemoveWishlist:) forControlEvents:UIControlEventTouchDown];
        // btn.userInteractionEnabled=NO;
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)btn.tag];
        
        // [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=2;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
        
    }];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    
    [alert addAction:defaultAction1];
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
//-(IBAction)onRemoveWishlist:(id)sender
//{
//    UIButton *btn=(UIButton *) sender;
//
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"You want to remove this video from your playlist ?" preferredStyle:UIAlertControllerStyleAlert]; // 7
//
//    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//        [btn setTitle:@"ADD TO MYLIST" forState:UIControlStateNormal];
//        [btn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
//        [btn addTarget:self action:@selector(onAddToPlayList:) forControlEvents:UIControlEventTouchDown];
//
//        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)btn.tag];
//
//        // [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
//
//        Webservice *service=[[Webservice alloc]init];
//        service.delegate=self;
//        service.tag=2;
//
//        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
//        NSString *strID=[defaluts valueForKey:@"id"];
//        NSString *strToken=[defaluts valueForKey:@"token"];
//
//        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
//
//        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
//    }]; // 8
//
//
//    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//        //NSLog(@"You pressed button Cancel");
//    }]; // 8
//
//    [alert addAction:defaultAction1];
//    [alert addAction:defaultAction];
//
//    [self presentViewController:alert animated:YES completion:nil];
//
//}
-(IBAction)onShare:(id)sender
{
    //   NSString *string = [NSString stringWithFormat:@"%@ Invited you to install StreamView", @"Ramesh"];
    
    // NSURL *URL = [NSURL URLWithString:@"www.apppoets.com/"];
    
    
    
    NSURL *URL = [NSURL URLWithString:[dictGetValue valueForKey:@"share_link"]];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]
                                                        initWithActivityItems:@[URL]
                                                        applicationActivities:nil];
    
    [activityViewController setCompletionWithItemsHandler:^(NSString *activityType,
                                                            
                                                            BOOL completed,
                                                            
                                                            NSArray *returnedItems,
                                                            
                                                            NSError * error){
        
        if ( completed )
        {
            //  TODO error handling
            //NSLog(@"share complete");
            //            NSExtensionItem* extensionItem = [returnedItems firstObject];
            //            NSItemProvider* itemProvider = [[extensionItem attachments] firstObject];
            
        } else {
            
            //NSLog(@"canceld");
            
        }
        
    }];
    
    if ( [activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
        
        // iOS8
        activityViewController.popoverPresentationController.sourceView = self.view;
        
        //        activityViewController.popoverPresentationController.sourceRect = self.view.bounds;
    }
    
    [self presentViewController:activityViewController animated:YES completion:nil];
}

-(IBAction)onRatingandComments:(id)sender
{
    viewcontent1=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height)];
    viewcontent1.backgroundColor=[UIColor clearColor];
    [self.view addSubview:viewcontent1];
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(20, 100, self.view.frame.size.width-40, 300)];
    view.backgroundColor=[UIColor whiteColor];
    view.layer.shadowOpacity = 0.4f;
    view.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    view.layer.shadowRadius = 1.0f;
    [viewcontent1 addSubview:view];
    
    
    NSDictionary *dictValue = [dictGetValue valueForKey:@"video"];
    
    UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 130)];
    [imgView sd_setImageWithURL:[NSURL URLWithString:[dictValue valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    imgView.clipsToBounds = YES;
    [view addSubview:imgView];
    
    
    lblRatings=[[UILabel alloc]initWithFrame:CGRectMake(10, 130, view.frame.size.width-20, 40)];
    lblRatings.text=@"Your support means a lot to us. \n Motivate us by giving us good comments ";
    // lblRatings.text=[dictValue valueForKey:@"Ratings"];
    [lblRatings setFont:[UIFont boldSystemFontOfSize:13]];
    lblRatings.numberOfLines=2;
    
    lblRatings.textAlignment=NSTextAlignmentCenter;
    lblRatings.textColor=[UIColor lightGrayColor];
    [view addSubview:lblRatings];
    
    
    txtComment=[[UITextView alloc]initWithFrame:CGRectMake(0, 180, view.frame.size.width,80)];
    txtComment.textColor=[UIColor blackColor];
    txtComment.tag=110;
    [txtComment setFont:[UIFont boldSystemFontOfSize:14]];
    txtComment.backgroundColor=[UIColor whiteColor];
    txtComment.textAlignment=NSTextAlignmentJustified;
    // [txtRev sizeToFit];
    //txtComment.delegate=self;
    txtComment.userInteractionEnabled=YES;
    [view addSubview:txtComment];
    
    [txtComment setDelegate:self];
    [txtComment setReturnKeyType:UIReturnKeyDone];
    [txtComment setText:@"Write comments"];
    [txtComment setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    [txtComment setTextColor:[UIColor lightGrayColor]];
    [txtComment becomeFirstResponder];
    
    UIButton *btnSubmit=[[UIButton alloc] initWithFrame:CGRectMake(0,view.frame.size.height-40,view.frame.size.width,40)];
    [btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    [btnSubmit.titleLabel setTextColor:[UIColor whiteColor]];
    [btnSubmit.titleLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:15]];
    [btnSubmit addTarget:self action:@selector(onSubmit:) forControlEvents:UIControlEventTouchDown];
    btnSubmit.backgroundColor=[UIColor colorWithRed:218.0f/255.0f green:106.0f/255.0f blue:44.0f/255.0f alpha:1];
    [btnSubmit.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [view addSubview:btnSubmit];
}

#pragma mark Watch full video

-(IBAction)onWatchFullVideo:(id)sender
{
    
    //    [_player stop];
    [player pause];
    trailerORfullVideo = @"fullVideo";
    [self addBlurView];
    
    if (!pay_per_view_status) {
        
        if (is_ppv_subscribe_page == 1) {
            [self displaySelectPaymentView];
            
        }else{
            
            [self displayInvoice:nil];
        }
        
    }else{
        
        if ([[dictGetValue objectForKey:@"user_type"] intValue] == 0) {
            
            UIAlertController *premiumAlert = [UIAlertController alertControllerWithTitle:@"Pay now !" message:@"Subscribe as Premium user to Watch the full Video." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *viewplans = [UIAlertAction actionWithTitle:@"View Plans" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                ViewPlansVC *plans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
                [self.navigationController pushViewController:plans animated:YES];
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [premiumAlert addAction:viewplans];
            [premiumAlert addAction:cancelAction];
            [self presentViewController:premiumAlert animated:YES completion:nil];
        }else{
            
            [blurEffectView removeFromSuperview];
            //            [_player.view removeFromSuperview];
            [controller removeFromParentViewController];
            [self performSegueWithIdentifier:@"fullVideo" sender:self];
        }
    }
}
#pragma mark -- UITextView Delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.textColor == [UIColor lightGrayColor]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Write comments";
        [textView resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            textView.textColor = [UIColor lightGrayColor];
            textView.text = @"Write comments";
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender
{
    strRating=[NSString stringWithFormat:@"%.0f",sender.value];
    
    int nVal=[strRating intValue];
    
    
    NSString *strRatingValue=[NSString stringWithFormat:@"%d",nVal];
    
    if ([strRatingValue isEqualToString:@"1"])
    {
        lblRatings.text=@"Hated it";
    }
    else if([strRatingValue isEqualToString:@"2"])
    {
        lblRatings.text=@"Disliked it";
    }
    else if([strRatingValue isEqualToString:@"3"])
    {
        lblRatings.text=@"Its ok";
    }
    
    else if([strRatingValue isEqualToString:@"4"])
    {
        lblRatings.text=@"Liked it";
    }
    
    else if([strRatingValue isEqualToString:@"5"])
    {
        lblRatings.text=@"Loved it";
    }
    
}

-(IBAction)onSubmit:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    
    if ([txtComment.text isEqualToString:@"Write comments"])
    {
        alertController.message=@"Please Enter comments";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\",\"rating\":\"%@\",\"comments\":\"%@\"}",strID,strToken,strAdmin_VideoId,strRating,txtComment.text];
    
    [service executeWebserviceWithMethod:METHOD_USER_RATING withValues:strSend];
    
}
-(IBAction)onCancel:(id)sender
{
    [viewcontent1 removeFromSuperview];
}


- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            
            [viewcontent1 removeFromSuperview];
            [self onCallWebService];
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
            
        }
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            pay_per_view_status = [[dictResponse objectForKey:@"pay_per_view_status"] intValue];
            is_ppv_subscribe_page = [[dictResponse objectForKey:@"is_ppv_subscribe_page"] intValue];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    else if (webservice.tag==3)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            dictGetValue=dictResponse;
            pay_per_view_status = [[dictResponse objectForKey:@"pay_per_view_status"] intValue];
            is_ppv_subscribe_page = [[dictResponse objectForKey:@"is_ppv_subscribe_page"] intValue];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self onPageLoad];
                
            });
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
}
#pragma mark - callbacks

- (void)setupNotifications
{
    //    NSArray *notifications = @[JWPlayerStateChangedNotification, JWMetaDataAvailableNotification, JWAdActivityNotification, JWErrorNotification, JWCaptionsNotification, JWVideoQualityNotification, JWPlaybackPositionChangedNotification, JWFullScreenStateChangedNotification, JWAdClickNotification];
    
    //    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    //    [notifications enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
    //        [center addObserver:self selector:@selector(handleNotification:) name:obj object:nil];
    //    }];
    //    [center addObserver:self selector:@selector(updatePlaybackTimer:) name:JWPlaybackPositionChangedNotification object:nil];
    //    [center addObserver:self selector:@selector(playerStateChanged:) name:JWPlayerStateChangedNotification object:nil];
    //    [center addObserver:self selector:@selector(playerStateChanged:) name:JWAdActivityNotification object:nil];
}

#pragma mark - callback notification handling

- (void)handleNotification:(NSNotification*)notificaiton
{
    NSDictionary *userInfo = notificaiton.userInfo;
    NSString *callback = userInfo[@"event"];
    
    if([callback isEqualToString:@"onTime"]) { return;}
    
    
}

- (void)updatePlaybackTimer:(NSNotification*)notification
{
    NSDictionary *userinfo = notification.userInfo;
    if([userinfo[@"event"] isEqualToString:@"onTime"]) {
        NSString *position = [NSString stringWithFormat:@"%@/%@", userinfo[@"position"], userinfo[@"duration"]];
        // self.playbackTime.text = position;
    }
}

- (void)controlCenter {
    MPNowPlayingInfoCenter* mpic = [MPNowPlayingInfoCenter defaultCenter];
    mpic.nowPlayingInfo = @{MPMediaItemPropertyTitle: @"Title",
                            MPMediaItemPropertyArtist: @"Artist"
                            };
}

#pragma mark - callback delegate methods

-(void)onTime:(double)position ofDuration:(double)duration
{
    NSString *playbackPosition = [NSString stringWithFormat:@"%.01f/.01%f", position, duration];
    // self.playbackTime.text = playbackPosition;
}
- (void)onPlay
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
    //    trailerORfullVideo = @"trailer";
    //    [self performSegueWithIdentifier:@"fullVideo" sender:self];
    
    trailerORfullVideo = @"trailer";
    //    [gradientViewBottom removeFromSuperview];
    //    [gradientViewTop removeFromSuperview];
    
    //    [self.player stop];
    //    [self.player.view removeFromSuperview];
    
    [blurEffectView removeFromSuperview];
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self performSegueWithIdentifier:@"fullVideo" sender:self];
}

-(void)onPause
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onBuffer
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onIdle
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onReady
{
    //  [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onFirstFrame:(NSInteger)loadTime
{
    [timerHide invalidate];
    timerHide=nil;
    
    timerHide=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(HideNavBar) userInfo:nil repeats:NO];
    
}
-(void)onComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdSkipped:(NSString *)tag
{
    //  [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdComplete:(NSString *)tag
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdImpression:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforePlay
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onBeforeComplete
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPlay:(NSString *)tag
{
    // [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

-(void)onAdPause:(NSString *)tag
{
    // [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
}

-(void)onAdError:(NSError *)error
{
    //[self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
}

#pragma mark Webservice

- (void)serviceRequest:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            
            [[UtilityClass sharedInstance] networkError:self];
            
        }else{
            if ([[dict objectForKey:@"success"] intValue] == 1) {
                if ([key isEqualToString:@"ApplyCoupon"]){
                    NSString *amount = [NSString stringWithFormat:@"%@", [[dict objectForKey:@"data"] objectForKey:@"remaining_amount"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        invoice.couponCodeTF.userInteractionEnabled = NO;
                        stripeAmount = amount;
                        invoice.priceLabel.text = amount;
                        if ([amount isEqualToString:@"0"]) {
                            invoice.stripePayButton.hidden = YES;
                            invoice.okButton.hidden = NO;
                        }else{
                            invoice.stripePayButton.hidden = NO;
                            invoice.okButton.hidden = YES;                        }
                    });
                }else if ([key isEqualToString:@"likes"]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (like_dislike) {
                            
//                            likesLabel.text = [NSString stringWithFormat:@"%@ Likes", [dict valueForKey:@"like_count"]];
                        }
                    });
                }
                
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
        
    }];
}

- (void)stripePayRequest:(NSString *)service params:(NSMutableDictionary *)params tag:(NSInteger)tag{
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
        
        if (![[UtilityClass sharedInstance] isNetworkConnected]) {
            [[UtilityClass sharedInstance] networkError:self];
        }else{
            if ([dict objectForKey:@"success"]) {
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self paymentCompleteInvoice:dict];
                    
                });
            }else{
                [[UtilityClass sharedInstance] errorHandling:dict VC:self];
            }
        }
        
    }];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

#pragma mark Invoive for Stripe payment

- (void)fadeInAnimation:(UIView *)view{
    
    CATransition *transition = [CATransition animation];
    transition.type =kCATransitionFade;
    transition.duration = 0.5f;
    transition.delegate = self;
    [view.layer addAnimation:transition forKey:nil];
}

- (void)displayInvoice:(UIButton *)sender{
    invoice.couponCodeTF.text = @"";
    // Invoice View Config :
    
    invoice = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] lastObject];
    invoice = [invoice initWithFrame:self.view.frame];
    invoice.contentView.layer.cornerRadius = 5.0f;
    
    invoice.durationLabel.hidden = YES;
    invoice.monthLabel.hidden = YES;
    invoice.okButton.hidden = YES;
    
    invoice.titleLabel.text = [[dictGetValue objectForKey:@"video"] valueForKey:@"title"];
    invoice.priceLabel.text = [NSString stringWithFormat:@"%@ %@", [dictGetValue objectForKey:@"currency"],[[dictGetValue objectForKey:@"video"] objectForKey:@"amount"]];
    
    //Note:
    
    if ([[[dictGetValue objectForKey:@"video"] objectForKey:@"amount"] intValue] == 0){
        
        invoice.stripePayButton.hidden = YES;
        invoice.okButton.hidden = NO;
        
    }else{
        invoice.stripePayButton.hidden = NO;
        invoice.okButton.hidden = YES;
        
    }
    [self.view.window addSubview:blurEffectView];
    [self.view.window addSubview:invoice];
    [self fadeInAnimation:invoice];
    
    [invoice.closeButton addTarget:self action:@selector(closeInvoice:) forControlEvents:UIControlEventTouchUpInside];
    [invoice.stripePayButton addTarget:self action:@selector(stripePay:) forControlEvents:UIControlEventTouchUpInside];
    
    [invoice.checkCouponButton addTarget:self action:@selector(applyCouponTapped:) forControlEvents:UIControlEventTouchUpInside];
    invoice.checkCouponButton.tag = sender.tag;
}

- (void)applyCouponTapped:(UIButton *)sender{
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:invoice.couponCodeTF.text forKey:@"coupon_code"];
    [postDataDict setValue:strAdmin_VideoId forKey:@"subscription_id"];
    
    if ([invoice.couponCodeTF.text isEqualToString:@""]) {
        [self popup:@"Please enter valid code"];
    }else{
        [self serviceRequest:METHOD_APPLY_COUPON_SUBS params:postDataDict key:@"ApplyCoupon"];
    }
}

- (void)displaySelectPaymentView{
    
    selectPaymentView = [[[NSBundle mainBundle] loadNibNamed:@"PaymentInvoice" owner:self options:nil] lastObject];
    selectPaymentView = [selectPaymentView initWithFrame:self.view.frame];
    selectPaymentView.layer.cornerRadius = 5.0f;
    selectPaymentView.contentView.layer.cornerRadius = 5.0f;
    [selectPaymentView.amountPayButton setTitle:[NSString stringWithFormat:@"Pay %@ %@",[dictGetValue objectForKey:@"currency"],[[dictGetValue objectForKey:@"video"] objectForKey:@"amount"]] forState:UIControlStateNormal];
    
    [self.view.window addSubview:blurEffectView];
    [self.view.window addSubview:selectPaymentView];
    
    [selectPaymentView.viewPlansButton addTarget:self action:@selector(goToViewPlans:) forControlEvents:UIControlEventTouchUpInside];
    [selectPaymentView.amountPayButton addTarget:self action:@selector(displayInvoice:) forControlEvents:UIControlEventTouchUpInside];
    [selectPaymentView.closeButton addTarget:self action:@selector(closeInvoice:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addBlurView{
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        //        [self.view.window addSubview: blurEffectView];
        
    } else {
        self.view.backgroundColor = [UIColor blackColor];
    }
}

- (void)goToViewPlans:(UIButton *)sender{
    
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
    ViewPlansVC *viewPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
    [self.navigationController pushViewController:viewPlans animated:YES];
}

- (void)stripePay:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
    defaults = [[NSUserDefaults alloc] init];
    if (!(stripeAmount.length > 0)) {
        stripeAmount = [[dictGetValue objectForKey:@"video"] objectForKey:@"amount"];
    }
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:strVideoID forKey:@"admin_video_id"];
        
        UIAlertController *payAlert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Are you sure to pay %@ %@ ?",[dictGetValue objectForKey:@"currency"], stripeAmount] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self stripePayRequest:METHOD_STRIPE_PPV params:postDataDict tag:sender.tag];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    [payAlert addAction:okAction];
    [payAlert addAction:cancelAction];
    
    [self presentViewController:payAlert animated:YES completion:nil];
}

- (void)closeInvoice:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [selectPaymentView removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
}
-(void)paymentCompleteInvoice:(NSDictionary *)response{
    
    invoice.payThroughLabel.hidden = YES;
    invoice.stripePayButton.hidden = YES;
    invoice.okButton.hidden = NO;
    invoice.closeButton.hidden = YES;
    invoice.durationLabel.hidden = NO;
    invoice.monthLabel.hidden = NO;
    
    invoice.title.text = @"Paid Amount";
    invoice.price.text = @"Status";
    invoice.monthLabel.text = @"Payment ID";
    
    invoice.titleLabel.text = [NSString stringWithFormat:@"%@ %@", [dictGetValue objectForKey:@"currency"],[[dictGetValue objectForKey:@"video"] objectForKey:@"amount"]];
    invoice.priceLabel.text = @"Approved";
    invoice.durationLabel.text = [[response objectForKey:@"data"] objectForKey:@"payment_id"];
    
    [invoice.okButton addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view.window addSubview:blurEffectView];
    [self.view.window addSubview:invoice];
    
}

- (void)okButtonTapped:(UIButton *)sender{
    
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    //    [self.player stop];
    //    [self.player.view removeFromSuperview];
    [player pause];
    [controller removeFromParentViewController];
    [self performSegueWithIdentifier:@"fullVideo" sender:self];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"fullVideo"])
    {
        
        WatchFullVideo *watchFullVideo=[segue destinationViewController];
        //        ios_trailer_video
        
        if ([trailerORfullVideo isEqualToString:@"fullVideo"]) {
            watchFullVideo.strVideoURL=[dictGetValue valueForKey:@"ios_video"];
        }else{
            watchFullVideo.strVideoURL= [dictGetValue objectForKey:@"ios_trailer_video"];
        }
        watchFullVideo.strUserType=[NSString stringWithFormat:@"%@",  [[dictGetValue valueForKey:@"video"] valueForKey:@"video_type"]];
        watchFullVideo.strImageURL=[[dictGetValue valueForKey:@"video"] valueForKey:@"default_image"];
        watchFullVideo.strTitle=[[dictGetValue valueForKey:@"video"] valueForKey:@"title"];
        watchFullVideo.adminVideoID = [[dictGetValue valueForKey:@"video"] objectForKey:@"admin_video_id"];
        watchFullVideo.seekTime = [dictGetValue objectForKey:@"seek"];
        
    }
}


@end

