//
//  SettingsView.h
//  Streaming
//
//  Created by Ramesh on 17/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"

@interface SettingsView : UIViewController
{
    IBOutlet UIView *viewPrivacy;
    IBOutlet UIView *viewNotication;
    IBOutlet UIView *viewDeleteAccount;
    IBOutlet UIView *viewPlansView;
    IBOutlet UIView *paidVideosView;
    
    IBOutlet UIView *myPlansView;
    IBOutlet UISwitch *switchBtn;
    IBOutlet UIButton *viewPlansButton;
    IBOutlet UIButton *paidVideosButton;
   // IBOutlet UIView *view
    
    IBOutlet UIButton *myPlansButton;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *SideBarButton;

- (IBAction)onPrivacy:(id)sender;

- (IBAction)onTerms:(id)sender;
- (IBAction)onDelegate:(id)sender;
- (IBAction)switchToggled:(id)sender;
- (IBAction)viewPlansTapped:(id)sender;
- (IBAction)paidVideosTapped:(id)sender;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *deleteContentView;
@property (weak, nonatomic) IBOutlet UIView *deleteView;

@end
