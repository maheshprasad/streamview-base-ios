//
//  PaymentInvoiceVCViewController.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 30/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentInvoiceVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UIView *couponCodeView;
@property (strong, nonatomic) IBOutlet UITextField *couponCodeTF;
@property (strong, nonatomic) IBOutlet UIButton *stripeButton;
@property (strong, nonatomic) IBOutlet UIView *successCodeView;
@property (strong, nonatomic) IBOutlet UILabel *couponCodeLabel;
@property (strong, nonatomic) IBOutlet UIView *okButtonView;
@property (strong, nonatomic) IBOutlet UIView *stripePayView;
@property (strong, nonatomic) IBOutlet UIButton *okButton;

@property (strong, nonatomic) NSDictionary *detailedDict;
@property (strong, nonatomic) UIImage *bgImage;
@property BOOL isPPV;

@end
