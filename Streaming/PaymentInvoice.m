//
//  PaymentInvoice.m
//  Streaming
//
//  Created by Aravinth Ramesh on 12/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "PaymentInvoice.h"

@implementation PaymentInvoice

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self custominit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self custominit];
    }
    
    return self;
}

- (void)custominit{
    
    self.viewPlansButton.layer.cornerRadius = 3.0f;
    self.amountPayButton.layer.cornerRadius = 3.0f;
    self.contentView.layer.cornerRadius = 5.0f;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
