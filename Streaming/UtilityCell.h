//
//  UtilityCell.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 10/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKDropdownMenu.h"
@interface UtilityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *seasonListView;

@end
