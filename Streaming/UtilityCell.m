//
//  UtilityCell.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 10/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "UtilityCell.h"

@implementation UtilityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
