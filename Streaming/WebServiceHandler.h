//
//  WebServiceHandler.h
//  WebServiceDemo
//
//  Created by Dhanasekaran on 05/11/17.
//  Copyright © 2017 Premkumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIService.h"

typedef enum : NSUInteger {
    GET = 0,
    POST,
    PUT,
    DELETE
} WebMethod;

typedef void(^completionHandler)(NSDictionary *dict, NSURLResponse *response, NSError *error);

@interface WebServiceHandler : NSObject

+ (WebServiceHandler *)sharedInstance;

- (void)sendRequest:(NSString *)service isApiRequired:(BOOL)isApiRequired  urlParameters:(NSDictionary *)params method:(WebMethod)method isAuthRequired:(BOOL)isAuthRequired postData:(NSData *)postData withCompletionHandler:(completionHandler)completionHandler;

@end
