//
//  SlideMenuView.h
//  Royal Water
//
//  Created by Ramesh on 13/06/16.
//  Copyright © 2016 WePOP Info Solution Pvt LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"


@interface SlideMenuView : UIViewController<WebServiceDelegate>
{
  
    IBOutlet UIImageView *imgProfile;
    IBOutlet UILabel *lblProfileName;
}
- (IBAction)onEditProfile:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

@end
