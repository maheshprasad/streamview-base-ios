//
//  ViewPlansVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 03/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "ViewPlansVC.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"
#import "VIewPlansCell.h"
#import "InvoiceView.h"
#import "CardsVC.h"
#import "PaymentInvoice.h"
#import "UtilityClass.h"
#import "PaymentInvoiceVC.h"
#import "PaymentCompleteVC.h"
#import "MyPlans.h"

@interface ViewPlansVC ()<UITableViewDelegate, UITableViewDataSource, CAAnimationDelegate, UITextFieldDelegate>
{
    NSMutableArray *plansArray;
    InvoiceView *invoice;
    PaymentInvoice *paymentView;
    UIVisualEffectView *blurEffectView;
    NSUserDefaults *defaults;
    NSString *currency;
    NSString *stripeAmount;
    NSDictionary *invoiceDetails;
    PaymentCompleteVC *completeInvoice;
}
@property (strong, nonatomic) IBOutlet UITableView *plansTableView;
@end

@implementation ViewPlansVC

- (void)viewDidLoad {
    [super viewDidLoad];
    plansArray = [NSMutableArray new];
    defaults = [NSUserDefaults standardUserDefaults];
    _plansTableView.delegate = self;
    _plansTableView.dataSource = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentCompleted:) name:@"PaymentCompleted" object:nil];
    completeInvoice = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentCompleteVC"];
    self.plansTableView.rowHeight = UITableViewAutomaticDimension;
    self.plansTableView.estimatedRowHeight = 350;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    invoice = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] lastObject];
    invoice = [invoice initWithFrame:self.view.frame];
    invoice.layer.cornerRadius = 5.0f;
    invoice.titleBackView.layer.cornerRadius = 5.0f;
    invoice.couponCodeTF.delegate = self;
    [self viewPlanList];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
    backButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = backButton;
    [blurEffectView removeFromSuperview];
}

- (void)viewPlanList{
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:@"0" forKey:@"skip"];
    self.title = @"View Plans";
    [self callService:METHOD_VIEW_PLANS params:postDataDict key:@"CardList"];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeInvoice:(UIButton *)sender{
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
}

- (void)stripePay:(UIButton *)sender{
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    
    NSDictionary *planDetails = [plansArray objectAtIndex:sender.tag];
    
    if (!(stripeAmount.length > 0)) {
        stripeAmount = [planDetails objectForKey:@"amount"];
    }
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:[defaults valueForKey:@"sub_profile_id"] forKey:@"sub_profile_id"];
    [postDataDict setValue:[[plansArray objectAtIndex:sender.tag] objectForKey:@"subscription_id"] forKey:@"subscription_id"];
    
    UIAlertController *payAlert = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"Are you sure to pay %@%@ for %@",[planDetails objectForKey:@"currency"], stripeAmount,[planDetails objectForKey:@"title"]] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [self stripePayRequest:METHOD_STRIPE_PAY_PLAN params:postDataDict tag:sender.tag];
//        [blurEffectView removeFromSuperview];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    [payAlert addAction:okAction];
    [payAlert addAction:cancelAction];
    
    [self presentViewController:payAlert animated:YES completion:nil];
}

- (void)fadeInAnimation:(UIView *)view{

//    CATransition *transition = [CATransition animation];
//    transition.type =kCATransitionFade;
//    transition.duration = 1.0f;
//    transition.delegate = self;
//    [view.layer addAnimation:transition forKey:nil];
}
#pragma mark Tableview delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return plansArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *planDetails = [plansArray objectAtIndex:indexPath.row];
    
    VIewPlansCell *cardCell = (VIewPlansCell *)[tableView dequeueReusableCellWithIdentifier:@"VIewPlansCell"];
    
        if ([[planDetails objectForKey:@"popular_status"] integerValue] == 1) {
            cardCell.priceBackView.backgroundColor = [UIColor redColor];
            cardCell.payButton.backgroundColor = [UIColor redColor];
        }else{
            cardCell.priceBackView.backgroundColor = [UIColor blackColor];
            cardCell.payButton.backgroundColor = [UIColor blackColor];
        }
        
        if ([[planDetails objectForKey:@"amount"] isEqualToString:@"0.00"]){
            [cardCell.payButton setTitle:@"Free" forState:UIControlStateNormal];
        }else{
            [cardCell.payButton setTitle:@"Pay" forState:UIControlStateNormal];
        }
    
    cardCell.headingLabel.text = [planDetails objectForKey:@"title"];
    cardCell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[planDetails objectForKey:@"currency"],[planDetails objectForKey:@"amount"]];
    cardCell.contentLabel.text = [planDetails objectForKey:@"description"];
    cardCell.durationLabel.text = [NSString stringWithFormat:@"%@ months",[planDetails objectForKey:@"plan"]];
    cardCell.accountCountLabel.text = [NSString stringWithFormat:@"%@ Accounts",[planDetails objectForKey:@"no_of_account"]];
    
    cardCell.payButton.tag = indexPath.row;
    [cardCell.payButton addTarget:self action:@selector(payButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return cardCell;
}

- (void)payButtonTapped:(UIButton *)sender{
    PaymentInvoiceVC *invoice = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentInvoiceVC"];
    invoice.detailedDict = [plansArray objectAtIndex:sender.tag];
    invoice.bgImage = [self takeScreenshot];
    [self presentViewController:invoice animated:YES completion:nil];
//    [self.navigationController pushViewController:invoice animated:NO];
//    invoice.couponCodeTF.text = @"";
//    invoice.couponCodeView.hidden = NO;
//    invoice.couponSuccessView.hidden = YES;
//    invoiceDetails = [plansArray objectAtIndex:sender.tag];
//
//        if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//            self.view.backgroundColor = [UIColor clearColor];
//
//            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//
//            blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//            //always fill the view
//            blurEffectView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+60);
//            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
////            [self.view.window addSubview:blurEffectView];
//            [self.view addSubview:blurEffectView];
//
//        } else {
//            self.view.backgroundColor = [UIColor blackColor];
//        }
//
//    // Invoice View Config :
//
//    if ([[invoiceDetails objectForKey:@"amount"] isEqualToString:@"0.00"]) {
//
//        NSMutableDictionary *postDataDict = [NSMutableDictionary new];
//        [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
//        [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
//        [postDataDict setValue:[invoiceDetails objectForKey:@"subscription_id"] forKey:@"subscription_id"];
//        [postDataDict setValue:@"free_plan" forKey:@"payment_id"];
//        [postDataDict setValue:@"" forKey:@"coupon_code"];
//
//        [self callService:METHOD_FREE_PLAN_PAY params:postDataDict key:@"FreePlanPay"];
//
//    }else{
//        invoice.payThroughLabel.hidden = NO;
//        invoice.stripePayButton.hidden = NO;
//        invoice.couponCodeTF.hidden = NO;
//        invoice.checkCouponButton.hidden = NO;
//        invoice.okButton.hidden = YES;
//        invoice.closeButton.hidden = NO;
//
//        invoice.title.text = @"Title";
//        invoice.price.text = @"Amount";
//        invoice.monthLabel.text = @"Month";
//
//        invoice.titleLabel.text = [invoiceDetails objectForKey:@"title"];
//        invoice.priceLabel.text = [NSString stringWithFormat:@" %@ %@", [invoiceDetails objectForKey:@"currency"],[invoiceDetails objectForKey:@"amount"]];
//        invoice.durationLabel.text = [NSString stringWithFormat:@"%@ months",[invoiceDetails objectForKey:@"plan"]];
//
//        //    [self.view.window addSubview:invoice];
//        [self.view addSubview:invoice];
//        [self fadeInAnimation:invoice];
//    }
//
//    [invoice.closeButton addTarget:self action:@selector(closeInvoice:) forControlEvents:UIControlEventTouchUpInside];
//    invoice.stripePayButton.tag = sender.tag;
//    [invoice.stripePayButton addTarget:self action:@selector(stripePay:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.okButton addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.checkCouponButton addTarget:self action:@selector(applyCouponTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [invoice.removeButton addTarget:self action:@selector(removeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    invoice.checkCouponButton.tag = sender.tag;
}

- (UIImage *)takeScreenshot {
    CGSize size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGRect rec = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view drawViewHierarchyInRect:rec afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)applyCouponTapped:(UIButton *)sender{
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setValue:invoice.couponCodeTF.text forKey:@"coupon_code"];
    [postDataDict setValue:[[plansArray objectAtIndex:sender.tag] objectForKey:@"subscription_id"] forKey:@"subscription_id"];
    
    if ([invoice.couponCodeTF.text isEqualToString:@""]) {
        [self popup:@"Please enter valid code"];
    }else{
        [self callService:METHOD_APPLY_COUPON_SUBS params:postDataDict key:@"ApplyCoupon"];
    }
}

- (void)removeButtonTapped:(UIButton *)sender{
    invoice.couponCodeTF.text = @"";
    NSDictionary *planDetails = [plansArray objectAtIndex:sender.tag];
    invoice.priceLabel.text = [NSString stringWithFormat:@"%@ %@", [planDetails objectForKey:@"currency"],[planDetails objectForKey:@"amount"]];
    stripeAmount = [planDetails objectForKey:@"amount"];
    invoice.couponSuccessView.hidden = YES;
    invoice.couponCodeView.hidden = NO;
}

#pragma mark Service Call

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params key:(NSString *)key{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    if ([key isEqualToString:@"CardList"]) {
                        plansArray = [dict objectForKey:@"data"];
                        currency = [[plansArray objectAtIndex:0] objectForKey:@"currency"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.plansTableView reloadData];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }else if ([key isEqualToString:@"ApplyCoupon"]){
                        NSString *amount = [NSString stringWithFormat:@"%@", [[dict objectForKey:@"data"] objectForKey:@"remaining_amount"]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            invoice.couponCodeView.hidden = YES;
                            invoice.couponSuccessView.hidden = NO;
                            invoice.appliedCouponLabel.text = [[dict objectForKey:@"data"] objectForKey:@"coupon_code"];
                            stripeAmount = amount;
                            invoice.priceLabel.text = [NSString stringWithFormat:@"%@%@",currency, amount];
                            if ([stripeAmount isEqualToString:@"0"]) {
                                completeInvoice.price = [NSString stringWithFormat:@"%@%@",currency, stripeAmount];
                                completeInvoice.status = @"Approved";
                                completeInvoice.paymentID = @"Coupon added";
                                [self presentViewController:completeInvoice animated:YES completion:nil];
                            }
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }else if ([key isEqualToString:@"FreePlanPay"]){
//                        NSDictionary *freePlanDict = @{
//                                            @"payment_id" : @"Free Plan"
//                                                    };
                        stripeAmount = @"0";
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completeInvoice.price = [NSString stringWithFormat:@"%@%@", currency, stripeAmount];
                            completeInvoice.status = @"Approved";
                            completeInvoice.paymentID = @"Free Plan";
                            [self presentViewController:completeInvoice animated:false completion:nil];

                            [self viewWillAppear:YES];
                            [self popup: [dict objectForKey:@" message"]];
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                    }
                }
                else{
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                }
            }
        }];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

- (void)stripePayRequest:(NSString *)service params:(NSMutableDictionary *)params tag:(NSInteger)tag{
    
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    
                    NSDictionary *detailedDict = plansArray[tag];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
//                        [self paymentCompleteInvoice:dict];
                        
                        completeInvoice.price = [NSString stringWithFormat:@"%@%@", currency, stripeAmount];
                        completeInvoice.status = @"Approved";
                        completeInvoice.paymentID = [[dict objectForKey:@"data"] objectForKey:@"payment_id"];
                        [self presentViewController:completeInvoice animated:false completion:nil];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                    
                }else{
                    
                    CardsVC *addCard = [self.storyboard instantiateViewControllerWithIdentifier:@"CardsVC"];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController pushViewController:addCard animated:YES];
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        [self popup:[dict objectForKey:@"error_messages"]];
                    });
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.plansTableView reloadData];
                    [self labelAndClearUpdate];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                });
            }
        }];
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

// Payment Complete !!

-(void)paymentCompleteInvoice:(NSDictionary *)response{
    invoice.couponCodeView.hidden = YES;
    invoice.couponSuccessView.hidden = YES;
    invoice.payThroughLabel.hidden = YES;
    invoice.stripePayButton.hidden = YES;
    invoice.couponCodeTF.hidden = YES;
    invoice.checkCouponButton.hidden = YES;
    invoice.okButton.hidden = NO;
    invoice.closeButton.hidden = YES;
    
    invoice.title.text = @"Paid Amount";
    invoice.price.text = @"Status";
    invoice.monthLabel.text = @"Payment ID";
    
    invoice.titleLabel.text = [NSString stringWithFormat:@"%@ %@",[invoiceDetails objectForKey:@"currency"],stripeAmount];
    invoice.priceLabel.text = @"Approved";
    invoice.durationLabel.text = [[response objectForKey:@"data"] objectForKey:@"payment_id"];
    
//    [self.view.window addSubview:blurEffectView];
//    [self.view.window addSubview:invoice];
    [self.view addSubview:blurEffectView];
    [self.view addSubview:invoice];
}

- (void) labelAndClearUpdate{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

    if (plansArray.count == 0) {
        
        UIImageView *imgeView=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 150, 100, 100)];
        imgeView.image=[UIImage imageNamed:@"oops.png"];
        [self.view addSubview:imgeView];
        
        UILabel *lblName=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-40, 30)];
        lblName.text=@"Oops! List is empty ";
        lblName.textAlignment=NSTextAlignmentCenter;
        lblName.textColor=[UIColor whiteColor];
        [lblName setFont:[UIFont systemFontOfSize:15]];
        [self.view addSubview:lblName];
    }
}

// Pop up

- (void) popup:(NSString *)message{
    
    if (message == nil) {
        message = @"Done !";
    }
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

- (void)okButtonTapped:(UIButton *)sender{
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    dispatch_async(dispatch_get_main_queue(), ^{
//        self.myPlans = YES;
//        [self viewWillAppear:YES];
        MyPlans *myplanVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPlans"];
        [self.navigationController pushViewController:myplanVC animated:YES];
    });
}

- (void)paymentCompleted:(NSNotification *)notification{
    [invoice removeFromSuperview];
    [blurEffectView removeFromSuperview];
    dispatch_async(dispatch_get_main_queue(), ^{
//        self.myPlans = YES;
//        [self viewWillAppear:YES];
        MyPlans *myplanVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPlans"];
        [self.navigationController pushViewController:myplanVC animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
