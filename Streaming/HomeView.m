//
//  HomeView.m
//  Streaming
//
//  Created by Ramesh on 19/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "HomeView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "ProgressIndicator.h"
#import "UIImageView+WebCache.h"
#import "SingleVideoView.h"
#import "viewAllViewController.h"
#import "SearchBarVC.h"
#import "SearchBarVC.h"
#import "UtilityClass.h"
#import "SingleVideoVC.h"

@interface HomeView ()<WebServiceDelegate,UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating,UIScrollViewDelegate>
{
    AppDelegate *appDelegate;
    UIScrollView *scrollView;
    NSArray *arrList;
      NSArray *arrBanner;
    
    NSArray *arrViewAll;
    
    NSDictionary *dictSendVal;
    NSString *strAd_VideoID;
    UIScrollView *imgScrlView;
    UIPageControl *pageControl;
    int nBannerCount;
    NSTimer *nTime;
    NSString *strViewAllTitle;
    NSUserDefaults *defaluts;
    NSDictionary *viewAllDict;
    NSString *strSendKey,*strTotalCount,*strIdenty;
    UIButton *menu,*search;
}
//Fetch result controller
@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation HomeView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    strIdenty=@"home";
   
//    self.title=@"Home";
    nBannerCount=0;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    defaluts=[[NSUserDefaults alloc]init];

    // Menus
    Webservice *service1=[[Webservice alloc]init];
    service1.tag=10;
    service1.delegate=self;
    
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    NSString *strSend1=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\"}",strID,strToken];
    [service1 executeWebserviceWithMethod:METHOD_GET_CATEGORIES withValues:strSend1];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self callHomeAPI];
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.navigationBarHidden=YES;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
//    @"streamview_new.png"
    UIImage *img = [UIImage imageNamed:@"streamview_new"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 15)];
    [imgView setImage:img];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
//    self.navigationItem.titleView = imgView;
    [scrollView addSubview:imgView];
    
}

- (void)callHomeAPI{
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    NSString *subProfileID = [defaluts valueForKey:@"sub_profile_id"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"sub_profile_id\":\"%@\"}",strID,strToken,subProfileID];
    
    [service executeWebserviceWithMethod:METHOD_HOME withValues:strSend];
}

//-(void)viewanimation
//{
//    if (nBannerCount<arrBanner.count)
//    {
//        nBannerCount++;
//        int pageNumber = nBannerCount;
//
//        CGRect frame = imgScrlView.frame;
//        frame.origin.x = frame.size.width*pageNumber;
//        frame.origin.y=0;
//
//        [imgScrlView scrollRectToVisible:frame animated:YES];
//    }
//    else
//    {
//        nBannerCount=0;
//        int pageNumber = nBannerCount;
//
//        CGRect frame = imgScrlView.frame;
//        frame.origin.x = frame.size.width*pageNumber;
//        frame.origin.y=0;
//
//        [imgScrlView scrollRectToVisible:frame animated:YES];
//
//    }
//}

-(void)onPageLoad
{
    [scrollView removeFromSuperview];
    
//    int n=self.view.frame.size.width/3-10;
    scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollView.showsHorizontalScrollIndicator=NO;
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    
    UIImage *arfeenkhanImage = [UIImage imageNamed:@"iphone_bg"];
    UIImageView *afimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 480)];
    [afimgView setImage:arfeenkhanImage];
    [afimgView setContentMode:UIViewContentModeScaleAspectFill];
    afimgView.clipsToBounds = YES;
    [scrollView addSubview:afimgView];
    
    
    UIButton *menu = [[UIButton alloc] initWithFrame:CGRectMake(8, 22, 50, 50)];
    [menu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [menu setContentMode:UIViewContentModeCenter];
    [menu addTarget:self action:@selector(navigationView:) forControlEvents:UIControlEventTouchDown];
     [scrollView addSubview:menu];
    
    

    UIButton *search = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-65, 22, 50, 50)];
    [search setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [search addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:search];
    
    
    
    UILabel *arfeenkhanname = [[UILabel alloc] initWithFrame:CGRectMake(30, 330, self.view.frame.size.width-40, 30)];
    arfeenkhanname.text = @"A R F E E N K H A N' S";
    arfeenkhanname.textColor = [UIColor whiteColor];
    [arfeenkhanname setFont:[UIFont fontWithName:@"montserrat_light.ttf" size:50]];
    arfeenkhanname.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:arfeenkhanname];
    
   
    UILabel *albl = [[UILabel alloc]initWithFrame:CGRectMake(20, 320, self.view.frame.size.width-40, 100)];
    albl.text = @"THE SECRET MILLIONAIRE BLUEPRINT" ;
    albl.textColor = [UIColor whiteColor];
    albl.textAlignment = NSTextAlignmentCenter;
    albl.numberOfLines = 2;
    [albl setFont:[UIFont fontWithName:@"montserrat_light.ttf" size:30]];
    [scrollView addSubview:albl];
    
    UILabel *typelbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 350, self.view.frame.size.width-30, 100)];
    typelbl.text = @"Wealth & Finance";
    typelbl.textColor = [UIColor whiteColor];
    [typelbl setFont:[UIFont fontWithName:@"montserrat_light.ttf" size:25]];
    typelbl.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:typelbl];
    
    UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, 420, 110, 50)];
    [playBtn setTitle:@"Play" forState:UIControlStateNormal];
    [playBtn setImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
    playBtn.backgroundColor = [UIColor redColor];
    [scrollView addSubview:playBtn];
    
    
//
//    if (arrBanner.count!=0)
//    {
//        int pageCount=arrBanner.count;
//
//        int xPos=0;
//
//        imgScrlView=[[UIScrollView alloc]initWithFrame:CGRectMake(xPos, 5, self.view.frame.size.width, self.view.frame.size.height/3)];
//        imgScrlView.backgroundColor=[UIColor clearColor];
//        imgScrlView.pagingEnabled=YES;
//        imgScrlView.delegate=self;
//        imgScrlView.contentSize=CGSizeMake(pageCount * imgScrlView.bounds.size.width , imgScrlView.bounds.size.height);
//        [imgScrlView setShowsHorizontalScrollIndicator:NO];
//        [imgScrlView setShowsVerticalScrollIndicator:NO];
//        [scrollView addSubview:imgScrlView];
//
//        CGRect viewSize;//=imgScrlView.bounds;
//        for (int i=0; i<arrBanner.count; i++)
//        {
//            if (i==0)
//                viewSize=imgScrlView.bounds;
//            else
//                viewSize=CGRectOffset(viewSize, imgScrlView.bounds.size.width, 0);
//
//            NSDictionary *dictLoc= [arrBanner objectAtIndex:i];
//
//          //  NSString *strImageURL;//=[dictLocal valueForKey:@"EventImageURL"] ;
//            UIImageView *imgView=[[UIImageView alloc] initWithFrame:viewSize];
//
//            imgView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:imgView urlString:[dictLoc valueForKey:@"default_image"]];
//
////            [imgView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"dummy.png"] options:SDWebImageProgressiveDownload];
//
//            imgView.contentMode = UIViewContentModeScaleAspectFill;
//            imgView.clipsToBounds = YES;
//           [imgScrlView addSubview:imgView];
//
//            UIButton *btnClick=[[UIButton alloc]initWithFrame:viewSize];
//            btnClick.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//            // [btnClick addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchDown];
//            [imgScrlView addSubview:btnClick];
//
//            //Single tap
//            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
//            tapGesture.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
//            tapGesture.numberOfTapsRequired = 1;
//            tapGesture.numberOfTouchesRequired = 1;
//            [btnClick addGestureRecognizer:tapGesture];
//        }
//        pageControl = [[UIPageControl alloc] init];
//        pageControl.frame = CGRectMake(imgScrlView.frame.size.width/2-70,self.view.frame.size.height/3-40,150,60);
//        pageControl.numberOfPages = pageCount;
//        pageControl.currentPage = 0;
//       // [pageControl addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
//        [scrollView addSubview:pageControl];
//
//     nTime=[NSTimer  scheduledTimerWithTimeInterval:03.0 target:self selector:@selector(viewanimation) userInfo:nil repeats:YES];
//    }
    
   int  yPos=480;
    for (int nCount=0; nCount<arrList.count; nCount++)
    {
        int xPos=5;
        
        NSDictionary *dictLocal=[arrList objectAtIndex:nCount];
        NSArray *arrLocal=[dictLocal valueForKey:@"list"];
        if (arrLocal.count!=0)
        {
            UILabel *lblNameTile=[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width-70, 25)];
            lblNameTile.text=[[dictLocal valueForKey:@"name"] uppercaseString];
            [lblNameTile setFont:[UIFont boldSystemFontOfSize:15]];
            lblNameTile.textAlignment=NSTextAlignmentLeft;
            lblNameTile.textColor=[UIColor whiteColor];
            [scrollView addSubview:lblNameTile];
            
            UIButton *btnMore=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width
                                                                        -55, yPos, 50, 25)];
            [btnMore setTitle:[dictLocal valueForKey:@"key"] forState:UIControlStateFocused];
            [btnMore setTitle:[dictLocal valueForKey:@"name"] forState:UIControlStateHighlighted];
//            [btnMore setTitle:@"See All >" forState:UIControlStateNormal];
            [btnMore setImage:[UIImage imageNamed:@"arrow_right_white.png"] forState:UIControlStateNormal];
            [btnMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
          //  [btnMore setTitleColor:[UIColor colorWithRed:218.0f/255.0f green:106.0f/255.0f blue:44.0f/255.0f alpha:1] forState:UIControlStateNormal];
            [btnMore.titleLabel setFont:[UIFont systemFontOfSize:13]];
            btnMore.contentHorizontalAlignment=NSTextAlignmentRight;
            // btnMore.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
            [btnMore addTarget:self action:@selector(onViewAll:) forControlEvents:UIControlEventTouchDown];
           // [btnMore setBackgroundImage:[UIImage imageNamed:@"view_more.png"] forState:UIControlStateNormal];
            [scrollView addSubview:btnMore];
            
            yPos+=30;
            
            UIScrollView *scrollViewSubView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, yPos, self.view.frame.size.width,150+25)];
            scrollViewSubView.showsHorizontalScrollIndicator=NO;
            scrollViewSubView.showsVerticalScrollIndicator=NO;
            [scrollView addSubview:scrollViewSubView];
            
            for (int i=0; i<arrLocal.count; i++)
            {
                NSDictionary *dictLoc=[arrLocal objectAtIndex:i];
                
                UIView *viewContent = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0,240,140+45)];
                viewContent.backgroundColor =[[UIColor blackColor] colorWithAlphaComponent:0.1f];
                //viewContent.opaque=NO;
               // viewContent.layer.shadowColor = [[UIColor blackColor] CGColor];
                viewContent.layer.shadowOpacity = 1.0f;
                viewContent.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
                viewContent.layer.shadowRadius = 1.0f;
                viewContent.layer.cornerRadius = 2.0f;
               // viewContent.alpha=0.5;
                viewContent.tag=123;
                [scrollViewSubView addSubview:viewContent];
                
                UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, viewContent.frame.size.width,140)];
                
//                UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, 115,140)];
                imageView.layer.cornerRadius = 2.0f;
                
                imageView = [[UtilityClass sharedInstance] setImageWithActivityIndicator:imageView urlString:[dictLoc valueForKey:@"default_image"]];

//                [imageView sd_setImageWithURL:[NSURL URLWithString:[dictLoc valueForKey:@"default_image"]] placeholderImage:[UIImage imageNamed:@"can_main.png"] options:SDWebImageProgressiveDownload];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                [viewContent addSubview:imageView];
                
                UIImageView *imgPlay=[[UIImageView alloc]initWithFrame:CGRectMake(imageView.frame.size.width/2-15, imageView.frame.size.height/2-15, 30, 30)];
                imgPlay.image=[UIImage imageNamed:@"play.png"];
                //imgPlay.alpha=0.6;
//                [viewContent addSubview:imgPlay];
                
                UIButton *btnClick=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, viewContent.frame.size.width, viewContent.frame.size.height)];
                btnClick.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
                [viewContent addSubview:btnClick];
                
                //Single tap
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBtnClick:)];
                tapGesture.view.tag=[[dictLoc valueForKey:@"admin_video_id"]intValue];
                tapGesture.numberOfTapsRequired = 1;
                tapGesture.numberOfTouchesRequired = 1;
                [btnClick addGestureRecognizer:tapGesture];
                
                UILabel *lblTitle=[[UILabel alloc]initWithFrame:CGRectMake(0, 115+35, viewContent.frame.size.width-10, 20)];
                
                lblTitle.text=[dictLoc valueForKey:@"title"];
                [lblTitle setFont:[UIFont systemFontOfSize:15.0f]];
                lblTitle.textColor=[UIColor lightGrayColor];
                lblTitle.lineBreakMode = NSLineBreakByClipping;
                //lblTitle.minimumFontSize = 0;
                [viewContent addSubview:lblTitle];
                xPos+=viewContent.frame.size.width+5;
            }
            
            scrollViewSubView.contentSize=CGSizeMake(xPos, 165);
            yPos+=185;
        }
        
     }
    
    scrollView.contentSize=CGSizeMake(self.view.frame.size.width, yPos+50);
    
}


- (IBAction)searchButtonTapped:(id)sender {
    
    SearchBarVC *search = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchBarVC"];
    [self.navigationController pushViewController:search animated:YES];
    
}


- (IBAction)navigationView:(id)sender {
    
//   printf("Hello World");
    
    
    
//    SlideMenuView *submenu = [self.storyboard instantiateViewControllerWithIdentifier:@""];
//    [self.navigationController pushViewController:submenu animated:YES];
    
//    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
//    singleVideo.adminVideoID = strAd_VideoID;
//    [self.navigationController pushViewController:singleVideo animated:YES];
    
//    [menu addTarget:self action:@selector(navigationView:) forControlEvents:UIControlEventTouchDown];
    
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController )
//    {
//        [self.SideBarButton setTarget: self.revealViewController];
//        [self.SideBarButton setAction: @selector( revealToggle: )];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
//
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView1{
    
    CGFloat viewWidth = scrollView1.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    int pageNumber = floor((scrollView1.contentOffset.x - viewWidth/50) / viewWidth) +1;
    
    pageControl.currentPage=pageNumber;
    
}

//- (void)pageChanged
//{
//    int pageNumber = pageControl.currentPage;
//
//    CGRect frame = imgScrlView.frame;
//    frame.origin.x = frame.size.width*pageNumber;
//    frame.origin.y=0;
//
//    [imgScrlView scrollRectToVisible:frame animated:YES];
//}

-(IBAction)onViewAll:(id)sender
{
    [self callHomeAPI];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    
    UIButton *btn=(UIButton *) sender;
    
    strViewAllTitle=[btn titleForState:UIControlStateHighlighted];
    
   // NSString *strVal=[btn titleForState:UIControlStateFocused];
    
    strSendKey = [btn titleForState:UIControlStateFocused];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=4;
    
    NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
    NSString *strID=[defaluts valueForKey:@"id"];
    NSString *strToken=[defaluts valueForKey:@"token"];
    
    NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"key\":\"%@\",\"skip\":\"%@\"}",strID,strToken,[btn titleForState:UIControlStateFocused],@"0"];
    
    [service executeWebserviceWithMethod:METHOD_VIEW_ALL withValues:strSend];
}

-(IBAction)onSingleClear:(id)sender
{
    UIButton *btn=(UIButton *) sender;
    
    UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"" preferredStyle:UIAlertControllerStyleActionSheet]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *strAdminVideoId=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
        
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=3;
        
        NSUserDefaults *defaluts=[[NSUserDefaults alloc]init];
        NSString *strID=[defaluts valueForKey:@"id"];
        NSString *strToken=[defaluts valueForKey:@"token"];
        
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"admin_video_id\":\"%@\"}",strID,strToken,strAdminVideoId];
        
        [service executeWebserviceWithMethod:METHOD_ADD_WISHLIST withValues:strSend];
    }]; // 8
    
    [alert addAction:defaultAction];
    
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button Cancel");
    }]; // 8
    
    [alert addAction:defaultAction1];
    
    [self presentViewController:alert animated:YES completion:nil]; // 11
    
}

//-(IBAction)onBannerClick:(id)sender
//{
//    UIButton *btn=(UIButton *) sender;
//    NSString *strVideoID=[NSString stringWithFormat:@"%ld",btn.tag];
//    
//    strAd_VideoID=strVideoID;
//    [self performSegueWithIdentifier:@"singleVideo" sender:self];
//    
//}

-(IBAction)onBtnClick:(id)sender
{
     UIGestureRecognizer *recognizer = (UIGestureRecognizer*) sender;
//
//    //UIButton *btn=(UIButton *) sender;
    NSString *strVideoID=[NSString stringWithFormat:@"%ld",(long)recognizer.view.tag];
//
    strAd_VideoID=strVideoID;
//     [self performSegueWithIdentifier:@"singleVideo" sender:self];
    
    SingleVideoVC *singleVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVideoVC"];
    singleVideo.adminVideoID = strAd_VideoID;
    [self.navigationController pushViewController:singleVideo animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrList=[dictResponse valueForKey:@"data"];
            arrBanner=[[dictResponse valueForKey:@"banner"] objectForKey:@"list"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self onPageLoad];
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            });
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
        }
    }

    else if (webservice.tag==4)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            arrViewAll=[dictResponse valueForKey:@"data"];
            strTotalCount=[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"total"]];
            viewAllDict = dictResponse;
            if (arrViewAll.count !=0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"view_all" sender:self];
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                });
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:appDelegate.strAlertTitle message:@"No more details" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    });
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];

            }
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
        }
    }
    if (webservice.tag==10)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            appDelegate.arrMenuCategoryList=[dictResponse valueForKey:@"categories"];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
        
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"singleVideo"])
    {
        SingleVideoView *singleVideo=[segue destinationViewController];
//        singleVideo.dictGetValue=dictSendVal;
        singleVideo.strVideoID=strAd_VideoID;
     }
    else if ([[segue identifier] isEqualToString:@"view_all"])
    {
        viewAllViewController *view=[segue destinationViewController];
        view.arrList=arrViewAll;
        view.strTitleName=strViewAllTitle;
        view.strKey=strSendKey;
        view.strTotalCount=strTotalCount;
        view.strIdenty=strIdenty;
        view.allVideoDict = viewAllDict;
    }
}


@end
