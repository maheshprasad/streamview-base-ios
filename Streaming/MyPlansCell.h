//
//  MyPlansCell.h
//  StreamFlix
//
//  Created by Aravinth Ramesh on 28/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlansCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *headingLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *accountCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *renewalButton;

@property (strong, nonatomic) IBOutlet UIView *cardBackView;
@property (strong, nonatomic) IBOutlet UIView *priceBackView;
@property (strong, nonatomic) IBOutlet UILabel *expireLabel;
@property (strong, nonatomic) IBOutlet UILabel *couponAmtLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalAmtLabel;
@property (strong, nonatomic) IBOutlet UIView *renewalButtonView;

@end
