//
//  SettingsView.m
//  Streaming
//
//  Created by Ramesh on 17/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "SettingsView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "ProgressIndicator.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "PrivacyandTermsView.h"
#import "ViewPlansVC.h"
#import "PaidVideosVC.h"
#import "UtilityClass.h"
#import "MyPlans.h"

@interface SettingsView ()<WebServiceDelegate>
{
    AppDelegate *appDelegate;
    NSDictionary *dictSend;
    NSUserDefaults *defaults;
}

@end

@implementation SettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    defaults=[[NSUserDefaults alloc]init];
    self.deleteView.hidden = YES;
    self.deleteContentView.layer.cornerRadius = 5.0;
    self.deleteContentView.layer.masksToBounds = YES;
    if ([[defaults objectForKey:@"payment_subscription"] intValue]) {
        viewPlansView.hidden = NO;
        paidVideosView.hidden = NO;
    }else{
        viewPlansView.hidden = YES;
        paidVideosView.hidden = YES;
    }
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.SideBarButton setTarget: self.revealViewController];
        [self.SideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    NSString *strID=[defaults valueForKey:@"id"];
    NSString *strToken=[defaults valueForKey:@"token"];
    
      NSString *strPush=[defaults valueForKey:@"push"];
    
    if ([strPush isEqualToString:@"1"])
    {
        [switchBtn setOn:YES animated:YES];
    }
    else
    {
        [switchBtn setOn:NO animated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    self.title = @"Settings";
}

- (IBAction)viewPlansTapped:(id)sender {
    ViewPlansVC *viewPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewPlansVC"];
    [self.navigationController pushViewController:viewPlans animated:YES];
}
- (IBAction)paidVideosTapped:(id)sender {
    PaidVideosVC *paidVideo = [self.storyboard instantiateViewControllerWithIdentifier:@"PaidVideosVC"];
    paidVideo.title = @"Paid Videos";
    [self.navigationController pushViewController:paidVideo animated:YES];
}
- (IBAction)myPlansTapped:(id)sender {
    MyPlans *subsPlans = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPlans"];
    [self.navigationController pushViewController:subsPlans animated:YES];
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue] == true)
        {
            dictSend=[dictResponse valueForKey:@"page"];
            [self performSegueWithIdentifier:@"details" sender:self];
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
        
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [defaults setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"push_status"]] forKey:@"push"];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
        
    }
    else if (webservice.tag==3)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            [appDelegate onExpriedPage];
            
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"details"])
    {
        PrivacyandTermsView *privacy=[segue destinationViewController];
        privacy.dictValue=dictSend;
    }
    
}


- (IBAction)onPrivacy:(id)sender
{
    NSString *strID=[defaults valueForKey:@"id"];
    NSString *strToken=[defaults valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    NSString *strSend=[NSString stringWithFormat:@"?%@&=%@",strID,strToken];
    
    [service executeWebserviceWithMethod1:METHOD_PRIVACY withValues:strSend];
}

- (IBAction)onTerms:(id)sender
{
    NSString *strID=[defaults valueForKey:@"id"];
    NSString *strToken=[defaults valueForKey:@"token"];
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    NSString *strSend=[NSString stringWithFormat:@"?%@&=%@",strID,strToken];
    
    [service executeWebserviceWithMethod1:METHOD_TERMS withValues:strSend];
    
}

- (IBAction)switchToggled:(id)sender
{
    UISwitch *mySwitch = (UISwitch *)sender;
    NSString *strSend;
    NSString *strID=[defaults valueForKey:@"id"];
    NSString *strToken=[defaults valueForKey:@"token"];
    if ([mySwitch isOn])
    {
        strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"1"];
         //NSLog(@"its on!");
    } else
    {
        
     strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"status\":\"%@\"}",strID,strToken,@"0"];
        
       //NSLog(@"its off!");
    }//METHOD_NOTIFICATION
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=2;
    [service executeWebserviceWithMethod:METHOD_NOTIFICATION withValues:strSend];

}
- (IBAction)deleteButtonTapped:(id)sender {
    [self.passwordTF resignFirstResponder];
    if ([self.passwordTF.text isEqualToString:@""]) {
        [self popup:@"Please enter password"];
    }else if (self.passwordTF.text.length < 6){
        [self popup:@"Passsword should be minimum 6 characters"];
    }else{
        [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
        Webservice *service=[[Webservice alloc]init];
        service.delegate=self;
        service.tag=3;
        NSString *strID=[defaults valueForKey:@"id"];
        NSString *strToken=[defaults valueForKey:@"token"];
        NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"password\":\"%@\"}",strID,strToken,self.passwordTF.text];
        [service executeWebserviceWithMethod:METHOD_DELETE withValues:strSend];
    }    
}
- (IBAction)cancelButtonTapped:(id)sender {
    [self.passwordTF resignFirstResponder];
    self.deleteView.hidden = YES;
}

- (IBAction)onDelegate:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:APP_NAME message:@"Do you want delete your account" preferredStyle:UIAlertControllerStyleAlert]; // 7
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if ([[defaults objectForKey:@"login_by"] isEqualToString:@"manual"]) {
            self.deleteView.hidden = NO;
        }else{
            [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
            Webservice *service=[[Webservice alloc]init];
            service.delegate=self;
            service.tag=3;
            NSString *strID=[defaults valueForKey:@"id"];
            NSString *strToken=[defaults valueForKey:@"token"];
            NSString *strSend=[NSString stringWithFormat:@"{\"id\":\"%@\",\"token\":\"%@\",\"password\":\"%@\"}",strID,strToken,@""];
            [service executeWebserviceWithMethod:METHOD_DELETE withValues:strSend];
        }
    }]; // 8
    
    [alert addAction:defaultAction];
    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {

    }]; // 8
    
    [alert addAction:defaultAction1];
    [self presentViewController:alert animated:YES completion:nil]; // 11
}

- (void) popup:(NSString *)message{
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:spamAlert animated:YES completion:nil];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
    });
}
@end
