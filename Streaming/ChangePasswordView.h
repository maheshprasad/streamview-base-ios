//
//  ChangePasswordView.h
//  Streaming
//
//  Created by KrishnaDev on 21/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordView : UIViewController
{
    IBOutlet UIScrollView *scrollView;
}
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
- (IBAction)onChangePassword:(id)sender;
- (IBAction)onBack:(id)sender;

@end
