//
//  SearchVideosCell.h
//  Streaming
//
//  Created by Aravinth Ramesh on 04/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVideosCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *cellBackView;
@property (strong, nonatomic) IBOutlet UIImageView *videoImageView;
@property (strong, nonatomic) IBOutlet UILabel *videoTitleLabel;

@end
