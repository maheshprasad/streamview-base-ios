//
//  ManageProfileVC.m
//  Streaming
//
//  Created by Aravinth Ramesh on 02/04/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "ManageProfileVC.h"
#import "SWRevealViewController.h"
#import "WebServiceHandler.h"
#import "ManageProfileCell.h"
#import "AddSubProfileVC.h"
#import "ProgressIndicator.h"
#import "UtilityClass.h"

@interface ManageProfileVC (){
    NSMutableArray *profilesArray;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic)  UIButton *morebutton;

@end

@implementation ManageProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];

    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    
    _morebutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [_morebutton setImage:[UIImage imageNamed:@"edit_white_new.png"] forState:UIControlStateNormal];
    _morebutton.selected=YES;
    [_morebutton addTarget:self action:@selector(editButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_editButton setCustomView:_morebutton];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = NO;
    
    profilesArray = [[NSMutableArray alloc] init];
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
    
    NSMutableDictionary *postDataDict = [[NSMutableDictionary alloc] init];
    [postDataDict setValue:[defaults valueForKey:@"id"] forKey:@"id"];
    [postDataDict setValue:[defaults valueForKey:@"token"] forKey:@"token"];
    [postDataDict setObject:@"ios" forKey:@"device_type"];
    
    [self callService:METHOD_MANAGE_PROFILES params:postDataDict];

}
- (IBAction)backButtonTapped:(id)sender {
    
    if (!_morebutton.isSelected) {
        [_morebutton setImage:[UIImage imageNamed:@"edit_white_new.png"] forState:UIControlStateNormal];
        _morebutton.selected=YES;
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)editButtonTapped:(id)sender {
    
    [_morebutton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    _morebutton.selected=NO;


    [self popup:@"Click the Profile to edit."];
    
}

- (void) popup:(NSString *)message{
    
    
    if (message == nil) {
        message = @"Done !";
    }
    
    UIAlertController *spamAlert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:spamAlert animated:YES completion:nil];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [spamAlert dismissViewControllerAnimated:YES completion:nil];
        
    });
}

#pragma mark Webservice

- (void)callService:(NSString *)service params:(NSMutableDictionary *)params{
    __block NSError *error;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait..."];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    
        [[WebServiceHandler sharedInstance] sendRequest:service isApiRequired:NO urlParameters:nil method:POST isAuthRequired:NO postData:postData withCompletionHandler:^(NSDictionary *dict, NSURLResponse *response, NSError *error) {
            
            if (![[UtilityClass sharedInstance] isNetworkConnected]) {
                [[UtilityClass sharedInstance] networkError:self];
            }else{
                if ([[dict objectForKey:@"success"] intValue] == 1) {
                    [profilesArray addObjectsFromArray:[dict objectForKey:@"data"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                        
                    });
                }else{
                    [[UtilityClass sharedInstance] errorHandling:dict VC:self];
                }

            }
            
        }];
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
}

#pragma mark CollectionView Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return profilesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ManageProfileCell *cell = (ManageProfileCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ManageProfileCell" forIndexPath:indexPath];
    NSDictionary *profileDetails = [profilesArray objectAtIndex:indexPath.row];
    
    cell.profilePicBackView.layer.cornerRadius = cell.profilePicBackView.frame.size.width/2;
    cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width/2;
    
    NSURL *pictureURL = [NSURL URLWithString:[profileDetails objectForKey:@"picture"]];
    NSData *imageData = [NSData dataWithContentsOfURL:pictureURL];
    UIImage *proImage = [UIImage imageWithData:imageData];
    
    cell.profileImage.image = proImage;
    cell.nameLabel.text = [profileDetails objectForKey:@"name"];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *profileDict = [profilesArray objectAtIndex:indexPath.row];
    
    if ([[profileDict objectForKey:@"name"] isEqualToString:@"Add Profile"]) {
        
        AddSubProfileVC *addProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSubProfileVC"];
        addProfile.deleteORadd = @"add";
        [self presentViewController:addProfile animated:YES completion:nil];
    
    }else if (!_morebutton.isSelected){
        
        AddSubProfileVC *addProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSubProfileVC"];
        addProfile.deleteORadd = @"delete";
        addProfile.subProfileDict = profileDict;
        
        [self presentViewController:addProfile animated:YES completion:nil];
        
    }
    
    else{
        
        NSUserDefaults *defaults = [[NSUserDefaults alloc] init];
        [defaults setObject:[profileDict objectForKey:@"name"] forKey:@"name"];
        [defaults setObject:[profileDict objectForKey:@"picture"] forKey:@"picture"];
        [defaults setObject:[profileDict objectForKey:@"id"] forKey:@"sub_profile_id"];
        [self.navigationController popViewControllerAnimated:YES];
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.view.frame.size.width/2-10, 160);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
