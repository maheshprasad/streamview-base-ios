//
//  AppDelegate.h
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Google/SignIn.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic) NSString *strDeviceToken,*strExpiredMsg,*strPlayerIdenty,*strAlertTitle;
@property(strong,nonatomic) NSString *strCategroryID,*strCategoryName,*strListORHistroyORSpam;
@property(strong,nonatomic) NSArray *arrMenuCategoryList;
//@property(nonatomic, assign) BOOL fullScreenVideoIsPlaying;
@property (assign, nonatomic) BOOL shouldRotate;
-(void)onExpriedPage;

@end

