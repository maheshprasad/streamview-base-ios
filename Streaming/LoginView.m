//
//  LoginView.m
//  Streaming
//
//  Created by KrishnaDev on 15/08/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

#import "LoginView.h"
#import "ProgressIndicator.h"
#import "AppDelegate.h"
#import <CFNetwork/CFNetwork.h>
#import "UtilityClass.h"

#define thumbSize CGSizeMake(130, 150)

@interface LoginView ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    AppDelegate *appDelegate;
    NSString *strProfileImg,*strID,*strName,*strEmailID,*strLoginType;
    
    UITapGestureRecognizer *tapGesture;
    bool keyboardIsShown;
    
    UITextField *currentTextView;

}

@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;

@end

@implementation LoginView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
     [appDelegate setShouldRotate:NO];
    
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    
    
    self.title=@"Login";
    
  //  scrollView=[uisc]
    
    [self UIUpdate];
    
}

- (void)UIUpdate{
//    [GIDSignIn sharedInstance].uiDelegate = self;
//
//    GIDSignIn* signIn = [GIDSignIn sharedInstance];
//    signIn.shouldFetchBasicProfile = YES;
//    //     [signIn setClientID: kClientId];
//    [signIn setScopes:[NSArray arrayWithObject:@"https://www.googleapis.com/auth/plus.login"]];
//    signIn.delegate = self;
////    signIn.uiDelegate = self;
//
    int xPos=0;
    int yPos=100;
//
////    UIButton *btnGoogle=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-160, yPos, 150, 45)];
//    UIButton *btnGoogle=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-165, yPos, 150, 45)];
//
//    [btnGoogle setTintColor:[UIColor whiteColor]];
//    [btnGoogle setImage:[UIImage imageNamed:@"google_final.png"] forState:UIControlStateNormal];
//    btnGoogle.backgroundColor = [UIColor whiteColor];
//    btnGoogle.contentMode = UIViewContentModeScaleAspectFit;
//    //    btnGoogle.clipsToBounds = YES;
//    btnGoogle.layer.cornerRadius = 3.0f;
//    [btnGoogle addTarget:self action:@selector(onGoogle:) forControlEvents:UIControlEventTouchDown];
//    [scrollView addSubview:btnGoogle];
//
//    UIButton *btnFacebook=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+10, yPos, 150, 45)];
//    btnFacebook.backgroundColor = [UIColor blueColor];
//    btnFacebook.contentMode = UIViewContentModeScaleAspectFit;
//    //    btnFacebook.clipsToBounds = YES;
//    btnFacebook.layer.cornerRadius = 3.0f;
//    [btnFacebook setImage:[UIImage imageNamed:@"fb_final.png"] forState:UIControlStateNormal];
//    [btnFacebook addTarget:self action:@selector(onFacebook:) forControlEvents:UIControlEventTouchDown];
//    [scrollView addSubview:btnFacebook];
//
//    yPos+=90;
//
    UILabel *lblOR=[[UILabel alloc]initWithFrame:CGRectMake(xPos, yPos, self.view.frame.size.width, 50)];
    lblOR.text=@"Login";
    lblOR.textColor = [UIColor whiteColor];
    lblOR.textAlignment=NSTextAlignmentCenter;
    //    [lblOR setFont:[UIFont boldSystemFontOfSize:20]];
    [lblOR setFont:[UIFont systemFontOfSize:35.0f]];
    [scrollView addSubview:lblOR];
    
    
    yPos+=50;
    
    txtEmail=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtEmail setBorderStyle:UITextBorderStyleNone];
    txtEmail.placeholder=@"Email";
    txtEmail.delegate=self;
    txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    txtEmail.keyboardType=UIKeyboardTypeEmailAddress;
    [txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtEmail.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtEmail];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=20;
    }
    
    
    txtPassword=[[UITextField alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 40)];
    [txtPassword setBorderStyle:UITextBorderStyleNone];
    txtPassword.placeholder=@"Password";
    txtPassword.delegate=self;
    txtPassword.secureTextEntry=YES;
    
    [txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    txtPassword.textColor=[UIColor whiteColor];
    [scrollView addSubview:txtPassword];
    
    yPos+=40;
    {
        UIView *viewLine=[[UIView alloc]initWithFrame:CGRectMake(20, yPos, self.view.frame.size.width-40, 1)];
        viewLine.backgroundColor=[UIColor whiteColor];
        [scrollView addSubview:viewLine];
        
        yPos+=10;
    }
    
    //yPos+=20;
    
    UIButton *btnForgetPwd=[UIButton buttonWithType:UIButtonTypeCustom];
    btnForgetPwd.frame=CGRectMake(20, yPos,self.view.frame.size.width/2-60, 40);
    [btnForgetPwd setTitle:@"Forgot Password" forState:UIControlStateNormal];
    [btnForgetPwd.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [btnForgetPwd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//     btnForgetPwd.backgroundColor=[UIColor colorWithRed:94.0f/255.0f green:94.0f/255.0f blue:100.0f/255.0f alpha:1];
    [btnForgetPwd addTarget:self action:@selector(onForgotPWD:) forControlEvents:UIControlEventTouchDown];
    btnForgetPwd.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [scrollView addSubview:btnForgetPwd];
    
    
    UIButton *btnSignUp=[UIButton buttonWithType:UIButtonTypeCustom];
    btnSignUp.frame=CGRectMake(270, yPos, 100, 60);
    [btnSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    //    [btnSignUp.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnSignUp.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [btnSignUp setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    btnSignUp.layer.cornerRadius = 5.0f;
    btnSignUp.backgroundColor = [UIColor clearColor];
    [btnSignUp addTarget:self action:@selector(onSignup:) forControlEvents:UIControlEventTouchDown];
//    btnSignUp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [scrollView addSubview:btnSignUp];
    
    
    yPos+=150;
    
    UIButton *btnLogin=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLogin.frame=CGRectMake(20, yPos, self.view.frame.size.width-40, 50);
    [btnLogin setTitle:@"LOGIN" forState:UIControlStateNormal];
    //    [btnLogin.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [btnLogin.titleLabel setFont:[UIFont systemFontOfSize:15]];
    btnLogin.layer.cornerRadius = 5.0f;
    [btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    btnLogin.backgroundColor=[UIColor colorWithRed:41.0f/255.0f green:50.0f/255.0f blue:64.0f/255.0f alpha:1];
    btnLogin.backgroundColor = [UIColor redColor];
    [btnLogin addTarget:self action:@selector(onLogin:) forControlEvents:UIControlEventTouchDown];
    [scrollView addSubview:btnLogin];
    
    yPos+=70;
    
//    UIButton *btnSignUp=[UIButton buttonWithType:UIButtonTypeCustom];
//    btnSignUp.frame=CGRectMake(20, yPos, self.view.frame.size.width-40, 50);
//    [btnSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
//    //    [btnSignUp.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
//    [btnSignUp.titleLabel setFont:[UIFont systemFontOfSize:15]];
//    [btnSignUp setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//    btnSignUp.layer.cornerRadius = 5.0f;
//    btnSignUp.backgroundColor = [UIColor clearColor];
//    [btnSignUp addTarget:self action:@selector(onSignup:) forControlEvents:UIControlEventTouchDown];
//    btnSignUp.contentHorizontalAlignment = UIControlContentVerticalAlignmentBottom;
//    [scrollView addSubview:btnSignUp];
    
    
    
    //    [self.txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    _txtEmail.textColor=[UIColor whiteColor];
    //
    //    [self.txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //    _txtPassword.textColor=[UIColor whiteColor];
    

    
    //    [signIn setDelegate: self];
    //    [signIn setUiDelegate:self];
    //NSLog(@"Initialized auth2...");
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    tapGesture.delegate = self;
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
}
-(void)dismissKeyBoard
{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}];
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)onGoogle:(id)sender
{
    [[GIDSignIn sharedInstance] signOut];
    
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    if (error)
    {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
    }
    else
        
    {
        
        //NSLog(@"User Id: %@",user.userID);
        //NSLog(@"Name: %@",user.profile.name);
        //NSLog(@"Email: %@",user.profile.email);
        
        strName=user.profile.name;
        strEmailID=user.profile.email;
        strID=user.userID;
        strLoginType=@"google";
        
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            strProfileImg=imageURL.absoluteString;
            //NSLog(@"Image URL :%@",imageURL);
        }
        
         [self onSocialLogin];
        
    }
    
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    
    if (error) {
        //NSLog(@"Error in signin %@",[error localizedDescription]);
        return;
    }
}



//-(IBAction)onFacebook:(id)sender
//{
//    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
//        if (error) {
//            //NSLog(@"Process error");
//        }
//        else if (result.isCancelled)
//        {
//            //NSLog(@"Cancelled");
//        }
//        else
//        {
//            //NSLog(@"Logged in");
//                        
//            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
//                                          initWithGraphPath:@"/me"
//                                          parameters:@{ @"fields": @"id,email,name,gender,birthday"}
//                                          HTTPMethod:@"GET"];
//            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
//             
//             
//             {
//                 if (!error) {
//                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                     [defaults setValue:result[@"email"] forKey:@"Email"];
//                     
//                   
//                     
//                     strName=result[@"name"];
//                     strEmailID=result[@"email"];
//                     strID=result[@"id"];
//                     strProfileImg=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", strID];
//                     strLoginType=@"facebook";
//                     
//                     
//                     //NSLog(@"fetched user:%@", result);
//                     [self onSocialLogin];
//                     
//                 }
//             }];
//        }
//    }];
//}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}

- (IBAction)onForgotPWD:(id)sender
{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
//     [self presentViewController:alertController animated:YES completion:nil];
    
    NSString *emailRegEx =@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([txtEmail.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Email id";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if([emailTest evaluateWithObject:txtEmail.text] == NO)
    {
        alertController.message=@"Please enter valid email address.";
        [self presentViewController:alertController animated:YES completion:nil];
        //_txtEmail.text=@"";
        return;
    }
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=2;
    
    NSString *strSend=[NSString stringWithFormat:@"{\"email\":\"%@\"}",txtEmail.text];
    
    [service executeWebserviceWithMethod:METHOD_FORGOT_PASSWORD withValues:strSend];
    
}

- (IBAction)onSignup:(id)sender {
    [self performSegueWithIdentifier:@"register" sender:self];
}

- (IBAction)onLogin:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    // [self presentViewController:alertController animated:YES completion:nil];
    
    NSString *emailRegEx =@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([txtEmail.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Email id";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    else if([emailTest evaluateWithObject:txtEmail.text] == NO)
    {
        alertController.message=@"Please enter valid email address.";
        [self presentViewController:alertController animated:YES completion:nil];
        //_txtEmail.text=@"";
        return;
    }
    
    else if ([txtPassword.text isEqualToString:@""])
    {
        alertController.message=@"Please Enter Password";
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
   
    
    if ([appDelegate.strDeviceToken isEqualToString:@""])
    {
        appDelegate.strDeviceToken=@"agsjsadjfbsdfsjdfbsdfsdf";
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strSend=[NSString stringWithFormat:@"{\"email\":\"%@\",\"password\":\"%@\",\"social_unique_id\":\"%@\",\"login_by\":\"%@\",\"device_token\":\"%@\",\"device_type\":\"%@\"}",txtEmail.text,txtPassword.text,@"",@"manual",appDelegate.strDeviceToken,@"ios"];
    
    [service executeWebserviceWithMethod:METHOD_LOGIN withValues:strSend];
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    [user setObject:@"1"  forKey:@"logintype"];
    [user synchronize];
    
}

-(void)onSocialLogin
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.navigationController.view withMessage:@"Please wait.."];
    
    Webservice *service=[[Webservice alloc]init];
    service.delegate=self;
    service.tag=1;
    
    NSString *strSend=[NSString stringWithFormat:@"{\"social_unique_id\":\"%@\",\"email\":\"%@\",\"picture\":\"%@\",\"mobile\":\"%@\",\"login_by\":\"%@\",\"device_token\":\"%@\",\"device_type\":\"%@\",\"name\":\"%@\"}",strID,strEmailID,strProfileImg,@"",strLoginType,appDelegate.strDeviceToken,@"ios",strName];
    
    [service executeWebserviceWithMethod:METHOD_REGISTER withValues:strSend];
    
    NSUserDefaults *user=[[NSUserDefaults alloc]init];
    [user setObject:@"2"  forKey:@"logintype"];
    [user synchronize];


    
}

#pragma  mark
#pragma mark  WebService Delegate

-(void)receivedErrorWithMessage:(NSString *)message
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(void)receivedResponse:(NSDictionary *)dictResponse fromWebservice:(Webservice *)webservice
{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (webservice.tag==1)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            
            NSUserDefaults *user=[[NSUserDefaults alloc]init];
            [user setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"id"]]  forKey:@"id"];
            [user setObject:[dictResponse valueForKey:@"name"]  forKey:@"name"];
            [user setObject:[dictResponse valueForKey:@"picture"]  forKey:@"picture"];
            [user setObject:[dictResponse valueForKey:@"token"]  forKey:@"token"];
            [user setObject:[NSString stringWithFormat:@"%@",[dictResponse valueForKey:@"push_status"]]  forKey:@"push"];
            [user setObject:[NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"sub_profile_id"]] forKey:@"sub_profile_id"];
            [user setObject:[dictResponse objectForKey:@"login_by"] forKey:@"login_by"];
            [user setObject:[dictResponse objectForKey:@"payment_subscription"] forKey:@"payment_subscription"];
//            [user setObject:@"1" forKey:@"payment_subscription"];
            [user synchronize];
            [self performSegueWithIdentifier:@"login_menu" sender:self];
        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];
        }
    }
    else if (webservice.tag==2)
    {
        if ([[dictResponse valueForKey:@"success"] boolValue]== true)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:APP_NAME message:[dictResponse valueForKey:@"error_messages"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];

        }
        else
        {
            [[UtilityClass sharedInstance] errorHandling:dictResponse VC:self];

        }
    }
}

#pragma mark -- Keyboard hide  and moving

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    //    if (!textField.inputAccessoryView)
    //    {
    //        textField.inputAccessoryView = toolKeyboard;
    //    }
    currentTextView=textField;
    
    //    [self moveViewUp:YES toRect:textField.frame];
    [self moveScrollView:textField.frame]; //edited
}

- (void)keyboardDidHide:(NSNotification *)n
{
    keyboardIsShown = NO;
    [self.view removeGestureRecognizer:tapGesture];
    
  //  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
     UIEdgeInsets contentInsets = UIEdgeInsetsMake(60, 0.0, 0.0, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardDidShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    keyboardIsShown = YES;
    [scrollView addGestureRecognizer:tapGesture];
}

- (void) moveScrollView:(CGRect) rect
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    CGSize kbSize = CGSizeMake(32, 260); // keyboard height
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = scrollView.frame;
    aRect.size.height -= (kbSize.height+55);
    if (!CGRectContainsPoint(aRect, rect.origin) )
    {
        CGPoint scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100);
        
        if(screenHeight==480)
            scrollPoint = CGPointMake(0.0, rect.origin.y-kbSize.height+100+90);
        
        if(scrollPoint.y>0)
            [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

@end
