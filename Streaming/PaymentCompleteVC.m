//
//  PaymentCompleteVC.m
//  StreamFlix
//
//  Created by Aravinth Ramesh on 25/07/18.
//  Copyright © 2018 Ramesh. All rights reserved.
//

#import "PaymentCompleteVC.h"

@interface PaymentCompleteVC ()

@end

@implementation PaymentCompleteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentVIew.layer.cornerRadius = 10.0;
    self.contentVIew.layer.masksToBounds = YES;
    self.okButton.layer.cornerRadius = 5.0;
    
    self.priceLabel.text = self.price;
    self.statusLabel.text = self.status;
    self.paymentIDLabel.text = self.paymentID;
    self.bgImageView.image = self.bgImage;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}
- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)okButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"PaymentCompleted"
     object:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
